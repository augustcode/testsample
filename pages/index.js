import React, { Component } from 'react';
import Head from "next/head"
import axios from "axios"
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import Loader from "../components/loader"
import Layout_Light from "../Layout/layout_light"
import { Toast } from 'reactstrap';

//images import start

//images import end

let i = 0

export async function getServerSideProps() {

  const about = await fetch(`${API_URL}/homepage-about-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );

  let solution = await fetch(`${API_URL}/solution-infos`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );

  let temp = await fetch(`${API_URL}/testimonials`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );

  let response = await fetch(`${API_URL}/homepage-banners`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );

  let pages = await fetch(`${API_URL}/pages`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );

  let temp_1 = await fetch(`${API_URL}/get-acquanited-footer-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );

  const headerMenus = await fetch(`${API_URL}/menus`
  , {
    headers: {
      'Authorization': `Bearer ${API_JWT_Token}`
    }

  }
);
const header_Menu = await headerMenus.json()

const socialIcons = await fetch(`${API_URL}/social-icons`
, {
  headers: {
    'Authorization': `Bearer ${API_JWT_Token}`
  }
}
);
const social_Icons = await socialIcons.json()

const footerAssociations = await fetch(`${API_URL}/associations`
, {
  headers: {
    'Authorization': `Bearer ${API_JWT_Token}`
  }
}
);
const footer_associations = await footerAssociations.json()

const footerContact = await fetch(`${API_URL}/contact-infos`
, {
  headers: {
    'Authorization': `Bearer ${API_JWT_Token}`
  }
}
);
const footer_contact = await footerContact.json()
const CookieWeb = await fetch(`${API_URL}/Web-settings`
, {
  headers: {
    'Authorization': `Bearer ${API_JWT_Token}`
  }

})
const Web_settings = await CookieWeb.json();


  var temp1 = await about.json() 
  var temp2 = await response.json() 
  var temp3 = await solution.json() 
  var temp4 = await pages.json() 
  var temp5 = await temp.json() 
  var temp6 = await temp_1.json() 

  return {
    props: {
      aboutus: temp1, homes: temp2, solutions: temp3, Pages: temp4, testimonial: temp5,
       accquinated: temp6,cookieWeb: Web_settings ,
      HeaderMenu: header_Menu, SocialIcons:social_Icons,FooterAssociations:footer_associations, FooterContact:footer_contact

    }
  };
}



export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      isLoaderLoaded: false,
    }
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }


  componentDidMount() {
 
    setTimeout(() => {
      $('html').removeClass('no-scroll');
    
      $(document).ready(function () {
        //
        //start and hide the page loader (start)
        setTimeout(function () {
          $('.loader-left').removeClass(' slideInLeft');
          $('.loader-left').addClass(' slideInLeftReverse');
          $('.loader-right').removeClass(' slideInRight');
          $('.loader-right').addClass(' slideInRightReverse');
        }, 5000);
  
        setTimeout(function () {
          $('.loader-center').addClass(' loader-logo-hide');
        }, 6000);
  
        setTimeout(function () {
          $(".loader").addClass(' animated zoomInCustom');
        }, 6500);
        setTimeout(function () {
          $(".loader").addClass(' loader-hide');
          () => { this.setState({ isLoaderLoaded: true }) };
        }, 6500);
  
        //start and hide the page loader (end)
        /*
         this code helps to split the
         words in the banner which help us to add 
         typing effect to all content. 
    */
        Splitting({
          /* target: String selector, Element, Array of Elements, or NodeList */
          target: "[data-splitting]",
          /* by: String of the plugin name */
          by: "chars",
          /* key: Optional String to prefix the CSS variables */
          key: null
        });
        // banner code
        var slideWrapper = $(".main-slider"),
          iframes = slideWrapper.find('.embed-player'),
          lazyImages = slideWrapper.find('.slide-image'),
          lazyCounter = 0;
  
        // POST commands to YouTube or Vimeo API
        function postMessageToPlayer(player, command) {
          if (player == null || command == null) return;
          player.contentWindow.postMessage(JSON.stringify(command), "*");
        }
  
        // When the slide is changing
        function playPauseVideo(slick, control) {
          var currentSlide, slideType, startTime, player, video;
  
          currentSlide = slick.find(".slick-current");
          slideType = currentSlide.attr("class").split(" ")[1];
          player = currentSlide.find("iframe").get(0);
          startTime = currentSlide.data("video-start");
  
          if (slideType === "vimeo") {
            switch (control) {
              case "play":
                if ((startTime != null && startTime > 0) && !currentSlide.hasClass('started')) {
                  currentSlide.addClass('started');
                  postMessageToPlayer(player, {
                    "method": "setCurrentTime",
                    "value": startTime
                  });
                }
                postMessageToPlayer(player, {
                  "method": "play",
                  "value": 1
                });
                break;
              case "pause":
                postMessageToPlayer(player, {
                  "method": "pause",
                  "value": 1
                });
                break;
            }
          } else if (slideType === "youtube") {
            switch (control) {
              case "play":
                postMessageToPlayer(player, {
                  "event": "command",
                  "func": "mute"
                });
                postMessageToPlayer(player, {
                  "event": "command",
                  "func": "playVideo"
                });
                break;
              case "pause":
                postMessageToPlayer(player, {
                  "event": "command",
                  "func": "pauseVideo"
                });
                break;
            }
          } else if (slideType === "video") {
            video = currentSlide.children("video").get(0);
            if (video != null) {
              if (control === "play") {
                video.play();
              } else {
                video.pause();
              }
            }
          }
        }
  
        // Resize player
        function resizePlayer(iframes, ratio) {
          if (!iframes[0]) return;
          var win = $(".main-slider"),
            width = win.width(),
            playerWidth,
            height = win.height(),
            playerHeight,
            ratio = ratio || 16 / 9;
  
          iframes.each(function () {
            var current = $(this);
            if (width / ratio < height) {
              playerWidth = Math.ceil(height * ratio);
              current.width(playerWidth).height(height).css({
                left: (width - playerWidth) / 2,
                top: 0
              });
            } else {
              playerHeight = Math.ceil(width / ratio);
              current.width(width).height(playerHeight).css({
                left: 0,
                top: (height - playerHeight) / 2
              });
            }
          });
        }
  
        // Initialize
        slideWrapper.on("init", function (slick) {
          slick = $(slick.currentTarget);
          setTimeout(function () {
            playPauseVideo(slick, "play");
          }, 1000);
          resizePlayer(iframes, 16 / 9);
        });
        slideWrapper.on("beforeChange", function (event, slick) {
          slick = $(slick.$slider);
          playPauseVideo(slick, "pause");
        });
        slideWrapper.on("afterChange", function (event, slick) {
          slick = $(slick.$slider);
          playPauseVideo(slick, "play");
        });
        slideWrapper.on("lazyLoaded", function (event, slick, image, imageSource) {
          lazyCounter++;
          if (lazyCounter === lazyImages.length) {
            lazyImages.addClass('show');
            // slideWrapper.slick("slickPlay");
          }
        });
  
        //start the slider
        slideWrapper.slick({
          // fade:true,
          autoplaySpeed: 4000,
          lazyLoad: "progressive",
          speed: 600,
          arrows: false,
          dots: true,
          cssEase: "cubic-bezier(0.87, 0.03, 0.41, 0.9)"
        });
  
  
        // Resize event
        $(window).on("resize.slickVideoPlayer", function () {
          resizePlayer(iframes, 16 / 9);
        });
  
        // banner code close
        /******************* banner text animation  ***********************/
  
        var el = $(".letter");
        var animation = new TimelineMax({ paused: true }).staggerTo(
          '.caption h3 .char, .caption .custom-h3-heading .char',
          0.3,
          {
            y: -15,
            ease: Power1.easeIn,
            onComplete() {
              TweenMax.to(this.target, 0.3, {
                y: 0,
                ease: Power3.easeOut,
                delay: 0.2
              });
            }
          },
          0.05
        );
  
  
  
        $('.caption h3 .char, .caption .custom-h3-heading .char').mouseover(function () {
          var duration = 2;
          TweenMax.to(this, duration / 4, { y: -15, ease: Power2.easeOut });
          TweenMax.to(this, duration / 2, { y: 0, ease: Power2.easeOut, delay: duration / 4 });
        });
  
  
        var controller = new ScrollMagic.Controller();
        var t5 = new TimelineMax();
  
        t5.staggerFromTo('.logo img', 0.9, {
          xPercent: -100,
          opacity: 0,
          ease: Elastic.easeOut.config(0.1, 0.5)
        },
          {
            xPercent: 0,
            opacity: 1,
            ease: Elastic.easeOut.config(0.1, 0.5)
          }, 0.2)
  
  
          .staggerFrom('.caption h3 .char, .caption .custom-h3-heading .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.05, "+=0.05")
  
          .fromTo(
            '.caption p .char, .caption .custom-h1-heading .char',
            0.01,
            { opacity: 0 },
            { opacity: 1 }
          )
  
        var scene = new ScrollMagic.Scene({
          triggerElement: ".banner-section"
        })
  
          .setTween(t5)
          .addTo(controller);
        /******************* banner text animation close  ***********************/
        /*********** about us section animation ****************/
        var controller = new ScrollMagic.Controller();
        var t2 = new TimelineMax();
  
        t2.fromTo('.about-trangle1 svg', 0.5, { display: "none" }, {
          display: "block", onComplete: function () {
            lazy1();
          }
        })
  
          .staggerFromTo('.about-section .section-title h3', 0.9, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.3)
  
          .fromTo('.about-section .title-border svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              pattern();
            }
          })
  
          .staggerFrom('.about-content .sub-title .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.01, "-=1")
          //.to('.about-content .sub-title .char',0.1, {opacity:0})
  
          .fromTo(
            '.about-content p',
            0.2,
            { opacity: 0 },
            { opacity: 1 },
            "-=0.5"
          )
  
          .staggerFromTo('.about-content li', 0.1, {
            xPercent: 100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.2)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.2)
            }, 0.1, "-=0.5")
  
          .fromTo(
            '.about-content .read-more',
            0.5,
            { opacity: 0 },
            { opacity: 1 },
            "-=1"
          )
  
          .fromTo('.about-trangle2 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              abouttriangle2();
            }
          })
  
        var scene = new ScrollMagic.Scene({
          triggerElement: "#about"
        })
          .setTween(t2)
          // .addIndicators()
          .addTo(controller);
        // scene.triggerHook(0.7);
        /*********** about us section animation close ****************/
        /*********** services section animation ****************/
        var controller = new ScrollMagic.Controller();
        var t3 = new TimelineMax();
  
        t3.staggerFromTo('.services-section .section-title h3', 0.4, {
          xPercent: 100,
          opacity: 0,
          ease: Elastic.easeOut.config(0.1, 0.5)
        },
          {
            xPercent: 0,
            opacity: 1,
            ease: Elastic.easeOut.config(0.1, 0.5)
          }, 0.2)
  
          .fromTo('.services-section .title-border svg', 0.5, { opacity: 0 }, {
            opacity: 1, onComplete: function () {
              pattern1();
            }
          })
  
          .fromTo('.services-line1 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              services_line1();
            }
          })
  
          .staggerFrom('.service1 .sub-title .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.01, "-=1")
  
          .staggerFromTo('.service1 h5', 0.4, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.1, "-=1")
  
          .staggerFromTo(
            '.service1 p',
            0.01,
            { opacity: 0 },
            { opacity: 1 },
            "-=1"
          )
  
          .staggerFromTo(
            '.service1 .read-more',
            0.1,
            { opacity: 0 },
            { opacity: 1 }
          )
  
  
  
          .staggerFromTo('.services-triangle1', 0.4, {
            xPercent: 100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.9)
  
          .fromTo('.services-line2 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              services_line2();
            }
          })
  
          .staggerFrom('.service2 .sub-title .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.01, "-=1")
  
          .staggerFromTo('.service2 h5', 0.4, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.1, "-=1")
  
          .staggerFromTo(
            '.service2 p',
            0.01,
            { opacity: 0 },
            { opacity: 1 },
            "-=1"
          )
  
          .staggerFromTo(
            '.service2 .read-more',
            0.1,
            { opacity: 0 },
            { opacity: 1 }
          )
  
          //  .fromTo('.services-line3 svg', 0.5,{display: "none"},{display: "block", onComplete:function(){
          //    services_line3()();
          //  }})
          //  
  
          .staggerFrom('.service3 .sub-title .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.01, "-=1")
  
          .staggerFromTo('.service3 h5', 0.4, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.1, "-=1")
  
          .staggerFromTo(
            '.service3 p',
            0.01,
            { opacity: 0 },
            { opacity: 1 },
            "-=1"
          )
  
          .staggerFromTo(
            '.service3 .read-more',
            0.1,
            { opacity: 0 },
            { opacity: 1 }
          )
  
          .fromTo('.services-left-bottom-line-2 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              services_left_bottom_line2();
            }
          })
  
          .fromTo('.services-left-bottom-line-1 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              services_left_bottom_line1();
            }
          })
  
  
  
          .staggerFromTo(
            '.load-more',
            0.5,
            { opacity: 0 },
            { opacity: 1 }
          )
  
        var scene = new ScrollMagic.Scene({
          triggerElement: "#services"
        })
          .setTween(t3)
          .addTo(controller);
  
  
  
        /*********** services section animation close ****************/
  
  
        /*********** testimonials section animation ****************/
        setTimeout(function () {
          var controller = new ScrollMagic.Controller();
          var t4 = new TimelineMax();
  
          t4.staggerFromTo('.testimonials-section .section-title h3', 0.2, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0, "-=3")
  
            .fromTo('.testimonials-section .title-border svg', 0.5, { display: "none" }, {
              display: "block", onComplete: function () {
                pattern2();
              }
            })
  
            .fromTo('.testimonials-line1 svg', 0.5, { display: "none" }, {
              display: "block", onComplete: function () {
                testimonial_line1();
              }
            })
  
            .fromTo(
              '.testimonials-wrap1 .testimonial-block',
              0.1,
              { scale: 0 },
              { scale: 1 },
              "-=3"
            )
  
            .staggerFromTo('.testimonials-wrap1 .testi-traingle1', 0.1, {
              xPercent: 100,
              opacity: 0,
              ease: Elastic.easeOut.config(0.2, 0.4)
            },
              {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.5, 0.7)
              }, 0.2, "-=2")
  
            .staggerFromTo('.testimonials-wrap1 .testi-traingle2', 0.1, {
              yPercent: 100,
              opacity: 0,
              ease: Elastic.easeOut.config(0.2, 0.4)
            },
              {
                yPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.5, 0.7)
              }, 0.2, "-=2")
  
            .fromTo(
              '.testimonials-wrap1 .user-img',
              0.1,
              { scale: 0 },
              { scale: 1 },
              "-=3"
            )
  
            .staggerFrom('.testimonials-wrap1 .testimonial-block .sub-title .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.01, "-=3")
  
            .staggerFromTo('.testimonials-wrap1 .testimonial-block h5', 0.1, {
              xPercent: -100,
              opacity: 0,
              ease: Elastic.easeOut.config(0.1, 0.1)
            },
              {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.1, 0.5)
              }, "-=5")
  
            .fromTo(
              '.testimonials-wrap1 .testimonial-block p',
              0.1,
              { opacity: 0 },
              { opacity: 1 },
              "-=2"
            )
  
            .fromTo(
              '.testimonials-wrap1 .testimonial-block .read-more',
              0.2,
              { opacity: 0 },
              { opacity: 1 },
              "-=1"
            )
  
            .staggerFromTo('.testimonials-triangle1', 0.5, {
              xPercent: -100,
              opacity: 0,
              ease: Elastic.easeOut.config(0.2, 0.4)
            },
              {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.5, 0.7)
              }, 0.7)
  
  
          var scene = new ScrollMagic.Scene({
            triggerElement: "#testimonials"
          })
            .setTween(t4)
            // .addIndicators()
            .addTo(controller);
  
  
  
  
          var t7 = new TimelineMax();
  
          t7.fromTo(
            '.testimonials-trigger-2 .testimonial-block',
            0.1,
            { scale: 0 },
            { scale: 1 },
            "-=4"
          )
  
            .staggerFromTo('.testimonials-trigger-2 .testi-traingle1', 0.1, {
              xPercent: 100,
              opacity: 0,
              ease: Elastic.easeOut.config(0.2, 0.4)
            },
              {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.5, 0.7)
              }, 0.2, "-=2")
  
            .staggerFromTo('.testimonials-trigger-2 .testi-traingle2', 0.1, {
              yPercent: 100,
              opacity: 0,
              ease: Elastic.easeOut.config(0.2, 0.4)
            },
              {
                yPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.5, 0.7)
              }, 0.2, "-=2")
  
            .fromTo(
              '.testimonials-trigger-2 .user-img',
              0.1,
              { scale: 0 },
              { scale: 1 },
              "-=3"
            )
  
            .staggerFrom('.testimonials-trigger-2 .testimonial-block .sub-title .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.05, "-=3")
  
            .staggerFromTo('.testimonials-trigger-2 .testimonial-block h5', 0.1, {
              xPercent: -100,
              opacity: 0,
              ease: Elastic.easeOut.config(0.1, 0.1)
            },
              {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.1, 0.5)
              }, "-=5")
  
            .fromTo(
              '.testimonials-trigger-2 .testimonial-block p',
              0.1,
              { opacity: 0 },
              { opacity: 1 },
              "-=2"
            )
  
            .fromTo(
              '.testimonials-trigger-2 .testimonial-block .read-more',
              0.2,
              { opacity: 0 },
              { opacity: 1 },
              "-=1"
            )
  
            .staggerFromTo('testimonials-trigger-2 .partnership-quote .sub-title', 0.2, {
              xPercent: -100,
              opacity: 0,
              ease: Elastic.easeOut.config(0.1, 0.5)
            },
              {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.1, 0.5)
              }, 0.3)
  
            .fromTo(
              'testimonials-trigger-2 .partnership-quote .button',
              0.1,
              { scale: 0 },
              { scale: 1 }
            )
  
          var scene = new ScrollMagic.Scene({
            triggerElement: ".testimonials-trigger-2"
          })
            .setTween(t7)
            .addTo(controller);
  
  
          var t8 = new TimelineMax();
  
          t8.staggerFromTo('.partnership-quote .sub-title', 0.2, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.3)
  
            .fromTo(
              '.partnership-quote .button',
              0.2,
              { scale: 0 },
              { scale: 1 }
            )
  
          var scene = new ScrollMagic.Scene({
            triggerElement: ".partnership-quote"
          })
            .setTween(t8)
            .addTo(controller);
  
        }, 6500);
        /*********** testimonials section animation close ****************/
  
        //SVG ANIMTION 
  
        //aboutTrangle1
        function lazy1() {
  
          if (document.readyState === 'complete') {
            if ($('#aboutTrangle1').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#aboutTrangle1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeOutQuad", "strokeWidth": 20, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
  
            }
          }
        }
  
        //title border
        function pattern() {
  
          if (document.readyState === 'complete') {
            if ($('#pattern').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#pattern');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1 });
              myAnimation.paint();
            }
          }
        }
  
        function pattern1() {
  
          if (document.readyState === 'complete') {
            if ($('#pattern').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#pattern');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1 });
              myAnimation.paint();
            }
          }
        }
  
        function pattern2() {
  
          if (document.readyState === 'complete') {
            if ($('#pattern').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#pattern');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1 });
              myAnimation.paint();
            }
          }
        }
  
        function services_right_top_line1() {
  
          if (document.readyState === 'complete') {
            if ($('#servicesright-top-line-1').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#servicesright-top-line-1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616", "delay": 0 });
              myAnimation.paint();
            }
          }
        }
  
        function services_right_top_line2() {
  
          if (document.readyState === 'complete') {
            if ($('#servicesright-top-line-2').length) {
              /*
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#servicesright-top-line-2');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616", "delay": 0 });
              myAnimation.paint();
            }
          }
        }
  
        function services_left_bottom_line1() {
  
          if (document.readyState === 'complete') {
            if ($('#servicesleft-bottom-line-1').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#servicesleft-bottom-line-1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }
  
        function services_left_bottom_line2() {
  
          if (document.readyState === 'complete') {
            if ($('#servicesleft-bottom-line-2').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#servicesleft-bottom-line-2');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }
  
  
        function services_line1() {
  
          if (document.readyState === 'complete') {
            if ($('#servicesline1').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#servicesline1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 20.7, "strokeOpacity": 1, "strokeColor": "#d61616", "strokeCap": "square" });
              myAnimation.paint();
            }
          }
        }
  
        function abouttriangle2() {
          if (document.readyState === 'complete') {
            if ($('#abouttriangle22').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#abouttriangle22');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 25, "strokeOpacity": 1, "strokeColor": "#d61616", "strokeCap": "square" });
              myAnimation.paint();
            }
          }
        }
  
        function services_line2() {
  
          if (document.readyState === 'complete') {
            if ($('#servicesline2').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#servicesline2');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 20.7, "strokeOpacity": 1, "strokeColor": "#d61616", "strokeCap": "square" });
              myAnimation.paint();
            }
          }
        }
  
        function services_line3() {
  
          if (document.readyState === 'complete') {
            if ($('#services-line3').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#services-line3');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 10.7, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        };
  
        function testimonial_line1() {
  
          if (document.readyState === 'complete') {
            if ($('#testimonials-line1').length) {
              /**
               * Setup your Lazy Line element.
               * see README file for more settings
               */
  
              let el = document.querySelector('#testimonials-line1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 10.7, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }
  
      });
    }, 100);
   

  }

  componentDidCatch(error, info) {
    // log the error to our server with loglevel
    log.error({ error, info });
  }

  testimonialText(temp_string) {
    // let temp_string = this.props.testimonial.testimonial_text
    let data_return_string = ""
    let temp_array = temp_string.split(' ');
    for (let i = 0; i <= 11; i++) {
      data_return_string = data_return_string + temp_array[i] + " "
    }
    data_return_string = data_return_string + "..."
    return (
      data_return_string
    )
  }

  render() {
    /* 
      following code controls the testimonials string to be 
      shown on the page. It makes substring from 0 to 69 characters and 70th character 
      it adds "..." in it. 
    */

    //    this.props.homes.testimonial.map((testi, i) => {

    //     //console.log(post);
    //     let add1 = testi.testimonial_content;
    //     // console.log(add1);

    //     //  console.log(add1.length);

    //      var res = add1.substring(0, 69);
    //     //  console.log(res + "...");

    //      this.state.testimonial.push(res+ "...");
    //      //console.log(this.state.testimonial)


    //  }) 
    return (
      <>

        <Layout_Light menus={this.props.HeaderMenu} page={this.props.Pages} social_icons={this.props.SocialIcons} accquinated={this.props.accquinated} associations={this.props.FooterAssociations} output={this.props.FooterContact} cookie={this.props.cookieWeb}
        >
          <Head>
            <meta property="og:title" content="White label Digital agency in india | Software development, mobile app, web development outsourcing company in india - AugustCode." />
            <meta property="og:description" content="AugustCode is best White label digital agency and one of the leading outsourcing company in India offering Software, Mobile application, Web development." />
            {/*Canonical Tag Start*/}
            <link rel="canonical" href={'https://www.augustinfotech.com'} />
            {/*Canonical Tag End*/}
          </Head>
          <div>
            {/* site-main-content */}
            <div className="site-main-content ">
              {this.props.homes &&
              <section className="banner-section">
                <div className="main-slider">
                  <div className="item video" key={ this.props.homes && this.props.homes[0].id}>
                    <video autoPlay loop muted playsInline className="slide-video slide-media" preload="metadata" poster={this.props.homes[0].Banner[0].url}>
                      <source src={this.props.homes[0].Banner[0].url} type="video/mp4" />
                    </video>
                    <div className="caption m19">
                      <p className="text-white custom-h3-heading" data-splitting>{this.props.homes[0].Title}</p>
                      <p className="text-white custom-h3-heading" data-splitting>{this.props.homes[0].Sub_Title}</p>
                      <h1 className="text-white custom-h1-heading" data-splitting>{this.props.homes[0].Short_description}</h1>
                    </div>
                  </div>
                  <div className="item image" key={this.props.homes[1].id}>
                    <figure>
                      <div>
                        <img className="slide-image slide-media" src={this.props.homes[0].Banner[1].url} />
                        <img data-lazy={this.props.homes[0].Banner[1].url} className="image-entity" />
                      </div>

                      <figcaption className="caption">
                        <p className="text-white custom-h3-heading h1" data-splitting>{this.props.homes[1].Title}</p>
                        <p className="text-white custom-h3-heading" data-splitting>{this.props.homes[1].Sub_Title}</p>
                        <h1 className="text-white custom-h1-heading" data-splitting>{this.props.homes[1].Short_description}</h1>

                      </figcaption>
                    </figure>
                  </div>

                  <div className="item image" key={this.props.homes[2].id}>
                    <figure>
                      <div>
                        <img className="slide-image slide-media" src={this.props.homes[0].Banner[2].url} />
                        <img data-lazy={this.props.homes[0].Banner[2].url} className="image-entity" />
                      </div>

                      <figcaption className="caption">
                        <p className="text-white custom-h3-heading" data-splitting>{this.props.homes[2].Title}</p>
                        <p className="text-white custom-h3-heading" data-splitting>{this.props.homes[2].Sub_Title}</p>
                        <h1 className="text-white custom-h1-heading" data-splitting>{this.props.homes[2].Short_description}</h1>

                      </figcaption>
                    </figure>
                  </div>
                </div>

                {/* <a className="contact-us-link" href="../contact">Contact Us</a>
          
          <a className="scroll-down" href="#about" /> */}
              </section>
  }
  {this.props.aboutus &&
              <section id="about" className="about-section section-gapping">
                <div className="container">
                  <div className="section-title">
                    <h3>{this.props.aboutus.title}</h3>
                    <span className="title-border">
                      <svg id="pattern" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="69.43" height="69.46" viewBox="0 0 69.43 69.46" data-llp-composed="true" className="lazy-line-painter">
                        <defs>
                          <style dangerouslySetInnerHTML={{ __html: "\n                              .cls-1 {\n                                fill: #fff;\n                                stroke: #d61616;\n                                stroke-miterlimit: 10;\n                              }\n                            " }} />
                        </defs>
                        <title>pattern</title>
                        <line className="cls-1" x1="0.35" y1="69.1" x2="69.08" y2="0.35" data-llp-id="pattern-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} style={{}} />
                      </svg>
                    </span>
                  </div>
                  <div className="about-content">
                    <h4 className="sub-title" data-splitting>{this.props.aboutus.sub_title}</h4>

                    <p>{this.props.aboutus.description}</p>
                    <ul className="list-with-bullet">

                      {this.props.aboutus.key_points.map((item) =>
                        <li key={item.id}>{item.key_points}</li>
                      )}

                    </ul>
                    <a className="read-more" href={this.props.aboutus.read_more_url}>Readmore</a>

                  </div>
                  <div className="scroll-down-wrapper">
                    <a className="scroll-down black" href="#services" />
                  </div>
                </div>
                <div className="about-trangle1">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 494 237" data-llp-composed="true" id="aboutTrangle1" className="lazy-line-painter"><title>aboutTrangle1</title><polyline points="139 0 0 237 494 237 0 237" style={{ fill: '#d61616', fillRule: 'evenodd' }} data-llp-id="aboutTrangle1-0" data-llp-duration={1500} data-llp-delay={0} fillOpacity={0} />
                  </svg></div>
                <div className="about-trangle2">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 251 291" data-llp-composed="true" id="abouttriangle22" className="lazy-line-painter"><title>about-triangle22</title><polygon points="0 0 251 130 0 291 0 291 251 130 0 0 0 0" style={{ fill: 'rgb(214, 22, 22)', fillRule: 'evenodd' }} data-llp-id="abouttriangle22-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} /></svg>
                </div>
              </section>
  }
              <section id="services" className="services-section section-gapping">
                <div className="container">
                  <div className="a-right">
                    <div className="section-title title-border-left">
                      <h3>Solutions</h3>
                      <span className="title-border">
                        <svg id="pattern1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="69.43" height="69.46" viewBox="0 0 69.43 69.46" data-llp-composed="true" className="lazy-line-painter">
                          <defs>
                            <style dangerouslySetInnerHTML={{ __html: "\n                                .cls-1 {\n                                  fill: #fff;\n                                  stroke: #d61616;\n                                  stroke-miterlimit: 10;\n                                }\n                              " }} />
                          </defs>
                          <title>pattern</title>
                          <line className="cls-1" x1="0.35" y1="69.1" x2="69.08" y2="0.35" data-llp-id="pattern1-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} style={{}} />
                        </svg>
                      </span>
                    </div>
                  </div>
                  <div className="services-content">
                    <div className="services-wrap clr">

                      {
                        this.props.solutions && this.props.solutions.map((solution) => {

                          {
                            if (solution.Prompt_homepage === true) {
                              return (

                                <div className="service" key={solution.id}>
                                  <h4 className="sub-title" data-splitting>{solution.Title}</h4>
                                  <h5>{solution.Sub_title}</h5>
                                  <p>{solution.Short_Description}</p>
                                  <a className="read-more" href={solution.Readmore_link}>Readmore</a>
                                </div>

                              )

                            }
                          }
                        })
                      }
                    </div>
                    <div className="scroll-down-wrapper">
                      <a href="#testimonials">
                        <img src="data:image/svg+xml,%3Csvg id='Loadmore' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='79' height='96' viewBox='0 0 79 96'%3E%3Cdefs%3E%3Cstyle%3E .cls-1 %7B filter: url(%23filter); %7D .cls-2 %7B fill: %23fff; %7D .cls-3 %7B font-size: 23px; font-family: 'Ubuntu-Regular'; %7D %3C/style%3E%3Cfilter id='filter' x='0' y='0' width='79' height='80' filterUnits='userSpaceOnUse'%3E%3CfeOffset result='offset' dy='5' in='SourceAlpha'/%3E%3CfeGaussianBlur result='blur' stdDeviation='2.236'/%3E%3CfeFlood result='flood' flood-color='%23ccc'/%3E%3CfeComposite result='composite' operator='in' in2='blur'/%3E%3CfeBlend result='blend' in='SourceGraphic'/%3E%3C/filter%3E%3C/defs%3E%3Ccircle class='cls-1' cx='39' cy='35' r='35'/%3E%3Cg id='arrow'%3E%3Crect class='cls-2' x='37' y='20' width='1' height='30'/%3E%3Crect class='cls-2' x='41' y='45' width='1' height='1'/%3E%3Crect id='Rectangle_15_copy' data-name='Rectangle 15 copy' class='cls-2' x='40' y='46' width='1' height='1'/%3E%3Crect id='Rectangle_15_copy_2' data-name='Rectangle 15 copy 2' class='cls-2' x='39' y='47' width='1' height='1'/%3E%3Crect id='Rectangle_15_copy_3' data-name='Rectangle 15 copy 3' class='cls-2' x='38' y='48' width='1' height='1'/%3E%3C/g%3E%3Ctext id='More' class='cls-3' x='11.108' y='95.999'%3EMore%3C/text%3E%3C/svg%3E%0A" className="load-more" />
                      </a>
                    </div>
                  </div>
                  <div className="services-line1">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621 358" data-llp-composed="true" id="servicesline1" className="lazy-line-painter"><title>services-line1</title><polygon points="0 0 621 358 619.87 358 0 0.17 0 0" style={{ fill: 'rgb(214, 22, 22)', fillRule: 'evenodd' }} data-llp-id="servicesline1-0" data-llp-duration={300} data-llp-delay={0} fillOpacity={0} /></svg>
                  </div>
                  <div className="services-right-top-line">
                    <div className="services-right-top-line-2">
                      <svg id="servicesright-top-line-2" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="280.39" height="280.39" viewBox="0 0 280.39 280.39" data-llp-composed="true" className="lazy-line-painter">
                        <defs>
                          <style dangerouslySetInnerHTML={{ __html: "\n                            .cls-1 {\n                              fill: #fff;\n                              stroke: #d61616;\n                              stroke-miterlimit: 10;\n                            }\n                          " }} />
                        </defs>
                        <title>services-right-top-line-2</title>
                        <line className="cls-1" x1="0.35" y1="280.03" x2="280.03" y2="0.35" data-llp-id="servicesright-top-line-2-0" data-llp-duration={900} data-llp-delay={0} fillOpacity={0} style={{}} />
                      </svg>
                    </div>
                    <div className="services-right-top-line-1">
                      <svg id="servicesright-top-line-1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="379.71" height="379.71" viewBox="0 0 379.71 379.71" data-llp-composed="true" className="lazy-line-painter">
                        <defs>
                          <style dangerouslySetInnerHTML={{ __html: "\n                          .cls-1 {\n                            fill: #fff;\n                            stroke: #d61616;\n                            stroke-miterlimit: 10;\n                          }\n                        " }} />
                        </defs>
                        <title>services-right-top-line-1</title>
                        <line className="cls-1" x1="0.35" y1="379.35" x2="379.35" y2="0.35" data-llp-id="servicesright-top-line-1-0" data-llp-duration={900} data-llp-delay={0} fillOpacity={0} style={{}} />
                      </svg>
                    </div>
                  </div>
                  <div className="services-left-bottom-line">
                    <div className="services-left-bottom-line-1">
                      <svg id="servicesleft-bottom-line-1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="160.4" height="160.39" viewBox="0 0 160.4 160.39" data-llp-composed="true" className="lazy-line-painter">
                        <defs>
                          <style dangerouslySetInnerHTML={{ __html: "\n                              .cls-1 {\n                                fill: #fff;\n                                stroke: #d61616;\n                                stroke-miterlimit: 10;\n                              }\n                            " }} />
                        </defs>
                        <title>services-left-bottom-line-1</title>
                        <line className="cls-1" x1="0.35" y1="160.03" x2="160.04" y2="0.35" data-llp-id="servicesleft-bottom-line-1-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} style={{}} />
                      </svg>
                    </div>
                    <div className="services-left-bottom-line-2">
                      <svg id="servicesleft-bottom-line-2" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="250.4" height="251.39" viewBox="0 0 250.4 251.39" data-llp-composed="true" className="lazy-line-painter">
                        <defs>
                          <style dangerouslySetInnerHTML={{ __html: "\n                              .cls-1 {\n                                fill: #fff;\n                                stroke: #d61616;\n                                stroke-miterlimit: 10;\n                              }\n                            " }} />
                        </defs>
                        <title>services-left-bottom-line-2</title>
                        <line className="cls-1" x1="0.35" y1="251.03" x2="250.04" y2="0.35" data-llp-id="servicesleft-bottom-line-2-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} style={{}} />
                      </svg>
                    </div>
                  </div>
                </div>
                <div className="services-triangle">
                  <div className="services-triangle1"><img src="/static/images/services-triangle1.svg" alt="services-triangle1" /></div>
                  <div className="services-line2">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 621 358" data-llp-composed="true" id="servicesline2" className="lazy-line-painter"><title>services-line2</title><polygon points="0 0 621 358 619.87 358 0 0.17 0 0" style={{ fill: 'rgb(214, 22, 22)', fillRule: 'evenodd' }} data-llp-id="servicesline2-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} /></svg>
                  </div>
                </div>
                <div className="services-line3">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 358.41 224.97" data-llp-composed="true" id="services-line3" className="lazy-line-painter"><title>services-line3</title><line x1="0.27" y1="224.54" x2="358.15" y2="0.42" style={{ fill: 'none', strokeMiterlimit: 10 }} data-llp-id="services-line3-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} data-llp-stroke-cap data-llp-stroke-join /></svg>
                </div>
              </section>


              <section id="testimonials" className="testimonials-section section-gapping">
                <div className="container">
                  <div className="section-title">
                    <h3>Testimonials</h3>
                    <span className="title-border">
                      <svg id="pattern2" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="69.43" height="69.46" viewBox="0 0 69.43 69.46" data-llp-composed="true" className="lazy-line-painter">
                        <defs>
                          <style dangerouslySetInnerHTML={{ __html: "\n                            .cls-1 {\n                              fill: #fff;\n                              stroke: #d61616;\n                              stroke-miterlimit: 10;\n                            }\n                          " }} />
                        </defs>
                        <title>pattern</title>
                        <line className="cls-1" x1="0.35" y1="69.1" x2="69.08" y2="0.35" data-llp-id="pattern2-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} style={{}} />
                      </svg>
                    </span>
                  </div>


                  <div className="row testimonials-wrap testimonials-wrap1">
                    {this.props.testimonial && this.props.testimonial.map((testimonials) => {
                      {
                        if (testimonials.Promot_homepage === true) {
                          //here i is for displaying index of testimonial
                          i = i += 1
                          return (

                            <div className={i % 2 ? "col col-4 col-md-4" : "col col-4 col-md-4 bg-red"}
                              key={testimonials.id}>
                              <div className="testimonial-block">
                                {/* <div className="user-img"><img src={this.props.page[5].banner_image[0].url} alt="Testimonial" /></div> */}
                                <h4 className="sub-title" data-splitting>{testimonials.Title}</h4>
                                <p className="testimonial-content">{this.testimonialText(testimonials.testimonial_text)}</p>
                                <a className="read-more" href="/testimonials">Readmore</a>
                                <div className="testi-traingle1" />
                                <div className="testi-traingle2" />
                              </div>
                            </div>

                          )
                        }
                      }

                    })
                    }
                  </div>
                </div>
                <div className="testimonials-line1">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 358.41 224.97" data-llp-composed="true" id="testimonials-line1" className="lazy-line-painter"><title>services-line3</title><line x1="0.27" y1="224.54" x2="358.15" y2="0.42" style={{ fill: 'none', strokeMiterlimit: 10 }} data-llp-id="testimonials-line1-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} data-llp-stroke-cap data-llp-stroke-join /></svg>
                </div>
                <div className="testimonials-triangle1"><img src="/static/images/testimonials-triangle1.svg" alt="testimonials-triangle1" /></div>
              </section>

            </div>
            <Loader />
          </div>
        </Layout_Light>
      </>
    )
  }
}
