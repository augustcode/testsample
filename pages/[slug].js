import React, { Component } from 'react';
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import fetch from 'isomorphic-unfetch';
import Solution from '../components/Solution'
import Contact from '../components/Contact';
// import Testimonial from '../components/Testimonial'
import Testinomial from '../components/Testimonial';
import CaseStudyList from '../components/Casestudy';
import Blogs from '../components/Blogs'
import About from '../components/About';
import Design from '../components/Design';
import NotFound from './404';


export async function getServerSideProps(context) {
  const { slug } = context.query
  const res = await fetch(`${API_URL}/pages?slug=${slug}`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const temp = await res.json()


  // About APi
  const aboutStory = await fetch(`${API_URL}/about-story-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const about = await aboutStory.json()

  const aboutTeam = await fetch(`${API_URL}/about-team-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const about_team = await aboutTeam.json()

  const aboutMission = await fetch(`${API_URL}/about-mission-vision-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const about_mission = await aboutMission.json()

  const aboutBios = await fetch(`${API_URL}/bios`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const about_bios = await aboutBios.json()

  // end about api

  // CaseStudy APi

  const Case = await fetch(`${API_URL}/case-studies`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const Case_Study = await Case.json();

  // End CaseStudy APi

  // Contact APi

  const contact = await fetch(`${API_URL}/contact-us-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const contact_block = await contact.json();

  // End Contact APi

  // Cookies APi

  const CookieWeb = await fetch(`${API_URL}/Web-settings`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    })
  const Web_settings = await CookieWeb.json();
  // End Cookies APi

  // Solution APi

  const solutionInfos = await fetch(`${API_URL}/solution-infos`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    })
  const Solution_Infos = await solutionInfos.json();

  const solutionBlock = await fetch(`${API_URL}/solution-how-we-do-it-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    })
  const Solution_Block = await solutionBlock.json();

  const solutionEngage = await fetch(`${API_URL}/solution-how-to-engage-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    })
  const Solution_Engage = await solutionEngage.json();

  // End Solution APi


  // Testimonial APi

  const testimonials = await fetch(`${API_URL}/testimonials`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    })
  const Testimonial_data = await testimonials.json();
  // End Testimonial APi


  // Blog Api

  const Count = await fetch(`${API_URL}/blogs/count`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const Blog_Count = await Count.json();

  const blogsLimit = await fetch(`${API_URL}/blogs?_limit=${Blog_Count}`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const Blogs_Limit = await blogsLimit.json();


   // End Blog Api

  // Footer Api

  const headerMenus = await fetch(`${API_URL}/menus`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const header_Menu = await headerMenus.json()

  const pages_data = await fetch(`${API_URL}/pages`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const page_data = await pages_data.json()


  const socialIcons = await fetch(`${API_URL}/social-icons`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const social_Icons = await socialIcons.json()

  const footerBlock = await fetch(`${API_URL}/get-acquanited-footer-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_Block = await footerBlock.json()

  const footerAssociations = await fetch(`${API_URL}/associations`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_associations = await footerAssociations.json()

  const footerContact = await fetch(`${API_URL}/contact-infos`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_contact = await footerContact.json()


  // Pass data to the page via props
  return {
    props: {
      data: temp, query: context.query, caseStudy: Case_Study, ContactBlock: contact_block, cookieWeb: Web_settings, AboutBlock: about, AboutTeam: about_team, AboutMission: about_mission, AboutBios: about_bios, SolutionInfos: Solution_Infos, SolutionBlock: Solution_Block, SolutionEngage: Solution_Engage, Testimonial: Testimonial_data, BlogCount: Blog_Count, BlogsLimit:Blogs_Limit,
      // BlogData:Blogtemp,
      Pages: page_data, HeaderMenu: header_Menu, SocialIcons: social_Icons, FooterBlock: footer_Block, FooterAssociations: footer_associations, FooterContact: footer_contact
    }
  }
}

// render(){
export default function Pages({ data, query, caseStudy, ContactBlock, cookieWeb, AboutBlock, AboutTeam, AboutMission, AboutBios, SolutionInfos, SolutionBlock, SolutionEngage, Testimonial, BlogCount,BlogsLimit, Pages, HeaderMenu, SocialIcons, FooterBlock, FooterAssociations, FooterContact }) {
  if (data[0]?.slug === "about") {
    return (
      <>
        <About path={data[0]} story={AboutBlock} team={AboutTeam} block={AboutMission} bios={AboutBios} page={Pages} menus={HeaderMenu} social_icons={SocialIcons} accquinated={FooterBlock} associations={FooterAssociations} output={FooterContact} cookie={cookieWeb}
        />
      </>
    )
  }
  else if (data[0]?.slug === "blog") {
    return (
      <>
        <Blogs path={data[0]}
          //  Blog_pages= {BlogData} 
          Blog_count={BlogCount} blogData={BlogsLimit} page={Pages} menus={HeaderMenu} social_icons={SocialIcons} accquinated={FooterBlock} associations={FooterAssociations} output={FooterContact} cookie={cookieWeb}
          query={data[0]}
        />
      </>
    )
  }
  else if (data[0]?.slug === "solutions") {
    return (
      <>
        <Solution path={data[0]} services={SolutionInfos} solution={SolutionBlock} engage={SolutionEngage} page={Pages} menus={HeaderMenu} social_icons={SocialIcons} accquinated={FooterBlock} associations={FooterAssociations} output={FooterContact} cookie={cookieWeb} />
      </>
    )
  }
  else if (data[0]?.slug === "testimonials") {
    return (
      <>
        <Testinomial path={data[0]} testimonial={Testimonial} page={Pages} menus={HeaderMenu} social_icons={SocialIcons} accquinated={FooterBlock} associations={FooterAssociations} output={FooterContact} cookie={cookieWeb} />
        {/* <Testimonial path={data[0]} testimonial={Testimonial} /> */}
      </>
    )
  }
  else if (data[0]?.slug === "contact") {
    return (
      <>
        <Contact path={data[0]} contact_us={ContactBlock} page={Pages} menus={HeaderMenu} social_icons={SocialIcons} accquinated={FooterBlock} associations={FooterAssociations} output={FooterContact} cookie={cookieWeb} />
      </>
    )
  }
  else if (data[0]?.slug === "case_study") {
    return (
      <>
        <CaseStudyList path={data[0]} detail={caseStudy} page={Pages} menus={HeaderMenu} social_icons={SocialIcons} accquinated={FooterBlock} associations={FooterAssociations} output={FooterContact} cookie={cookieWeb} />
      </>
    )
  }
  else if (data[0]?.slug === query.slug) {
    return (
      <>
        <Design path={data[0]} page={Pages} menus={HeaderMenu} social_icons={SocialIcons} accquinated={FooterBlock} associations={FooterAssociations} output={FooterContact} cookie={cookieWeb} />
      </>
    )
  }
  else {
    return (
      <NotFound />
    )
  }
}





