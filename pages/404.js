import Link from "next/link"
import React,{Component} from "react"


export default class NotFound  extends Component{


  render(){

  
    return (
      <>
        <div className='page-error'>
            <section className='section page-error-page'>
              <div className='section-inner'>
                <div className='container'>
                  <div className='not-found-content'>
                    <div className='not-found'>
                      <h1>404</h1>
                      <p>The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place.</p>   
                        <a rel="canonical" href={'https://www.augustinfotech.com'}> Go Back </a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
        </div>
      </>
      
    )


  }
}
