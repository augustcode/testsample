import React, { Component } from 'react';
import Head from "next/head"
import Layout_Dark from '../../Layout/layout_dark';
import Link from "next/link";
import Error from 'next/error';
import fetch from 'isomorphic-unfetch';
import { API_URL } from "../../next.config"
import { API_JWT_Token } from "../../next.config"
import Router from 'next/router'
import axios from "axios"
import blog_recommendation from '../../components/BlogRecommendation'

export async function getServerSideProps(context) {
  const { slug } = context.query
  const res = await fetch(`${API_URL}/blogs?slug=${slug}`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );

  // const blog = await fetch(`${API_URL}/blogs`
  //   , {
  //     headers: {
  //       'Authorization': `Bearer ${API_JWT_Token}`
  //     }

  //   }
  // );

  const headerMenus = await fetch(`${API_URL}/menus`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const header_Menu = await headerMenus.json()

  const pages_data = await fetch(`${API_URL}/pages`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const page_data = await pages_data.json()

  const socialIcons = await fetch(`${API_URL}/social-icons`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const social_Icons = await socialIcons.json()

  const footerBlock = await fetch(`${API_URL}/get-acquanited-footer-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_Block = await footerBlock.json()

  const footerAssociations = await fetch(`${API_URL}/associations`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_associations = await footerAssociations.json()

  const footerContact = await fetch(`${API_URL}/contact-infos`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_contact = await footerContact.json()

  const CookieWeb = await fetch(`${API_URL}/Web-settings`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    })
  const Web_settings = await CookieWeb.json();
  const temp = await res.json()

  // const blog_recommendation = await blog.json()

  // let max = blog.length
  // const n = 3;
  // let Tems_1 = []
  // let arr = []
  // do {
  //   // Generating random number
  //   const randomNumber = Math.floor(Math.random() * max) + 1

  //   // Pushing into the array only 
  //   // if the array does not contain it
  //   if (!arr.includes(randomNumber)) {
  //     arr.push(randomNumber);
  //   }
  // } while (arr.length < n);
  // for (let i = 0; i <= 2; i++) {
  //   if (arr[i] !== 0) {
  //     arr[i] = arr[i] - 1;

  //   }
  //   else if (arr[i] === 0) {
  //     arr[i] = 0
  //   }
  // }
  // let temp1 = arr
  // for (let k = 0; k <= 2; k++) {
  //   if (temp1[k] !== undefined) {
  //     Tems_1.push(blog.data[temp1[k]])
  //   }
  // }
  // Pass data to the page via props
  return { props: { blogs: temp, Pages: page_data, query: context.query, HeaderMenu: header_Menu, SocialIcons: social_Icons, FooterBlock: footer_Block, FooterAssociations: footer_associations, FooterContact: footer_contact, cookieWeb: Web_settings } }
}


export default class Blogdetail extends Component {

  componentDidMount() {
    $(document).ready(function () {
      var controller = new ScrollMagic.Controller();
      $(".setanime").each(function () {
        var tl = new TimelineMax();
        var cov = $(this).find(".reveal-block__black");
        var cov2 = $(this).find(".reveal-block__orange");
        var img = $(this).find(".blog-detail-img");

        tl
          .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
          .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
          .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left" }, "-=1.2")
          .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
          .from(img, 1, { opacity: 0 }, "reveal");

        var scene = new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.7
        })
          .setTween(tl)
          //.addIndicators()
          .addTo(controller);
      });

      var titleTxtAnim = new TimelineMax();

      titleTxtAnim
        .staggerFromTo('.section-title h3, .about-section .caption-text p, .section-title .custom-h3-heading', 0.5, {
          xPercent: 100,
          autoAlpha: 0,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, {
          xPercent: 0,
          autoAlpha: 1,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, 0.2);

      var blogdetailTxtAnim = new TimelineMax();

      blogdetailTxtAnim
        .staggerFromTo('.about-section .contentwrap *, .blogwrap .tags, .blogwrap .share-blog', 0.5, {
          xPercent: 100,
          autoAlpha: 0,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, {
          xPercent: 0,
          autoAlpha: 1,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, 0.2);

      var scene = new ScrollMagic.Scene({
        triggerElement: '.about-section .contentwrap *, .blogwrap .tags, .blogwrap .share-blog',
        triggerHook: 0.7
      })
        .setTween(blogdetailTxtAnim)
        //.addIndicators()
        .addTo(controller);
    });
  }

  UNSAFE_componentWillReceiveProps() {
    // $(document).ready(function(){
    setTimeout(() => {
      var controller = new ScrollMagic.Controller();
      // $(".setanime").each(function() {
      //   var tl = new TimelineMax();
      //   var cov = $(this).find(".reveal-block__black");
      //   var cov2 = $(this).find(".reveal-block__orange");
      //   var img = $(this).find(".blog-detail-img");

      //   tl
      //     .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
      //     .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
      //     .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left"}, "-=1.2" )
      //     .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
      //     .from(img, 1, { opacity: 0 }, "reveal");

      //   var scene = new ScrollMagic.Scene({
      //     triggerElement: this,
      //     triggerHook: 0.7
      //   })
      //     .setTween(tl)
      //     .addTo(controller);
      // });

      var titleTxtAnim = new TimelineMax();

      titleTxtAnim
        .staggerFromTo('.section-title h3, .about-section .caption-text p, .section-title .custom-h3-heading', 0.5, {
          xPercent: 100,
          autoAlpha: 0,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, {
          xPercent: 0,
          autoAlpha: 1,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, 0.2);

      var blogdetailTxtAnim = new TimelineMax();

      blogdetailTxtAnim
        .staggerFromTo('.about-section .contentwrap *, .blogwrap .tags, .blogwrap .share-blog', 0.5, {
          xPercent: 100,
          autoAlpha: 0,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, {
          xPercent: 0,
          autoAlpha: 1,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, 0.2);

      var scene = new ScrollMagic.Scene({
        triggerElement: '.about-section .contentwrap *, .blogwrap .tags, .blogwrap .share-blog',
        triggerHook: 0.7
      })
        .setTween(blogdetailTxtAnim)
        //.addIndicators()
        .addTo(controller);
      // });
    }, 1000);
  }
  dispBlogDate(blogDate) {
    if (blogDate) {
      let day = (new Date(blogDate)).toLocaleDateString('en-US', { day: 'numeric' });
      let year = (new Date(blogDate)).toLocaleDateString('en-US', { year: 'numeric' });
      let month = (new Date(blogDate)).toLocaleDateString('en-US', { month: 'long' });
      return (<>{month}{' '}<span>{day}{','} {year}</span></>)
    }
  }

  BlogDate(blogDate) {
    if (blogDate) {
      let day = (new Date(blogDate)).toLocaleDateString('en-US', { day: 'numeric' });
      let year = (new Date(blogDate)).toLocaleDateString('en-US', { year: 'numeric' });
      let month = (new Date(blogDate)).toLocaleDateString('en-US', { month: 'short' });
      return (<>{day}<span>{month} {' '} {year}</span></>)
    }
  }
  blogProperty(property, type) {
    if (property != '') {
      let BlogProp = property.split(',')
      if (BlogProp) {
        return (
          <>
            <section className="tags col-lg-12">
              <h4>{type == 'category' ? 'CATEGORIES' : 'TAGS'}</h4>
              {BlogProp.map((value, i) =>
                <a key={i}>{value}</a>
              )}
            </section>
          </>
        )
      }
    }
  }

  data(data) {
    let arr = [];
    let temp = this.props?.blogs && this.props?.blogs[0]?.seo_info?.meta
    if (temp !== undefined) {
      temp.map((el) => {
        arr.push(
          <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
      })
      return arr
    }

  }
  render() {

    return (

      <Layout_Dark menus={this.props.HeaderMenu} page={this.props.Pages} social_icons={this.props.SocialIcons} accquinated={this.props.FooterBlock} associations={this.props.FooterAssociations} output={this.props.FooterContact} cookie={this.props.cookieWeb}

        seo_info={this.props?.blogs[0].seo_info}
        meta={this.data(this.props?.blogs[0].seo_info)}
      >


        {/* site-main-content */}
        <div className="site-main-content">
          {/* about page top section */}
          <section id="about-id" className="about-section about-page-section section-gapping featureListBlog">
            <div className="ml-5 arrow-go-back-div mt-3 mb-4" onClick={() => Router.back()}>
              <a className="ml-1 arrow-go-back"><img src="/static/images/left-arrow.png" alt="blogdetail" /></a>Go Back
            </div>
            <div className="container">
              <div className="section-title col-lg-12">
                <p className="custom-h3-heading" style={{ opacity: 0, fontSize: '3rem', lineHeight: '120%' }}>
                  <span dangerouslySetInnerHTML={{ __html: this.props?.blogs[0] && this.props?.blogs[0].Title }} />
                </p>
                <div className="mt-5 mb-4"><span className="blogdate">  {this.dispBlogDate(this.props?.blogs[0] && this.props?.blogs[0].blog_date)} | {this.props?.blogs[0].blog_author}</span></div>
              </div>
              <div className="caption-text">
                <div className="col col-lg-12 offset-0">
                  {/* <p dangerouslySetInnerHTML={{__html: this.props?.blogs.BlogExcerpt}} /> */}
                </div>
              </div>
              <section className="blogwrap">
                {/*BLOG LISTING*/}
                <div className="mt-5 col-lg-12">
                  <figure className="text-center setanime">

                    <div className="reveal-block reveal-block__black" />
                    <div className="reveal-block reveal-block__orange" />

                    {/* {
                  this.props.blogs[0].blog_banner[0]!=='undefined'&&
                  <img className="blog-detail-img" src={!this.props.blogs[0].blog_banner[0] || this.props.blogs[0].blog_banner[0]== 'undefined' ? "" : this.props?.blogs[0].blog_banner[0].url} alt={this.props?.blogs[0].Title} />
                 } */}

                    <img className="blog-detail-img" src={!this.props?.blogs[0].blog_banner[0] || this.props?.blogs[0].blog_banner[0] == 'undefined' ? "" : this.props?.blogs[0].blog_banner[0].url} alt={this.props?.blogs[0].Title} />

                  </figure>

                </div>
                {/*BLOG LISTING*/}
                <section className="blogcont mt-5 col-lg-12 contentwrap">

                  {/* <h1 className="mt-4 mb-4 custom-h2-heading">
                    <span dangerouslySetInnerHTML={{__html: this.props?.blogs.BlogTitle}} />
                </h1> */}
                  <div className="blog-content" dangerouslySetInnerHTML={{ __html: this.props?.blogs[0] && this.props?.blogs[0].body }} />
                </section>
                {/*TAGS*/}


                <section className="tags col-lg-12">

                  <h4>CATEGORIES</h4>
                  {this.props?.blogs[0].blog_categories.map((item, i) => {
                    return (
                      <a key={i}>{item.Title}</a>
                    )
                  })}
                </section>



                {this.props?.blogs[0].blog_tag !== "null" && this.blogProperty(this.props?.blogs[0].blog_tag, 'blog')}
                {/*SHARE THIS BLOG*/}
                <section className="share-blog col-lg-12">
                  <h4>SHARE THIS BLOG</h4>
                  <nav>
                    <a href={'https://www.facebook.com/sharer/sharer.php?u=https://ai-nextjs-frontend.herokuapp.com/blog/' + this.props?.blogs[0].slug + '/&t=' + this.props?.blogs[0].slug} target="_blank" rel="nofollow"><img src="/static/images/facebook-logo.svg" alt="Facebook" /></a>
                    <a href={'https://api.whatsapp.com/send?text=%0ahttps://ai-nextjs-frontend.herokuapp.com/blog/' + this.props?.blogs[0].slug} target="_blank" className="ai-whatsup" rel="nofollow"><img src="/static/images/whatsapp.svg" alt="Whatsapp" /></a>
                    <a href={'https://twitter.com/intent/tweet?url=https://ai-nextjs-frontend.herokuapp.com/blog/' + this.props?.blogs[0].slug + '/&amp;text=' + this.props?.blogs[0].slug + '&amp;tw_p=tweetbutton&amp;url=https://ai-nextjs-frontend.herokuapp.com/blog/' + this.props?.blogs[0].slug} target="_blank" rel="nofollow"><img src="/static/images/twitter-logo.svg" alt="Twitter" /></a>
                    <a href={'https://pinterest.com/pin/create/button/?url=https://ai-nextjs-frontend.herokuapp.com/blog/' + this.props?.blogs[0].slug + '/&amp;media=' + this.props?.blogs[0].blog_banner.url + '&amp;description=' + this.props?.blogs[0].slug} target="_blank" rel="nofollow"><img src="/static/images/pinterest-logo.svg" alt="PInterest" /></a>
                    <a href={'https://www.linkedin.com/shareArticle?mini=true&url=https://ai-nextjs-frontend.herokuapp.com/blog/' + this.props?.blogs[0].slug + '/&amp;summary=&amp;source=August Infotech' + this.props?.blogs[0].slug} target="_blank" rel="nofollow"><img src="/static/images/linkedin-logo.svg" alt="LinkedIn" /></a>
                  </nav>
                </section>
              </section>
            </div>
          </section>
          {/* about page top section */}
        </div>
        <a className="contact-us-link contact-us-link-white-bg" href="/contact">Contact Us</a>
        {/* <div className="container">
          <BlogRecommendation currentBlogId={this.props.blogs.BlogID} currentBlogCategories={this.props.blogs.BlogCategory} currentBlogTags={this.props.blogs.BlogTag}></BlogRecommendation>
        </div> */}
      </Layout_Dark>




    )
  }
}

