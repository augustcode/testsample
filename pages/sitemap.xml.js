import React from "react";
import * as fs from "fs";
const Sitemap = () => {
  return null;
};

export const getServerSideProps = async ({res}) => {

  const staticPaths = fs
    .readdirSync("pages")
    .filter((staticPage) => {
      return ![
        "api",
        "product",
        "_app.js",
        "_document.js",
        "404.js",
        "sitemap.xml.js",
        "index.js",
        "newblog.js",
        "[slug].js",
        "startbootstrap-creative-gh-pages.zip"
      ].includes(staticPage);
    })
    .map((staticPagePath) => {
      return `${process.env.WEBSITE_URL}/${staticPagePath}`;
    });

    const temp = await fetch(`${process.env.API_URL}/pages`
    ,{
      headers:{
        'Authorization' : `Bearer ${process.env.API_JWT_Token}`
    }    
    }  
    );
    const pages= await temp.json()

    const pagePaths = pages.map( page => {
        return `${process.env.WEBSITE_URL}/${page.slug}`
    })

    const temp1 = await fetch(`${process.env.API_URL}/case-studies`

    , {
      headers: {
        'Authorization': `Bearer ${process.env.API_JWT_Token}`
      }
    }
  );
  // let temp = await res.json();
  let casestudy = await temp1.json();

  const case_studyPaths = casestudy.map( case_study => {
    return `${process.env.WEBSITE_URL}/case_study/${case_study.slug}`
})

const blog = await fetch(`${process.env.API_URL}/blogs?_limit=-1`
, {
  headers: {
    'Authorization': `Bearer ${process.env.API_JWT_Token}`
  }

}
);
const blog_recommendation = await blog.json()

const blogPaths = blog_recommendation.map( blog => {
    return `${process.env.WEBSITE_URL}/blog/${blog.slug}`
})

  const allPaths = [...staticPaths, ...pagePaths, ...case_studyPaths, ...blogPaths ];

  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      ${allPaths
        .map((url) => {
          return `
            <url>
              <loc>${url}</loc>
              <lastmod>${new Date().toISOString()}</lastmod>
              <changefreq>monthly</changefreq>
              <priority>1.0</priority>
            </url>
          `;
        })
        .join("")}
    </urlset>
  `;

  res.setHeader("Content-Type", "text/xml");
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
};

export default Sitemap;