import React, { Component } from 'react';
import Head from "next/head"
import { API_URL } from "../../next.config"
import { API_JWT_Token } from "../../next.config"
import Layout_Dark from '../../Layout/layout_dark';
import Error from 'next/error';
import fetch from 'isomorphic-unfetch';
import Router from 'next/router'

export async function getServerSideProps(context) {
  // Fetch data from external API
  const { slug } = context.query
  const res = await fetch(`${API_URL}/case-studies?slug=${slug}`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  let casestudy = await res.json();

  if (casestudy.error && res) {
    res.statusCode = 404;
  }
  // Pass data to the page via props

  const headerMenus = await fetch(`${API_URL}/menus`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const header_Menu = await headerMenus.json()

  const pages_data = await fetch(`${API_URL}/pages`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    }
  );
  const page_data = await pages_data.json()


  const socialIcons = await fetch(`${API_URL}/social-icons`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const social_Icons = await socialIcons.json()

  const footerBlock = await fetch(`${API_URL}/get-acquanited-footer-block`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_Block = await footerBlock.json()

  const footerAssociations = await fetch(`${API_URL}/associations`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_associations = await footerAssociations.json()

  const footerContact = await fetch(`${API_URL}/contact-infos`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }
    }
  );
  const footer_contact = await footerContact.json()

  const CookieWeb = await fetch(`${API_URL}/Web-settings`
    , {
      headers: {
        'Authorization': `Bearer ${API_JWT_Token}`
      }

    })
  const Web_settings = await CookieWeb.json();

  return { props: { query: context.query, case_study: casestudy[0], Pages: page_data, HeaderMenu: header_Menu, SocialIcons: social_Icons, FooterBlock: footer_Block, cookieWeb: Web_settings, FooterAssociations: footer_associations, FooterContact: footer_contact } }
}

function data(case_study) {
  let arr = [];
  let temp = case_study?.seo_info?.meta
  if (temp !== undefined) {
    temp.map((el) => {
      arr.push(
        <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
    })
    return arr
  }

}


export default class Casestudy extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (

      <>
        <Layout_Dark menus={this.props.HeaderMenu} page={this.props.Pages} social_icons={this.props.SocialIcons} accquinated={this.props.FooterBlock} associations={this.props.FooterAssociations} output={this.props.FooterContact} cookie={this.props.cookieWeb}
          seo_info={this.props.case_study?.seo_info}
          meta={data()}
        >
          <div className="case-banner" style={{ backgroundImage: `url(${this.props.case_study.banner_image.url})` }} />
          {/* <img src={this.props.case_study.banner_image.url} /> */}
          <section className="casestudyWrap casedetail">
            <section id="about-id" className="about-section about-page-section featureListBlog">
              <div className="container">
                <div className="col col-12  col-lg-12 mt-5">
                  <figcaption>
                    <div className="section-title">
                      <h1 className="custom-h2-heading mb-3">{this.props.case_study.Title}</h1>
                    </div>
                    <h2 className="custom-h1-heading">{this.props.case_study.Short_description}</h2>
                    {
                      this.props.case_study.challenges.length > 0 &&

                      <div className="row mt-5">

                        <div className="col col-12 col-lg-3">

                          <h3>The challenges:</h3>
                        </div>
                        <div className="col col-12 col-lg-9"
                          dangerouslySetInnerHTML={{ __html: this.props.case_study.challenges }}>
                        </div>
                      </div>

                    }
                    {
                      this.props.case_study.solution.length > 0 &&
                      <div className="row mt-5">
                        <div className="col col-12 col-lg-3">
                          <h3>The solution:</h3>
                        </div>
                        <div className="col col-12 col-lg-9"
                          dangerouslySetInnerHTML={{ __html: this.props.case_study.solution }} >
                        </div>
                      </div>
                    }

                    {
                      this.props.case_study.Outcome.length > 0 &&
                      <div className="row mt-5">
                        <div className="col col-12 col-lg-3">
                          <h3>The Outcome:</h3>
                        </div>
                        <div className="col col-12 col-lg-9"
                          dangerouslySetInnerHTML={{ __html: this.props.case_study.Outcome }}>
                        </div>
                      </div>
                    }
                    {
                      this.props.case_study.NextSteps.length > 0 &&
                      <div className="row mt-5">
                        <div className="col col-12 col-lg-3">
                          <h3>Next Steps:</h3>
                        </div>
                        <div className="col col-12 col-lg-9"
                          dangerouslySetInnerHTML={{ __html: this.props.case_study.NextSteps }}>
                        </div>
                      </div>
                    }
                    {
                      this.props.case_study.Technologies_FrameworkUsed.length > 0 &&

                      <div className="row mt-5">
                        <div className="col col-12 col-lg-3">
                          <h3>Technologies/Framework used:</h3>
                        </div>
                        <div className="col col-12 col-lg-9"
                          dangerouslySetInnerHTML={{ __html: this.props.case_study.Technologies_FrameworkUsed }}>
                        </div>
                      </div>
                    }

                    <div className="row mt-5">
                      <div className="col col-12 col-lg-3">
                      </div>
                      <div className="col col-12 col-lg-9">
                        {/*SHARE THIS BLOG*/}
                        <section className="share-blog">
                          <h4>SHARE THIS CASESTUDY</h4>
                          <nav>
                            <a href={'https://www.facebook.com/sharer/sharer.php?u=https://ai-nextjs-frontend.herokuapp.com/case_study/' + this.props.case_study.slug + '/&t=' + this.props.case_study.slug} target="_blank" rel="nofollow"><img src="/static/images/facebook-logo.svg" alt="Facebook" /></a>
                            <a href={'https://api.whatsapp.com/send?text=%0ahttps://ai-nextjs-frontend.herokuapp.com/case_study/' + this.props.case_study.slug} target="_blank" className="ai-whatsup" rel="nofollow"><img src="/static/images/whatsapp.svg" alt="Whatsapp" /></a>
                            <a href={'https://twitter.com/intent/tweet?url=https://ai-nextjs-frontend.herokuapp.com/case_study/' + this.props.case_study.slug + '/&amp;text=' + this.props.case_study.slug + '&amp;tw_p=tweetbutton&amp;url=https://ai-nextjs-frontend.herokuapp.com/case_study/' + this.props.case_study.slug} target="_blank" rel="nofollow"><img src="/static/images/twitter-logo.svg" alt="Twitter" /></a>
                            <a href={'https://pinterest.com/pin/create/button/?url=https://ai-nextjs-frontend.herokuapp.com/case_study/' + this.props.case_study.slug + '/&amp;media=' + this.props.case_study.slug + '&amp;description=' + this.props.case_study.slug} target="_blank" rel="nofollow"><img src="/static/images/pinterest-logo.svg" alt="PInterest" /></a>
                            <a href={'https://www.linkedin.com/shareArticle?mini=true&url=https://ai-nextjs-frontend.herokuapp.com/case_study/' + this.props.case_study.slug + '/&amp;summary=&amp;source=August Infotech' + this.props.case_study.slug} target="_blank" rel="nofollow"><img src="/static/images/linkedin-logo.svg" alt="LinkedIn" /></a>
                          </nav>
                        </section>
                      </div>
                    </div>
                  </figcaption>
                </div>
              </div>
            </section>
          </section>
          <a className="contact-us-link contact-us-link-white-bg" href="/contact">Contact Us</a>
        </Layout_Dark>
      </>
    )
  }
}