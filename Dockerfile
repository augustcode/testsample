FROM node:14-alpine

RUN mkdir /app/ && chown -R node:node /app

WORKDIR /app

COPY --chown=node:node package.json .
COPY --chown=node:node package-lock.json .

USER node

# RUN npm install && npm cache clean --force --loglevel=error
RUN npm ci --only=production

COPY --chown=node:node . .

ENV NODE_ENV production

RUN npm run build

EXPOSE 8080

CMD ["npm", "start"]
