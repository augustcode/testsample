// var basicAuth = require('basic-auth-connect');

const express = require('express')
const path = require('path')
const next = require('next')
const compression = require('compression')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const redirects = [
  { from: '/website-testing-using-selenium-automation-tool', to: '/blog/website-testing-using-selenium-automation-tool'},
  { from: '/7-tips-for-cold-calling-success', to: '/blog/7-tips-for-cold-calling-success'},
  { from: '/team-dysfunctions-5-dysfunctions-of-a-team-summary', to: '/blog/team-dysfunctions-5-dysfunctions-of-a-team-summary'},
  { from: '/scrum-effective-way-implement-agile', to: '/blog/scrum-effective-way-implement-agile'},
  { from: '/promote-website-using-social-media', to: '/blog/promote-website-using-social-media'},
  { from: '/materialize-design-css-framework', to: '/blog/materialize-design-css-framework'},
  { from: '/paypal-express-checkout-with-recurring-payment', to: '/blog/paypal-express-checkout-with-recurring-payment'},
  { from: '/cross-domain-file-upload', to: '/blog/cross-domain-file-upload'},
  { from: '/hr-activity-march-2015', to: '/blog/hr-activity-march-2015'},
  { from: '/lunch-at-sankalp', to: '/blog/lunch-at-sankalp'},
  { from: '/birthday-celebration-2015', to: '/blog/birthday-celebration-2015'},
  { from: '/launch-of-archtiko-theme', to: '/blog/launch-of-archtiko-theme'},
  { from: '/hr-event', to: '/blog/hr-event'},
  { from: '/seminar-on-how-to-build-effective-website', to: '/blog/seminar-on-how-to-build-effective-website'},
  { from: '/picnic-at-orsang-resort', to: '/blog/picnic-at-orsang-resort'},
  { from: '/christmas-celebration', to: '/blog/christmas-celebration'},
  { from: '/wordpress-seminar-harsh-mehta', to: '/blog/wordpress-seminar-harsh-mehta'},
  { from: '/blogging-for-small-businesses', to: '/blog/blogging-for-small-businesses'},
  { from: '/insidear-2014', to: '/blog/insidear-2014'},
  { from: '/diwali-celebrations', to: '/blog/diwali-celebrations'},
  { from: '/solution-of-twitter-api-error-401-error-timestamp-out-of-bounds-2', to: '/blog/solution-of-twitter-api-error-401-error-timestamp-out-of-bounds-2'},
  { from: '/is-there-any-plugin-to-set-custom-image-size-for-different-post-types-thumbnail-or-how-to-set-it-2', to: '/blog/is-there-any-plugin-to-set-custom-image-size-for-different-post-types-thumbnail-or-how-to-set-it-2'},
  { from: '/how-to-remove-child-selection-when-adding-or-editing-taxonomies-for-custom-post-type-2', to: '/blog/how-to-remove-child-selection-when-adding-or-editing-taxonomies-for-custom-post-type-2'},
  { from: '/secure-your-wordpress-blog-with-clef-for-wordpress-login-security-2', to: '/blog/secure-your-wordpress-blog-with-clef-for-wordpress-login-security-2'},
  { from: '/8-things-to-do-to-promote-your-latest-post', to: '/blog/8-things-to-do-to-promote-your-latest-post'},
  { from: '/3-website-design-mistakes-that-drives-customers-away', to: '/blog/3-website-design-mistakes-that-drives-customers-away'},
  { from: '/10-benefits-of-erp-system-in-an-organization-for-growth-of-your-business', to: '/blog/10-benefits-of-erp-system-in-an-organization-for-growth-of-your-business'},
  { from: '/be-the-change', to: '/blog/be-the-change'},
  { from: '/13-ways-to-keep-your-wordpress-site-safe', to: '/blog/13-ways-to-keep-your-wordpress-site-safe'},
  { from: '/technical-sales-why-it-works-in-the-information-technology-field', to: '/blog/technical-sales-why-it-works-in-the-information-technology-field'},
  { from: '/machine-learning-artificial-intelligence-major-roleplaying-winning-business', to: '/blog/machine-learning-artificial-intelligence-major-roleplaying-winning-business'},
  { from: '/how-to-automate-business-processes-with-erp-system', to: '/blog/how-to-automate-business-processes-with-erp-system'},
  { from: '/top-6-reasons-why-there-are-bugs-in-website', to: '/blog/top-6-reasons-why-there-are-bugs-in-website'},
  { from: '/what-are-instant-apps-how-will-change-mobile-dev-world', to: '/blog/what-are-instant-apps-how-will-change-mobile-dev-world'},
  { from: '/shopify-vs-bigcommerce-best-hosted-ecommerce-platform', to: '/blog/shopify-vs-bigcommerce-best-hosted-ecommerce-platform'},
  { from: '/augmented-reality-applications-future-mobile-apps', to: '/blog/augmented-reality-applications-future-mobile-apps'},
  { from: '/5-digital-marketing-trends-look-2019', to: '/blog/5-digital-marketing-trends-look-2019'},
  { from: '/7-essential-tips-outsourcing-app-development', to: '/blog/7-essential-tips-outsourcing-app-development'},
  { from: '/5-android-mobile-application-development-trends-will-dominate-2019', to: '/blog/5-android-mobile-application-development-trends-will-dominate-2019'},
  { from: '/big-data-analytics-can-improve-erp-system', to: '/blog/big-data-analytics-can-improve-erp-system'},
  { from: '/10-things-know-gutenberg-wordpress-editor', to: '/blog/10-things-know-gutenberg-wordpress-editor'},
  { from: '/iot-devices-impacting-mobile-app-development', to: '/blog/iot-devices-impacting-mobile-app-development'},
  { from: '/mobile-app-security-important', to: '/blog/mobile-app-security-important'},
  { from: '/manual-testing-vs-automation-testing-superior-choice', to: '/blog/manual-testing-vs-automation-testing-superior-choice'},
  { from: '/closer-look-erp-system-example', to: '/blog/closer-look-erp-system-example'},
  { from: '/outsource-web-development-7-ways-benefits-business', to: '/blog/outsource-web-development-7-ways-benefits-business'},
  { from: '/use-drupal-5-benefits-make-best-cms', to: '/blog/use-drupal-5-benefits-make-best-cms'},
  { from: '/3-wordpress-security-plugins-prevent-hacking', to: '/blog/3-wordpress-security-plugins-prevent-hacking'},
  { from: '/5-ux-web-design-points-create-website', to: '/blog/5-ux-web-design-points-create-website'},
  { from: '/crm-2019-features-need-crm-functionality', to: '/blog/crm-2019-features-need-crm-functionality'},
  { from: '/3-benefits-big-data-business-an-advantage', to: '/blog/3-benefits-big-data-business-an-advantage'},
  { from: '/5-data-analytics-trends-look-out-in-2019', to: '/blog/5-data-analytics-trends-look-out-in-2019'},
  { from: '/python-vs-php-which-programming-language-is-best-and-app', to: '/blog/python-vs-php-which-programming-language-is-best-and-app'},
  { from: '/how-to-take-ecommerce-personalization-next-level', to: '/blog/how-to-take-ecommerce-personalization-next-level'},
  { from: '/benefits-of-white-labeling-partners-in-web', to: '/blog/benefits-of-white-labeling-partners-in-web'},
  { from: '/how-to-optimize-your-website-for-voice-search', to: '/blog/how-to-optimize-your-website-for-voice-search'},
  { from: '/top-5-tips-to-increase-your-it-sales', to: '/blog/top-5-tips-to-increase-your-it-sales'},
  { from: '/driving-sales-the-importance-of-social-selling', to: '/blog/driving-sales-the-importance-of-social-selling'},
  { from: '/the-future-of-cloud-computing-predictions-for-coming-years', to: '/blog/the-future-of-cloud-computing-predictions-for-coming-years'},
  { from: '/why-angular-5-reasons-why-angular-is-most-popular-framework', to: '/blog/why-angular-5-reasons-why-angular-is-most-popular-framework'},
  { from: '/exploring-the-integration-of-intelligent-agents-in-web-applications', to: '/blog/exploring-the-integration-of-intelligent-agents-in-web-applications'},
  { from: '/how-iot-is-used-in-the-it-industry', to: '/blog/how-iot-is-used-in-the-it-industry'},
  { from: '/top-5-predictions-for-the-future-of-deep-learning', to: '/blog/top-5-predictions-for-the-future-of-deep-learning'},
  { from: '/what-are-the-5-key-concepts-of-react-native', to: '/blog/what-are-the-5-key-concepts-of-react-native'},
  { from: '/vue-js-advantages', to: '/blog/vue-js-advantages'},
  { from: '/artificial-intelligence-goes-online-expert-guide-use-ai-e-commerce', to: '/blog/artificial-intelligence-goes-online-expert-guide-use-ai-e-commerce'},
  { from: '/world-two-realities-exploring-mixed-reality-real-life-applications', to: '/blog/world-two-realities-exploring-mixed-reality-real-life-applications'},
  { from: '/introduction-to-blockchain-technology-7-important-things-you-need-to-know', to: '/blog/introduction-to-blockchain-technology-7-important-things-you-need-to-know'},
  { from: '/the-future-of-digital-marketing-5-powerful-reasons-you-must-embrace-online-video-marketing', to: '/blog/the-future-of-digital-marketing-5-powerful-reasons-you-must-embrace-online-video-marketing'},
  { from: '/how-to-double-your-sales-with-email-marketing', to: '/blog/how-to-double-your-sales-with-email-marketing'},
  { from: '/how-ar-is-changing-the-world-of-consumer-marketing', to: '/blog/how-ar-is-changing-the-world-of-consumer-marketing'},
  { from: '/the-importance-of-referrals-in-sales', to: '/blog/the-importance-of-referrals-in-sales'},
  { from: '/the-7-fundamental-features-to-raise-mobile-app-sales', to: '/blog/the-7-fundamental-features-to-raise-mobile-app-sales'},
  { from: '/how-iot-is-changing-the-food-retail-industry', to: '/blog/how-iot-is-changing-the-food-retail-industry'},
  { from: '/top-trends-in-mobile-app-development-in-2019', to: '/blog/top-trends-in-mobile-app-development-in-2019'},
  { from: '/important-optimize-images-ecommerce-website', to: '/blog/important-optimize-images-ecommerce-website'},
  { from: '/wordpress-review-wordpress-best-choice-website', to: '/blog/wordpress-review-wordpress-best-choice-website'},
  { from: '/increase-sales-using-social-media', to: '/blog/increase-sales-using-social-media'},
  { from: '/ecommerce-platform-right-business-shopify-wooc-magento', to: '/blog/ecommerce-platform-right-business-shopify-wooc-magento'},
  { from: '/choose-best-front-end-framework-website-angular-js-react-js-vue-js', to: '/blog/choose-best-front-end-framework-website-angular-js-react-js-vue-js'},
  { from: '/5-ai-trends-that-are-shaping-the-future-of-sales', to: '/blog/5-ai-trends-that-are-shaping-the-future-of-sales'},
  { from: '/how-to-use-drupal-8-your-guide-to-all-of-the-new-features77984-2', to: '/blog/how-to-use-drupal-8-your-guide-to-all-of-the-new-features77984-2'},
  { from: '/apps-make-money-6-tactics-know', to: '/blog/apps-make-money-6-tactics-know'},
  { from: '/users-lose-interest-demand-mobile-app', to: '/blog/users-lose-interest-demand-mobile-app'},
  { from: '/3-undeniable-benefits-of-push-notifications-in-digital-marketing', to: '/blog/3-undeniable-benefits-of-push-notifications-in-digital-marketing'},
  { from: '/magento-ecommerce-platform-perfect-solution-sell-online', to: '/blog/magento-ecommerce-platform-perfect-solution-sell-online'},
  { from: '/hottest-mobile-game-development-trends-2019', to: '/blog/hottest-mobile-game-development-trends-2019'},
  { from: '/wordpress-update-5-2-3-security-maintenance-release', to: '/blog/wordpress-update-5-2-3-security-maintenance-release'},
  { from: '/the-race-to-the-first-page-how-to-boost-your-google-keyword-ranking', to: '/blog/the-race-to-the-first-page-how-to-boost-your-google-keyword-ranking'},
  { from: '/integrating-the-internet-of-things-with-web-development', to: '/blog/integrating-the-internet-of-things-with-web-development'},
  { from: '/mobile-app-personalization-how-artificial-intelligence-is-in-the-drivers-seat', to: '/blog/mobile-app-personalization-how-artificial-intelligence-is-in-the-drivers-seat'},
  { from: '/3-reasons-ui-design-improves-conversion-rates', to: '/blog/3-reasons-ui-design-improves-conversion-rates'},
  { from: '/reduce-e-commerce-bounce-rates-with-personalization', to: '/blog/reduce-e-commerce-bounce-rates-with-personalization'},
  { from: '/top-4-future-trends-seo', to: '/blog/top-4-future-trends-seo'},
  { from: '/what-makes-a-website-effective-3-reasons-ui-design-improves-conversion-rates', to: '/blog/what-makes-a-website-effective-3-reasons-ui-design-improves-conversion-rates'},
  { from: '/this-is-the-product-death-cycle-why-it-happens-and-how-to-break-out-of-it', to: '/blog/this-is-the-product-death-cycle-why-it-happens-and-how-to-break-out-of-it'},
  { from: '/the-creation-of-issuemag-theme-episode-2', to: '/blog/the-creation-of-issuemag-theme-episode-2'},
  { from: '/the-creation-of-issuemag-theme-episode-1', to: '/blog/the-creation-of-issuemag-theme-episode-1'},
  { from: '/sneak-peak-of-our-new-product-archtiko-wordpress-theme', to: '/blog/sneak-peak-of-our-new-product-archtiko-wordpress-theme'},
  { from: '/significance-of-responsive-design-in-wordpress-theme', to: '/blog/significance-of-responsive-design-in-wordpress-theme'},
  { from: '/revamp-your-fashion-blog-using-our-fashionmagpro-theme', to: '/blog/revamp-your-fashion-blog-using-our-fashionmagpro-theme'},
  { from: '/pillars-of-inbound-marketing-that-no-one-ever-taught-you-infographic', to: '/blog/pillars-of-inbound-marketing-that-no-one-ever-taught-you-infographic'},
  { from: '/mobile-app-personalization-how-artificial-intelligence-is-in-the-drivers-seat-2', to: '/blog/mobile-app-personalization-how-artificial-intelligence-is-in-the-drivers-seat-2'},
  { from: '/latest-trends-in-magazine-publishing', to: '/blog/latest-trends-in-magazine-publishing'},
  { from: '/how-to-properly-maintain-your-wordpress-website-or-blog', to: '/blog/how-to-properly-maintain-your-wordpress-website-or-blog'},
  { from: '/how-to-create-issue-based-magazine-in-five-simple-steps-using-wordpress-core-features', to: '/blog/how-to-create-issue-based-magazine-in-five-simple-steps-using-wordpress-core-features'},
  { from: '/filesystem-api-create-files-store-locally-using-javascript-webkit', to: '/blog/filesystem-api-create-files-store-locally-using-javascript-webkit'},
  { from: '/check-it-out-our-free-theme-is-breaking-the-bars-at-wordpress-magazine-theme-charts', to: '/blog/check-it-out-our-free-theme-is-breaking-the-bars-at-wordpress-magazine-theme-charts'},
  { from: '/adding-six-social-media-posts-to-your-wordpress-blog', to: '/blog/adding-six-social-media-posts-to-your-wordpress-blog'},
  { from: '/8-bad-practices-of-wordpress-theme-development', to: '/blog/8-bad-practices-of-wordpress-theme-development'},
  { from: '/5-tips-to-optimize-your-wordpress-website', to: '/blog/5-tips-to-optimize-your-wordpress-website'},
  { from: '/5-reasons-for-using-premium-wordpress-magazine-theme', to: '/blog/5-reasons-for-using-premium-wordpress-magazine-theme'},
  { from: '/5-quick-tips-to-promote-your-premium-wordpress-theme', to: '/blog/5-quick-tips-to-promote-your-premium-wordpress-theme'},
  { from: '/5-benefits-of-using-issue-based-magazine-plugin', to: '/blog/5-benefits-of-using-issue-based-magazine-plugin'},
  { from: '/3-ways-you-can-use-manage-issue-based-magazine-plugin', to: '/blog/3-ways-you-can-use-manage-issue-based-magazine-plugin'},
  { from: '/3-point-strategy-on-how-to-make-your-online-magazine', to: '/blog/3-point-strategy-on-how-to-make-your-online-magazine'},
  { from: '/14-things-to-consider-before-buying-a-wordpress-magazine-theme', to: '/blog/14-things-to-consider-before-buying-a-wordpress-magazine-theme'},
  { from: '/10-responsive-web-design-principles-directly-from-our-designer-hub', to: '/blog/10-responsive-web-design-principles-directly-from-our-designer-hub'},
  { from: '/10-reasons-to-choose-wordpress-for-your-next-website', to: '/blog/10-reasons-to-choose-wordpress-for-your-next-website'} 
]

app.prepare()
  .then(() => {
    const server = express()
    server.use(compression())
    // if (process.env.NODE_ENV !== 'production') {
    //   server.use(basicAuth('augustcode', 'augustcode@2019'));
    // }

    // sitemap and robots txt file access start
    const sitemapOptions = {
      root: __dirname + '/',
      headers: {
        'Content-Type': 'text/xml;charset=UTF-8'
      }
    };

    server.get('/sitemap.xml', (req, res) => res.status(200).sendFile('sitemap.xml', sitemapOptions));

    const robotsOptions = {
      root: __dirname + '/',
      headers: {
        'Content-Type': 'text/plain;charset=UTF-8'
      }
    };

    server.get('/robots.txt', (req, res) => res.status(200).sendFile('robots.txt', robotsOptions));

    const bingOptions = {
      root: __dirname + '/',
      headers: {
        'Content-Type': 'text/xml;charset=UTF-8'
      }
    };
  
    server.get('/BingSiteAuth.xml', (req, res) => res.status(200).sendFile('BingSiteAuth.xml', bingOptions));

    const googleCode = {
        root: __dirname + '/',
        headers: {
            'Content-Type': 'text/html;charset=UTF-8'
        }
    };
  
    server.get('/googlee0d62b1254628b81.html', (req, res) => res.status(200).sendFile('googlee0d62b1254628b81.html', googleCode));
    // sitemap and robots txt file access end

    server.use("/static", express.static(__dirname + "/static", {
      maxAge: "365d"
    }));

    redirects.forEach(({ from, to, type = 301, method = 'get' }) => {
      server[method](from, (req, res) => {
        res.redirect(type, to)
      })
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(port, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })
  });
