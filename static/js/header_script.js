function createAndAppendScriptTags() {
    var scriptsArray = [
        // {
        //     "path": "/static/js/jquery-2.2.4.min.js",
        //     "async": true,
        //     "defer": true

        // },
        // {
        //     "path": "/static/js/popper.min.js",
        //     "async": true,
        //     "defer": true
        // },
        // {
        //     "path": "/static/js/bootstrap.min.js",
        //     "async": true,
        //     "defer": true
        // },
        // {
        //     "path": "/static/js/slick.min.js",
        //     "async": true,
        //     "defer": true
        // },
        // {
        //     "path": "/static/js/TweenMax.min.js",
        //     "async": true,
        //     "defer": true
        // },
        // {
        //     "path": "/static/js/ScrollMagic.min.js",
        //     "async": true,
        //     "defer": true
        // },
        // {
        //     "path": "/static/js/animation.gsap.js"
        // },
        // {
        //     "path": "/static/js/debug.addIndicators.js"
        // },
        {
            "path": "/static/js/MorphSVGPlugin.min.js",
            "defer": true
        },
        {
            "path": "/static/js/splitting.js",
            "defer": true,
        },
        {
            "path": "/static/js/anime.min.js",
            "async": true,
        },
        {
            "path": "https://cdn.jsdelivr.net/npm/lazy-line-painter@1.9.4/lib/lazy-line-painter-1.9.4.min.js",
            "defer": true
        },
        {
            "path": "/static/js/imagesloaded.pkgd.min.js",
            "defer": true
        },
        {
            "path": "/static/js/three.min.js",
            "defer": true
        },
        {
            "path": "/static/js/player.js",
            "defer": true
        },
        {
            "path": "/static/js/tilt.jquery.js",
            "defer": true
        },
        {
            "path": "/static/js/snap.svg-min.js",
            "defer": true
        }
    ];

    scriptsArray.map(function (data, index) {
        const scriptTag = document.createElement("script");
        scriptTag.src = data.path;
        if (data.async) {
            scriptTag.async = true
        }
        if (data.defer) {
            scriptTag.defer = true
        }
        document.head.appendChild(scriptTag);
    })
}
createAndAppendScriptTags();