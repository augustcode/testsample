import React, { Component } from 'react';
import Head from "next/head";
import Layout_Dark from '../Layout/layout_dark';
export default class Design extends Component {

    constructor(props) {
        super(props)
        this.state = {
            page: [],
            design: []
        }
    };
    data() {

        let arr = [];
        let temp = this.props.path?.seo_info?.meta
        if (temp !== undefined) {
            temp.map((el) => {
                arr.push(
                    <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
            })
            return arr
        }

    }
    render() {

        return (
            <>
                <Layout_Dark menus={this.props.menus} page={this.props.page} social_icons={this.props.social_icons} accquinated={this.props.accquinated} associations={this.props.associations} output={this.props.output} cookie={this.props.cookie}
                    seo_info={this.props.path?.seo_info}
                    meta={this.data()}
                >
                    <div className="container">
                        <div className="cms-pages">
                            <h1 className="text-center">{this.props.path.page_title}</h1>
                            <h2>{this.props.path.sub_title}</h2>
                            <style jsx>{`
                        .pages{
                            margin:0px
                        }`}</style>
                            <div className="cms-pages pages" dangerouslySetInnerHTML={{ __html: this.props.path.page_body }}>

                            </div>
                        </div>
                    </div>
                    <a className="contact-us-link contact-us-link-white-bg" href="/contact">Contact Us</a>
                </Layout_Dark>
            </>

        )

    }
}
