import React, { Component } from 'react';
import Head from "next/head";
import axios from "axios"
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import Layout_Light from "../Layout/layout_light"

export default class About extends Component {

  constructor(props) {
    super(props)
    this.state = {
      block: [],
      teams: [],
      bios: [],
      page: null,
      team_data: [],
      videoPlayed: false,

    }
  }
  async componentDidMount() {
    let max = this.props.bios.length
    const n = 6;
    let Tems_1 = []
    let arr = []
    do {
      // Generating random number
      const randomNumber = Math.floor(Math.random() * max) + 1

      // Pushing into the array only 
      // if the array does not contain it
      if (!arr.includes(randomNumber)) {
        arr.push(randomNumber);
      }
    } while (arr.length < n);
    for (let i = 0; i <= 5; i++) {
      if (arr[i] !== 0) {
        arr[i] = arr[i] - 1;

      }
      else if (arr[i] === 0) {
        arr[i] = 0
      }
    }
    let temp1 = arr
    for (let k = 0; k <= 5; k++) {
      if (temp1[k] !== undefined) {
        Tems_1.push(this.props.bios[temp1[k]])
      }
    }
    this.setState({ team_data: Tems_1 })

    setTimeout(() => {
      $(document).ready(function () {
        //HIDE LOADER...DISTORTION EFEECT STARTS HERE
        var hoverEffect = function (opts) {
          var vertex = `
                      varying vec2 vUv;
                      void main() {
                        vUv = uv;
                        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
                      }
                  `;

          var fragment = `
                      varying vec2 vUv;
              
                      uniform sampler2D texture;
                      uniform sampler2D texture2;
                      uniform sampler2D disp;
              
                      // uniform float time;
                      // uniform float _rot;
                      uniform float dispFactor;
                      uniform float effectFactor;
              
                      // vec2 rotate(vec2 v, float a) {
                      //  float s = sin(a);
                      //  float c = cos(a);
                      //  mat2 m = mat2(c, -s, s, c);
                      //  return m * v;
                      // }
              
                      void main() {
              
                          vec2 uv = vUv;
              
                          // uv -= 0.5;
                          // vec2 rotUV = rotate(uv, _rot);
                          // uv += 0.5;
              
                          vec4 disp = texture2D(disp, uv);
              
                          vec2 distortedPosition = vec2(uv.x + dispFactor * (disp.r*effectFactor), uv.y);
                          vec2 distortedPosition2 = vec2(uv.x - (1.0 - dispFactor) * (disp.r*effectFactor), uv.y);
              
                          vec4 _texture = texture2D(texture, distortedPosition);
                          vec4 _texture2 = texture2D(texture2, distortedPosition2);
              
                          vec4 finalTexture = mix(_texture, _texture2, dispFactor);
              
                          gl_FragColor = finalTexture;
                          // gl_FragColor = disp;
                      }
                  `;

          var parent = opts.parent || console.warn("no parent");
          var dispImage = opts.displacementImage || console.warn("displacement image missing");
          var image1 = opts.image1 || console.warn("first image missing");
          var image2 = opts.image2 || console.warn("second image missing");
          var intensity = opts.intensity || 1;
          var speedIn = opts.speedIn || 1.6;
          var speedOut = opts.speedOut || 1.2;
          var userHover = (opts.hover === undefined) ? true : opts.hover;
          var easing = opts.easing || Expo.easeOut;

          var mobileAndTabletcheck = function () {
            var check = false;
            (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
            return check;
          };

          var scene = new THREE.Scene();
          var camera = new THREE.OrthographicCamera(
            parent.offsetWidth / -2,
            parent.offsetWidth / 2,
            parent.offsetHeight / 2,
            parent.offsetHeight / -2,
            1,
            1000
          );

          camera.position.z = 1;

          var renderer = new THREE.WebGLRenderer({
            antialias: false,
            // alpha: true
          });

          renderer.setPixelRatio(window.devicePixelRatio);
          renderer.setClearColor(0xffffff, 0.0);
          renderer.setSize(parent.offsetWidth, parent.offsetHeight);
          parent.appendChild(renderer.domElement);

          // var addToGPU = function(t) {
          //     renderer.setTexture2D(t, 0);
          // };

          var loader = new THREE.TextureLoader();
          loader.crossOrigin = "";
          var texture1 = loader.load(image1);
          var texture2 = loader.load(image2);

          var disp = loader.load(dispImage);
          disp.wrapS = disp.wrapT = THREE.RepeatWrapping;

          texture1.magFilter = texture2.magFilter = THREE.LinearFilter;
          texture1.minFilter = texture2.minFilter = THREE.LinearFilter;

          texture1.anisotropy = renderer.getMaxAnisotropy();
          texture2.anisotropy = renderer.getMaxAnisotropy();

          var mat = new THREE.ShaderMaterial({
            uniforms: {
              effectFactor: { type: "f", value: intensity },
              dispFactor: { type: "f", value: 0.0 },
              texture: { type: "t", value: texture1 },
              texture2: { type: "t", value: texture2 },
              disp: { type: "t", value: disp }
            },

            vertexShader: vertex,
            fragmentShader: fragment,
            transparent: true,
            opacity: 1.0
          });

          var geometry = new THREE.PlaneBufferGeometry(
            parent.offsetWidth,
            parent.offsetHeight,
            1
          );
          var object = new THREE.Mesh(geometry, mat);
          scene.add(object);

          var addEvents = function () {
            var evtIn = "mouseenter";
            var evtOut = "mouseleave";
            if (mobileAndTabletcheck()) {
              evtIn = "touchstart";
              evtOut = "touchend";
            }
            parent.addEventListener(evtIn, function (e) {
              TweenMax.to(mat.uniforms.dispFactor, speedIn, {
                value: 1,
                ease: easing
              });
            });

            parent.addEventListener(evtOut, function (e) {
              TweenMax.to(mat.uniforms.dispFactor, speedOut, {
                value: 0,
                ease: easing
              });
            });
          };

          if (userHover) {
            addEvents();
          }

          window.addEventListener("resize", function (e) {
            renderer.setSize(parent.offsetWidth, parent.offsetHeight);
          });


          this.next = function () {
            TweenMax.to(mat.uniforms.dispFactor, speedIn, {
              value: 1,
              ease: easing
            });
          }

          this.previous = function () {
            TweenMax.to(mat.uniforms.dispFactor, speedOut, {
              value: 0,
              ease: easing
            });
          };

          var animate = function () {
            requestAnimationFrame(animate);

            renderer.render(scene, camera);
          };
          animate();
        };

        imagesLoaded(document.querySelectorAll('img'), () => {
          document.body.classList.remove('loading');
        });

        Array.from(document.querySelectorAll('.distortion-grid-item-img')).forEach((el) => {
          const imgs = Array.from(el.querySelectorAll('img'));
          setTimeout(() => {
            new hoverEffect({
              parent: el,
              intensity: el.dataset.intensity || undefined,
              speedIn: el.dataset.speedin || undefined,
              speedOut: el.dataset.speedout || undefined,
              easing: el.dataset.easing || undefined,
              hover: el.dataset.hover || undefined,
              image1: imgs[0].getAttribute('src'),
              image2: imgs[1].getAttribute('src'),
              displacementImage: el.dataset.displacement
            });
          }, 500)

        });
        //HIDE LOADER...DISTORTION EFEECT ENDS HERE

        /*********** about page banner section animation ****************/
        var controller = new ScrollMagic.Controller();
        var t11 = new TimelineMax();

        t11.staggerFromTo('.inner-page-banner h2, .inner-page-banner .custom-h2-heading', 0.9, {
          xPercent: -100,
          opacity: 0,
          ease: Elastic.easeOut.config(0.1, 0.5),
        },
          {
            xPercent: 0,
            opacity: 1,
            ease: Elastic.easeOut.config(0.1, 0.5)
          }, 1.9)

        var scene = new ScrollMagic.Scene({
          triggerElement: ".inner-page-banner"
        })
          .setTween(t11)
          .addTo(controller);
        /*********** about page banner section animation close ****************/

        /*********** about page top section animation ****************/
        var controller = new ScrollMagic.Controller();
        var t12 = new TimelineMax();

        t12.staggerFromTo(
          '.about-page-section p',
          1.0,
          { opacity: 0 },
          { opacity: 1 }
        )

          .staggerFromTo(
            '.video-wrap',
            1.0,
            { opacity: 0 },
            { opacity: 1 }
          )

          // .staggerFromTo(
          //   '.video-wrap',
          //   1.0,
          //   {scale: 0},
          //   {scale: 1}, 
          //   1
          // )

          .fromTo('#downtriangle', 0.2, { display: "none" }, {
            display: "block", onComplete: function () {
              downtriangle();
            }
          }, 0.2)

        var scene = new ScrollMagic.Scene({
          triggerElement: ".about-page-section"
        })
          .setTween(t12)
          .addTo(controller);
        /*********** about page top section animation close ****************/

        /*********** about page video section animation ****************/
        // var controller = new ScrollMagic.Controller();
        // var t9 = new TimelineMax();

        // t9.staggerFromTo(
        //   '.video-wrap',
        //   1.0,
        //   {scale: 0},
        //   {scale: 1}, 
        //   "+=2"
        // )

        // .fromTo('#downtriangle', 0.2,{display: "none"},{display: "block", onComplete:function(){
        //   downtriangle();
        // }}, 2)

        // var scene = new ScrollMagic.Scene({
        //   triggerElement: "#videoHome"
        // })
        // .setTween(t10)
        // .addTo(controller);
        /*********** about page video section animation close ****************/


        /*********** about page story section animation ****************/
        var controller = new ScrollMagic.Controller();
        var t10 = new TimelineMax();

        t10.staggerFromTo('.story-section .section-title h3', 0.5, {
          xPercent: -100,
          opacity: 0,
          ease: Elastic.easeOut.config(0.1, 0.5)
        },
          {
            xPercent: 0,
            opacity: 1,
            ease: Elastic.easeOut.config(0.1, 0.5)
          }, 0)

          .fromTo('.story-section .title-border svg', 0.2, { opacity: 0 }, {
            opacity: 1, onComplete: function () {
              story_pattern();
            }
          }, 0, "-=2")

          .fromTo('.story-line svg', 0.2, { display: "none" }, {
            display: "block", onComplete: function () {
              story_line();
            }
          }, 0, "-=6")

          .staggerFromTo(
            '.story-list',
            0.2,
            { opacity: 0 },
            { opacity: 1 },
            "-=2"
          )

          .staggerFromTo('.story-list li', 0.3, {
            xPercent: 70,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, "-=2")

          .staggerFrom('.story-list-details-wrap .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.01)

          .staggerFromTo('.story-list-detail-item p', 0.3, {
            xPercent: 70,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, "-=3")

        var scene = new ScrollMagic.Scene({
          triggerElement: "#story"
        })
          .setTween(t10)
          .addTo(controller);

        //story list
        $('.story-list').on('click', 'a', function (e) {
          e.preventDefault();
          e.stopPropagation();
          // setTimeout(function(){
          var businessListDetailItemWidth = $(".story-list-details-track").outerWidth();
          // $(".business-list-detail-item").css('width', businessListDetailItemWidth);
          var totalWidth = 0;
          $(".story-list-details-wrap").children().each(function () {
            totalWidth = totalWidth + $(this).outerWidth();
          });
          // $(".business-list-details-wrap").css('width', totalWidth);
          // }, 500);

          $('.story-list a').removeClass('active');
          $(this).addClass('active');

          $('.story-list-detail-item').removeClass('active');
          var activeMenuListItem = this.hash.slice(1);
          // console.log(activeMenuListItem);
          $('.story-list-detail-item').each(function () {
            var activeDetailListItem = $(this).attr('id');
            // console.log(activeDetailListItem);
            if (activeMenuListItem == activeDetailListItem) {
              $(this).addClass('active');
            }
          });

          var controller = new ScrollMagic.Controller();
          var t12 = new TimelineMax();
          t12.to('.story-list-detail-item', 0.1, {
            opacity: 0
          })

            .fromTo('.story-list-detail-item.active', 0.1, {
              opacity: 0
            },
              {
                opacity: 1
              })

            .staggerFrom('.story-list-details-wrap .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.01, "-=0.05")

            .staggerFromTo('.story-list-detail-item p', 1, {
              xPercent: 70,
              opacity: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            },
              {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.1, 0.75)
              }, 0.1, "-=0.05")


          var scene = new ScrollMagic.Scene({
            triggerElement: "#story"
          })
            .setTween(t12)
            .addTo(controller);

          return false;
        });

        /*********** about page story section animation close ****************/



        /*********** about page team section animation ****************/
        var controller = new ScrollMagic.Controller();
        var t13 = new TimelineMax();

        t13.staggerFromTo('.team-section .section-title h3', 0.9, {
          xPercent: -100,
          opacity: 0,
          ease: Elastic.easeOut.config(0.1, 0.5)
        },
          {
            xPercent: 0,
            opacity: 1,
            ease: Elastic.easeOut.config(0.1, 0.5)
          }, 0)

          .fromTo('.team-section .title-border svg', 0.5, { opacity: 0 }, {
            opacity: 1, onComplete: function () {
              team_pattern();
            }
          }, 0, "-=3")

          .fromTo('.team-line1 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              team_line1();
            }
          }, 0, "-=6")

          .staggerFromTo('.tilt2', 0.9, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.3)

          .fromTo('.team-trangle1 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              team_trangle1();
            }
          })

          .staggerFromTo('.team-trangle2 svg', 1.5, { display: "none" }, {
            display: "block", onComplete: function () {
              team_trangle2();
            }
          }, "-=2")

          .staggerFromTo('.member-name, .member-designation, .about-social-icons a, .allTeamWrap .grid__item', 0.2, {
            xPercent: 70,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          },
            {
              xPercent: 0,
              opacity: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.2, "-=3")

        var scene = new ScrollMagic.Scene({
          triggerElement: "#team"
        })
          .setTween(t13)
          .addTo(controller);


        function getRandomInt(min, max) {
          min = Math.ceil(min);
          max = Math.floor(max);
          return Math.floor(Math.random() * (max - min + 1)) + min;
        }


        /*********** about page team section animation close ****************/

        /*********** about page mission section animation ****************/

        var controller = new ScrollMagic.Controller();
        var t14 = new TimelineMax();

        t14.fromTo(
          '.mission-item',
          0.9,
          { scale: 0 },
          { scale: 1 }, "-=0.5"
        )

          .fromTo(
            '.mission-item-img',
            0.1,
            { scale: 0 },
            { scale: 1 }, "-=0.5"
          )

          .staggerFrom('.mission-item .sub-title .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.05, "+=0.1")

          .fromTo(
            '.mission-item p',
            0.2,
            { opacity: 0 },
            { opacity: 1 }, "-=0.5"
          )

          .fromTo('.mission-line1 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              mission_line1();
            }
          }, "-=0.5")

          .fromTo('.mission-line2 svg', 0.5, { display: "none" }, {
            display: "block", onComplete: function () {
              mission_line2();
            }
          }, "-=0.5")


        var scene = new ScrollMagic.Scene({
          triggerElement: "#mission"
        })
          .setTween(t14)
          .addTo(controller);
        /*********** about page mission section animation close ****************/

        /**************** SVG ANIMTION ***************/

        //aboutTrangle1
        function lazy1() {
          if (document.readyState === 'complete') {
            if ($('#aboutTrangle1').length) {
              let el = document.querySelector('#aboutTrangle1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeOutQuad", "strokeWidth": 20, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }

        //title border
        function pattern1() {

          if (document.readyState === 'complete') {
            if ($('#pattern').length) {
              let el = document.querySelector('#pattern');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1 });
              myAnimation.paint();
            }
          }
        }

        function pattern2() {

          if (document.readyState === 'complete') {
            if ($('#pattern').length) {
              let el = document.querySelector('#pattern');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1 });
              myAnimation.paint();
            }
          }
        }

        function services_right_top_line1() {
          if (document.readyState === 'complete') {
            if ($('#servicesright-top-line-1').length) {
              let el = document.querySelector('#servicesright-top-line-1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616", "delay": 0 });
              myAnimation.paint();
            }
          }
        }

        function services_right_top_line2() {

          if (document.readyState === 'complete') {
            if ($('#servicesright-top-line-2').length) {
              let el = document.querySelector('#servicesright-top-line-2');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616", "delay": 0 });
              myAnimation.paint();
            }
          }
        }

        function services_left_bottom_line1() {

          if (document.readyState === 'complete') {
            if ($('#servicesleft-bottom-line-1').length) {
              let el = document.querySelector('#servicesleft-bottom-line-1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }

        function services_left_bottom_line2() {

          if (document.readyState === 'complete') {
            if ($('#servicesleft-bottom-line-2').length) {
              let el = document.querySelector('#servicesleft-bottom-line-2');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }

        function services_line1() {

          if (document.readyState === 'complete') {
            if ($('#servicesline1').length) {
              let el = document.querySelector('#servicesline1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 20.7, "strokeOpacity": 1, "strokeColor": "#d61616", "strokeCap": "square" });
              myAnimation.paint();
            }
          }
        }

        function abouttriangle2() {
          if (document.readyState === 'complete') {
            if ($('#abouttriangle22').length) {
              let el = document.querySelector('#abouttriangle22');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 25, "strokeOpacity": 1, "strokeColor": "#d61616", "strokeCap": "square" });
              myAnimation.paint();
            }
          }
        }

        function services_line2() {

          if (document.readyState === 'complete') {
            if ($('#servicesline2').length) {
              let el = document.querySelector('#servicesline2');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 20.7, "strokeOpacity": 1, "strokeColor": "#d61616", "strokeCap": "square" });
              myAnimation.paint();
            }
          }
        }

        function testimonial_line1() {

          if (document.readyState === 'complete') {
            if ($('#testimonials-line1').length) {
              let el = document.querySelector('#testimonials-line1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 10.7, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }

        function services_line3() {

          if (document.readyState === 'complete') {
            if ($('#services-line3').length) {
              let el = document.querySelector('#services-line3');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 10.7, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        };

        function downtriangle() {

          if (document.readyState === 'complete') {
            if ($('#downtriangle').length) {
              let el = document.querySelector('#downtriangle');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 30, "strokeOpacity": 1, "strokeColor": "#d61616", "strokeCap": "square" });
              myAnimation.paint();
            }
          }

        }

        function story_line() {

          if (document.readyState === 'complete') {
            if ($('#story-line').length) {
              let el = document.querySelector('#story-line');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 10.7, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }

        function story_pattern() {

          if (document.readyState === 'complete') {
            if ($('#story-pattern').length) {
              let el = document.querySelector('#story-pattern');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1 });
              myAnimation.paint();
            }
          }
        }

        function team_pattern() {

          if (document.readyState === 'complete') {
            if ($('#team-pattern').length) {
              let el = document.querySelector('#team-pattern');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1 });
              myAnimation.paint();
            }
          }
        }

        function team_line1() {

          if (document.readyState === 'complete') {
            if ($('#teamline1').length) {
              let el = document.querySelector('#teamline1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 20.7, "strokeOpacity": 1, "strokeColor": "#d61616", "strokeCap": "square" });
              myAnimation.paint();
            }
          }
        }

        function team_trangle1() {
          if (document.readyState === 'complete') {
            if ($('#team-trangle1').length) {
              let el = document.querySelector('#team-trangle1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeOutQuad", "strokeWidth": 20, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }

        function team_trangle2() {
          if (document.readyState === 'complete') {
            if ($('#team-trangle2').length) {
              let el = document.querySelector('#team-trangle2');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeOutQuad", "strokeWidth": 20, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }

        function mission_line1() {

          if (document.readyState === 'complete') {
            if ($('#mission-line1').length) {
              let el = document.querySelector('#mission-line1');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 10.7, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        }

        function mission_line2() {

          if (document.readyState === 'complete') {
            if ($('#mission-line2').length) {
              let el = document.querySelector('#mission-line2');
              let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 10.7, "strokeOpacity": 1, "strokeColor": "#d61616" });
              myAnimation.paint();
            }
          }
        };
        /**************** SVG ANIMTION functions ***************/

        /***************************** menu js *********************/
        var $menu = $('.Menu-list'),
          $item = $('.Menu-list-item'),
          w = $(window).width(), //window width
          h = $(window).height(); //window height

        $(window).on('mousemove', function (e) {
          var offsetX = 0.5 - e.pageX / w, //cursor position X
            offsetY = 0.5 - e.pageY / h, //cursor position Y
            dy = e.pageY - h / 2, //@h/2 = center of poster
            dx = e.pageX - w / 2, //@w/2 = center of poster
            theta = Math.atan2(dy, dx), //angle between cursor and center of poster in RAD
            angle = theta * 180 / Math.PI - 90, //convert rad in degrees
            offsetPoster = $menu.data('offset'),
            transformPoster = 'translate3d(0, ' + -offsetX * offsetPoster + 'px, 0) rotateX(' + (-offsetY * offsetPoster) + 'deg) rotateY(' + (offsetX * (offsetPoster * 2)) + 'deg)'; //poster transform

          //get angle between 0-360
          if (angle < 0) {
            angle = angle + 360;
          }

          //poster transform
          $menu.css('transform', transformPoster);

          //parallax for each layer
          $item.each(function () {
            var $this = $(this),
              offsetLayer = $this.data('offset') || 0,
              transformLayer = 'translate3d(' + offsetX * offsetLayer + 'px, ' + offsetY * offsetLayer + 'px, 20px)';

            $this.css('transform', transformLayer);
          });
        });
        /***************************** menu js close *********************/

        //TITL.JS        
        const tilt = $('.tilt').tilt({
          glare: true,
          maxGlare: 0.3,
          maxTilt: 2
        });

        const tilt2 = $('.tilt2').tilt({
          glare: true,
          maxGlare: 0.3,
          maxTilt: 20
        });

        //TEAM POPUP

        /**
        * main.js
        * http://www.codrops.com
        *
        * Licensed under the MIT license.
        * http://www.opensource.org/licenses/mit-license.php
        * 
        * Copyright 2017, Codrops
        * http://www.codrops.com
        */
        {
          class Details {
            constructor() {
              this.DOM = {};

              const detailsTmpl = `
                    <div class="details__bg details__bg--up"></div>
                    <div class="details__bg details__bg--down"></div>
                    <img class="details__img" src="" alt="img 01"/>
                    <h2 class="details__title"></h2>
                    <div class="details__deco"></div>
                    <h3 class="details__subtitle"></h3>
                    <div class="details__price"></div>
                    <p class="details__description"></p>
                    <button class="details__addtocart">Add to cart</button>
                    <button class="details__close"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <button class="details__magnifier"><i class="fa fa-search-plus" aria-hidden="true"></i></button>
                    `;

              this.DOM.details = document.createElement('div');
              this.DOM.details.className = 'details';
              this.DOM.details.innerHTML = detailsTmpl;
              DOM.content.appendChild(this.DOM.details);
              this.init();
            }
            init() {
              this.DOM.bgUp = this.DOM.details.querySelector('.details__bg--up');
              this.DOM.bgDown = this.DOM.details.querySelector('.details__bg--down');
              this.DOM.img = this.DOM.details.querySelector('.details__img');
              this.DOM.title = this.DOM.details.querySelector('.details__title');
              this.DOM.deco = this.DOM.details.querySelector('.details__deco');
              this.DOM.subtitle = this.DOM.details.querySelector('.details__subtitle');
              this.DOM.price = this.DOM.details.querySelector('.details__price');
              this.DOM.description = this.DOM.details.querySelector('.details__description');
              this.DOM.cart = this.DOM.details.querySelector('.details__addtocart');
              this.DOM.close = this.DOM.details.querySelector('.details__close');
              this.DOM.magnifier = this.DOM.details.querySelector('.details__magnifier');

              this.initEvents();
            }
            initEvents() {
              this.DOM.close.addEventListener('click', () => this.isZoomed ? this.zoomOut() : this.close());
              this.DOM.magnifier.addEventListener('click', () => this.zoomIn());
            }
            fill(info) {
              this.DOM.img.src = info.img;
              this.DOM.title.innerHTML = info.title;
              this.DOM.deco.style.backgroundImage = `url(${info.img})`;
              this.DOM.subtitle.innerHTML = info.subtitle;
              this.DOM.price.innerHTML = info.price;
              this.DOM.description.innerHTML = info.description;
            }
            getProductDetailsRect() {
              return {
                productBgRect: this.DOM.productBg.getBoundingClientRect(),
                detailsBgRect: this.DOM.bgDown.getBoundingClientRect(),
                productImgRect: this.DOM.productImg.getBoundingClientRect(),
                detailsImgRect: this.DOM.img.getBoundingClientRect()
              };
            }
            open(data) {
              if (this.isAnimating) return false;
              this.isAnimating = true;

              this.DOM.details.classList.add('details--open');

              this.DOM.productBg = data.productBg;
              this.DOM.productImg = data.productImg;

              this.DOM.productBg.style.opacity = 0;
              this.DOM.productImg.style.opacity = 0;

              const rect = this.getProductDetailsRect();

              this.DOM.bgDown.style.transform = `translateX(${rect.productBgRect.left - rect.detailsBgRect.left}px) translateY(${rect.productBgRect.top - rect.detailsBgRect.top}px) scaleX(${rect.productBgRect.width / rect.detailsBgRect.width}) scaleY(${rect.productBgRect.height / rect.detailsBgRect.height})`;
              this.DOM.bgDown.style.opacity = 1;

              this.DOM.img.style.transform = `translateX(${rect.productImgRect.left - rect.detailsImgRect.left}px) translateY(${rect.productImgRect.top - rect.detailsImgRect.top}px) scaleX(${rect.productImgRect.width / rect.detailsImgRect.width}) scaleY(${rect.productImgRect.height / rect.detailsImgRect.height})`;
              this.DOM.img.style.opacity = 1;

              anime({
                targets: [this.DOM.bgDown, this.DOM.img],
                duration: (target, index) => index ? 800 : 250,
                easing: (target, index) => index ? 'easeOutElastic' : 'easeOutSine',
                elasticity: 250,
                translateX: 0,
                translateY: 0,
                scaleX: 1,
                scaleY: 1,
                complete: () => this.isAnimating = false
              });

              anime({
                targets: [this.DOM.title, this.DOM.deco, this.DOM.subtitle, this.DOM.price, this.DOM.description, this.DOM.cart, this.DOM.magnifier],
                duration: 600,
                easing: 'easeOutExpo',
                delay: (target, index) => {
                  return index * 60;
                },
                translateY: (target, index, total) => {
                  return index !== total - 1 ? [50, 0] : 0;
                },
                scale: (target, index, total) => {
                  return index === total - 1 ? [0, 1] : 1;
                },
                opacity: 1
              });

              anime({
                targets: this.DOM.bgUp,
                duration: 100,
                easing: 'linear',
                opacity: 1
              });

              anime({
                targets: this.DOM.close,
                duration: 250,
                easing: 'easeOutSine',
                translateY: ['100%', 0],
                opacity: 1
              });

              anime({
                targets: DOM.hamburger,
                duration: 250,
                easing: 'easeOutSine',
                translateY: [0, '-100%']
              });
            }
            close() {
              if (this.isAnimating) return false;
              this.isAnimating = true;

              this.DOM.details.classList.remove('details--open');

              anime({
                targets: DOM.hamburger,
                duration: 250,
                easing: 'easeOutSine',
                translateY: 0
              });

              anime({
                targets: this.DOM.close,
                duration: 250,
                easing: 'easeOutSine',
                translateY: '100%',
                opacity: 0
              });

              anime({
                targets: this.DOM.bgUp,
                duration: 100,
                easing: 'linear',
                opacity: 0
              });

              anime({
                targets: [this.DOM.title, this.DOM.deco, this.DOM.subtitle, this.DOM.price, this.DOM.description, this.DOM.cart, this.DOM.magnifier],
                duration: 20,
                easing: 'linear',
                opacity: 0
              });

              const rect = this.getProductDetailsRect();
              anime({
                targets: [this.DOM.bgDown, this.DOM.img],
                duration: 250,
                easing: 'easeOutSine',
                translateX: (target, index) => {
                  return index ? rect.productImgRect.left - rect.detailsImgRect.left : rect.productBgRect.left - rect.detailsBgRect.left;
                },
                translateY: (target, index) => {
                  return index ? rect.productImgRect.top - rect.detailsImgRect.top : rect.productBgRect.top - rect.detailsBgRect.top;
                },
                scaleX: (target, index) => {
                  return index ? rect.productImgRect.width / rect.detailsImgRect.width : rect.productBgRect.width / rect.detailsBgRect.width;
                },
                scaleY: (target, index) => {
                  return index ? rect.productImgRect.height / rect.detailsImgRect.height : rect.productBgRect.height / rect.detailsBgRect.height;
                },
                complete: () => {
                  this.DOM.bgDown.style.opacity = 0;
                  this.DOM.img.style.opacity = 0;
                  this.DOM.bgDown.style.transform = 'none';
                  this.DOM.img.style.transform = 'none';
                  this.DOM.productBg.style.opacity = 1;
                  this.DOM.productImg.style.opacity = 1;
                  this.isAnimating = false;
                }
              });
            }
            zoomIn() {
              this.isZoomed = true;

              anime({
                targets: [this.DOM.title, this.DOM.deco, this.DOM.subtitle, this.DOM.price, this.DOM.description, this.DOM.cart, this.DOM.magnifier],
                duration: 100,
                easing: 'easeOutSine',
                translateY: (target, index, total) => {
                  return index !== total - 1 ? [0, index === 0 || index === 1 ? -50 : 50] : 0;
                },
                scale: (target, index, total) => {
                  return index === total - 1 ? [1, 0] : 1;
                },
                opacity: 0
              });

              const imgrect = this.DOM.img.getBoundingClientRect();
              const win = { w: window.innerWidth, h: window.innerHeight };

              const imgAnimeOpts = {
                targets: this.DOM.img,
                duration: 250,
                easing: 'easeOutCubic',
                translateX: win.w / 2 - (imgrect.left + imgrect.width / 2),
                translateY: win.h / 2 - (imgrect.top + imgrect.height / 2)
              };

              if (win.w > 0.8 * win.h) {
                this.DOM.img.style.transformOrigin = '50% 50%';
                Object.assign(imgAnimeOpts, {
                  scaleX: 0.55 * win.w / parseInt(0.8 * win.h),
                  scaleY: 0.55 * win.w / parseInt(0.8 * win.h),
                  rotate: 0
                });
              }
              anime(imgAnimeOpts);

              anime({
                targets: this.DOM.close,
                duration: 250,
                easing: 'easeInOutCubic',
                scale: 1.8,
                rotate: 180
              });
            }
            zoomOut() {
              if (this.isAnimating) return false;
              this.isAnimating = true;
              this.isZoomed = false;

              anime({
                targets: [this.DOM.title, this.DOM.deco, this.DOM.subtitle, this.DOM.price, this.DOM.description, this.DOM.cart, this.DOM.magnifier],
                duration: 250,
                easing: 'easeOutCubic',
                translateY: 0,
                scale: 1,
                opacity: 1
              });

              anime({
                targets: this.DOM.img,
                duration: 250,
                easing: 'easeOutCubic',
                translateX: 0,
                translateY: 0,
                scaleX: 1,
                scaleY: 1,
                rotate: 0,
                complete: () => {
                  this.DOM.img.style.transformOrigin = '0 0';
                  this.isAnimating = false;
                }
              });

              anime({
                targets: this.DOM.close,
                duration: 250,
                easing: 'easeInOutCubic',
                scale: 1,
                rotate: 0
              });
            }
          };

          class Item {
            constructor(el) {

              this.DOM = {};
              this.DOM.el = el;
              this.DOM.product = this.DOM.el.querySelector('.product');

              this.DOM.productBg = this.DOM.product.querySelector('.product__bg');
              this.DOM.productImg = this.DOM.product.querySelector('.product__img');

              this.info = {
                img: this.DOM.productImg.src,
                title: this.DOM.product.querySelector('.product__title').innerHTML,
                subtitle: this.DOM.product.querySelector('.product__subtitle').innerHTML,
                description: this.DOM.product.querySelector('.product__description').innerHTML,
                price: this.DOM.product.querySelector('.product__price').innerHTML
              };


              this.initEvents();



            }
            initEvents() {
              this.DOM.product.addEventListener('click', () => this.open());
            }
            open() {
              DOM.details.fill(this.info);
              DOM.details.open({
                productBg: this.DOM.productBg,
                productImg: this.DOM.productImg

              });
            }
          };
          const DOM = {};
          DOM.grid = document.querySelector('.grid');
          DOM.content = DOM.grid.parentNode;
          DOM.hamburger = document.querySelector('.dummy-menu');
          DOM.gridItems = Array.from(DOM.grid.querySelectorAll('.grid__item'));
          let items = [];
          DOM.gridItems.forEach(item => items.push(new Item(item)));

          DOM.details = new Details();

          imagesLoaded(document.body, () => document.body.classList.remove('loading'));
        }
      });
    }, 2000)

  }


  startVideo() {
    /* Video api start*/
    //HOME VIDEO
    $('.videoLink').on('click', function () {
      var _videow = $(".videoPaly").width();
      var _videoh = $(".videoPaly").height();
      $('#videoHome .video-wrap h2').hide();
      var video = '<aside class="__videoPaly animated fadeInDownBig"><iframe src="' + $(this).attr('data-video') + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe><div class="closeVideo" onClick={this.closeVideo}>Close</div></aside>';
      $('.videoPaly').hide();
      $('.video-wrap').css('background-color', '#000');
      $(".video-wrap").append(video);

      $('.closeVideo').on('click', function () {
        $('.videoPaly').show();
        $('#videoHome .video-wrap h2').show();
        $('aside.__videoPaly').remove();
      });
    });

    /* video api end */
  }

  openCareersModal() {
    // //modal animations for the careers and quotations page start
    $('.cd-btn').click(function () {
      $('body').addClass('ohidden');
    });

    $('.modal-close').click(function () {
      $('body').removeClass('ohidden');
    });

    //MODAL
    var modalTriggerBts = $('a[data-type="cd-modal-trigger"]'),
      coverLayer = $('.cd-cover-layer');

    /*
      convert a cubic bezier value to a custom mina easing
      http://stackoverflow.com/questions/25265197/how-to-convert-a-cubic-bezier-value-to-a-custom-mina-easing-snap-svg
    */
    var duration = 600,
      epsilon = (1000 / 60 / duration) / 4,
      firstCustomMinaAnimation = bezier(.63, .35, .48, .92, epsilon);

    modalTriggerBts.each(function () {
      initModal($(this));
    });

    function initModal(modalTrigger) {
      var modalTriggerId = modalTrigger.attr('id'),
        modal = $('.cd-modal[data-modal="' + modalTriggerId + '"]'),
        svgCoverLayer = modal.children('.cd-svg-bg'),
        paths = svgCoverLayer.find('path'),
        pathsArray = [];
      //store Snap objects
      pathsArray[0] = Snap('#' + paths.eq(0).attr('id')),
        pathsArray[1] = Snap('#' + paths.eq(1).attr('id')),
        pathsArray[2] = Snap('#' + paths.eq(2).attr('id'));

      //store path 'd' attribute values 
      var pathSteps = [];
      pathSteps[0] = svgCoverLayer.data('step1');
      pathSteps[1] = svgCoverLayer.data('step2');
      pathSteps[2] = svgCoverLayer.data('step3');
      pathSteps[3] = svgCoverLayer.data('step4');
      pathSteps[4] = svgCoverLayer.data('step5');
      pathSteps[5] = svgCoverLayer.data('step6');

      //open modal window
      modalTrigger.on('click', function (event) {
        event.preventDefault();
        modal.addClass('modal-is-visible');
        coverLayer.addClass('modal-is-visible');
        animateModal(pathsArray, pathSteps, duration, 'open');
      });

      //close modal window
      modal.on('click', '.modal-close', function (event) {
        event.preventDefault();
        modal.removeClass('modal-is-visible');
        coverLayer.removeClass('modal-is-visible');
        animateModal(pathsArray, pathSteps, duration, 'close');
      });
    }

    function animateModal(paths, pathSteps, duration, animationType) {
      var path1 = (animationType == 'open') ? pathSteps[1] : pathSteps[0],
        path2 = (animationType == 'open') ? pathSteps[3] : pathSteps[2],
        path3 = (animationType == 'open') ? pathSteps[5] : pathSteps[4];
      paths[0].animate({ 'd': path1 }, duration, firstCustomMinaAnimation);
      paths[1].animate({ 'd': path2 }, duration, firstCustomMinaAnimation);
      paths[2].animate({ 'd': path3 }, duration, firstCustomMinaAnimation);
    }

    function bezier(x1, y1, x2, y2, epsilon) {
      //https://github.com/arian/cubic-bezier
      var curveX = function (t) {
        var v = 1 - t;
        return 3 * v * v * t * x1 + 3 * v * t * t * x2 + t * t * t;
      };

      var curveY = function (t) {
        var v = 1 - t;
        return 3 * v * v * t * y1 + 3 * v * t * t * y2 + t * t * t;
      };

      var derivativeCurveX = function (t) {
        var v = 1 - t;
        return 3 * (2 * (t - 1) * t + v * v) * x1 + 3 * (- t * t * t + 2 * v * t) * x2;
      };

      return function (t) {

        var x = t, t0, t1, t2, x2, d2, i;

        // First try a few iterations of Newton's method -- normally very fast.
        for (t2 = x, i = 0; i < 8; i++) {
          x2 = curveX(t2) - x;
          if (Math.abs(x2) < epsilon) return curveY(t2);
          d2 = derivativeCurveX(t2);
          if (Math.abs(d2) < 1e-6) break;
          t2 = t2 - x2 / d2;
        }

        t0 = 0, t1 = 1, t2 = x;

        if (t2 < t0) return curveY(t0);
        if (t2 > t1) return curveY(t1);

        // Fallback to the bisection method for reliability.
        while (t0 < t1) {
          x2 = curveX(t2);
          if (Math.abs(x2 - x) < epsilon) return curveY(t2);
          if (x > x2) t0 = t2;
          else t1 = t2;
          t2 = (t1 - t0) * .5 + t0;
        }

        // Failure
        return curveY(t2);

      };
    };
    //modal animations for the careers and quotations page end
  }
  componentWillUnmount() {

  }
  data() {

    let arr = [];
    let temp = this.props?.path?.seo_info?.meta
    temp.map((el) => {
      arr.push(
        <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
    })
    return arr
  }

  render() {
    return (
      // <>
      // <div>
      //   <p>Hello world</p>
      // </div>
      // </>
      <>
        <Layout_Light menus={this.props.menus} page={this.props.page} social_icons={this.props.social_icons} accquinated={this.props.accquinated} associations={this.props.associations} output={this.props.output} cookie={this.props.cookie}

          seo_info={this.props.path?.seo_info}
          meta={this.data()}
        >
          <div className="site-main-content">
            {/* inner page banner */}
            <div className="inner-page-banner">
              <div className="distortion-grid">
                <div className="distortion-grid-item">
                  <div className="distortion-grid-item-img" data-displacement="/static/images/displacement/8.jpg" data-intensity="-0.65" data-speedin="1.2" data-speedout="1.2">
                    <img src={this.props.path.banner_image[0].url} className="img-fluid" alt="distortion" />
                    <img src={this.props.path.banner_image[1].url} className="img-fluid" alt="distortion" />
                  </div>
                  <div className="distortion-grid-item-content">
                    <div className="container">
                      <div className="row">
                        <div className="col-md-12">
                          <p className="about-title custom-h2-heading pb-2">{this.props.path.page_title}</p>
                          <h1 className="text-white custom-h1-heading pb-5">{this.props.path.sub_title}</h1>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <a className="contact-us-link" href="/contact">Contact Us</a>
            {/* inner page banner */}
            {/* about page top section */}
            <section id="about-id" className="about-section about-page-section section-gapping">
              <div className="container">
                <div dangerouslySetInnerHTML={{ __html: this.props.path.page_body }}></div>
                {/*Home Video/}
            <section id="videoHome" className="mt-90">
              <div className="video-wrap">
                <div className="videoPaly tilt" style={{backgroundImage: 'url(/static/images/videobg.png)'}}>
                  <div className="video-content-block-img">
                    <a style={{transform: 'translateZ(20px)'}} className="videoLink btn-play playVideo" href="#" data-video={this.state.abouts.VideoUrl} ref={ this.startVideo.bind(this)}><img src="/static/images/playVideo.png" alt="about-video" /></a>
                  </div>
                </div>
              </div>
            </section>
            <div className="about-video-trangle">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 280 291" data-llp-composed="true" id="downtriangle" className="lazy-line-painter" style={{display: 'block'}}><title>about-triangle22</title><polygon points="0 0 251 130 0 291 0 291 251 130 0 0 0 0" style={{fill: 'rgb(214, 22, 22)', fillRule: 'evenodd', strokeLinecap: 'square'}} data-llp-id="downtriangle-0" data-llp-duration={1500} data-llp-delay={1000} fillOpacity={0} /></svg>
            </div>*/}
              </div>
            </section>
            {/* about page top section */}
            {/*Our Story*/}

            <section id="story" className="story-section section-gapping">
              <div className="container">
                <div className="section-title">
                  <h3>{this.props.story && this.props.story.Title}</h3>
                  <span className="title-border">
                    <svg id="story-pattern" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="69.43" height="69.46" viewBox="0 0 69.43 69.46" data-llp-composed="true" className="lazy-line-painter">
                      <defs>
                        <style dangerouslySetInnerHTML={{ __html: "\n                              .cls-1 {\n                                fill: #fff;\n                                stroke: #d61616;\n                                stroke-miterlimit: 10;\n                              }\n                            " }} />
                      </defs>
                      <title>pattern</title>
                      <line className="cls-1" x1="0.35" y1="69.1" x2="69.08" y2="0.35" data-llp-id="story-pattern-0" data-llp-duration={250} data-llp-delay={0} fillOpacity={0} style={{}} />
                    </svg>
                  </span>
                </div>
                <div className="row story-wrap">
                  <div className="col col-12 col-md-5 story-list-wrap">
                    {/* <div  dangerouslySetInnerHTML={{ __html: this.state.abouts.StoryArea.LeftArea}}> */}
                    <div>
                      <ul className="story-list">
                        <li><a className="story-list-item active" href="#name-content">{this.props.story && this.props.story.name.title}</a></li>
                        <li><a className="story-list-item " href="#why-content">{this.props.story && this.props.story.Why.title}</a></li>
                        <li><a className="story-list-item " href="#success-story-content">{this.props.story && this.props.story.Our_Strength.title}</a></li>
                      </ul>

                    </div>
                  </div>
                  <div className="col clo-12 col-md-7 story-list-details">
                    <div className="story-list-details-track">
                      <div className="story-list-details-wrap">
                        {/* <div dangerouslySetInnerHTML={{ __html: this.state.abouts.StoryArea.RightArea}}> */}

                        <div id="name-content" className="story-list-detail-item active"
                          dangerouslySetInnerHTML={{ __html: this.props.story && this.props.story.name.description }} >

                        </div>

                        <div id="why-content" className="story-list-detail-item"
                          dangerouslySetInnerHTML={{ __html: this.props.story && this.props.story.Why.description }} >
                        </div>

                        <div id="success-story-content" className="story-list-detail-item"
                          dangerouslySetInnerHTML={{ __html: this.props.story && this.props.story.Our_Strength.description }} >
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="story-line">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 358.41 224.97" data-llp-composed="true" id="story-line" className="lazy-line-painter"><title>services-line3</title><line x1="0.27" y1="224.54" x2="358.15" y2="0.42" style={{ fill: 'none', strokeMiterlimit: 10 }} data-llp-id="story-line-0" data-llp-duration={250} data-llp-delay={0} fillOpacity={0} data-llp-stroke-cap data-llp-stroke-join /></svg>
              </div>
            </section>
            {/*Our Story*/}

            {/*TEAM*/}

            <section id="team" className="team-section section-gapping">
              <div className="container">

                <div className="a-right">
                  <div className="section-title title-border-left">

                    <h3>{this.props.team.Title}</h3>

                    <span className="title-border">
                      <svg id="team-pattern" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="69.43" height="69.46" viewBox="0 0 69.43 69.46" data-llp-composed="true" className="lazy-line-painter">
                        <defs>
                          <style dangerouslySetInnerHTML={{ __html: "\n                                      .cls-1 {\n                                        fill: #fff;\n                                        stroke: #d61616;\n                                        stroke-miterlimit: 10;\n                                      }\n                                    " }} />
                        </defs>
                        <title>pattern</title>
                        <line className="cls-1" x1="0.35" y1="69.1" x2="69.08" y2="0.35" data-llp-id="team-pattern-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} style={{}} />
                      </svg>
                    </span>
                  </div>
                  <p className="team-description">{this.props.team.description}</p>
                </div>
                <div className="row teamWrap">
                  <div className="col col-12 col-md-4">
                    <div className="team-image-wrap">
                      <div className=" tilt2">
                        <a href="#!">
                          <img src="/static/images/user.svg" className="career-img" alt="career-form" />

                        </a>
                      </div>
                      <div className="team-trangle1">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 494 237" data-llp-composed="true" id="team-trangle1" className="lazy-line-painter" style={{ display: 'block' }}><title>aboutTrangle1</title><polyline points="139 0 0 237 494 237 0 237" style={{ fill: 'rgb(214, 22, 22)', fillRule: 'evenodd', stroke: 'rgb(214, 22, 22)', strokeOpacity: 1, strokeWidth: 20, strokeDasharray: '1262.75, 1262.75', strokeDashoffset: '49.485' }} data-llp-id="team-trangle1-0" data-llp-duration={1500} data-llp-delay={0} fillOpacity={0} />
                        </svg>
                      </div>
                      <div className="team-trangle2">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 251 291" data-llp-composed="true" id="team-trangle2" className="lazy-line-painter" style={{ display: 'block' }}><title>about-triangle22</title><polygon points="0 0 251 130 0 291 0 291 251 130 0 0 0 0" style={{ fill: 'rgb(214, 22, 22)', fillRule: 'evenodd', stroke: 'rgb(214, 22, 22)', strokeOpacity: 1, strokeWidth: 25, strokeLinecap: 'square', strokeDasharray: '1161.73, 1161.73', strokeDashoffset: 0 }} data-llp-id="team-trangle2-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} /></svg>
                      </div>
                    </div>
                    {/* <div className="team-desc">
                  <h4 className="member-name">Let's be a team</h4>
                  <a
                        className="button cd-btn career-btn"
                        id="modal-trigger"
                        ref={this.openCareersModal.bind(this)}
                        data-type="cd-modal-trigger"
                        style={{color: "#fff"}}
                      >
                        Join Team August
                      </a>
                 
                </div> */}
                  </div>
                  <div className="col col-12 col-md-8">
                    <div className="allTeamWrap">
                      <div className="grid">

                        {this.state.team_data !== null && this.state.team_data.map((services, index) => {

                          return (
                            <div className={`grid__item  position-${index + 1}`} key={services.id}>
                              <div className="product">
                                <div className="product__bg" />
                                <img className="product__img" src={services.bio_photo.url} alt="img 01" />
                                <h2 className="product__title">{services.bio_title}</h2>
                                <h3 className="product__subtitle">{services.sub_title}</h3>
                                <p className="product__description">{services.bio_text}</p>
                                <div className="product__price"></div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            {/*TEAM*/}


            {/* mission */}
            <section id="mission" className="mission-section section-gapping">
              <div className="container">
                <div className="row mission-wrap">
                  <div className="col col-md-4">
                    <div className="mission-item">
                      <div className="mission-item-img"><div className="mission-img" /></div>
                      <h4 className="sub-title" data-splitting>Mission</h4>
                      <p>{this.props.block.Mission_text}</p>
                    </div>
                  </div>
                  <div className="col col-md-4">
                    <div className="mission-item">
                      <div className="mission-item-img"><div className="vision-img" /></div>
                      <h4 className="sub-title" data-splitting>Vision</h4>
                      <p>{this.props.block.Vision_text}</p>
                    </div>
                  </div>
                  <div className="col col-md-4">
                    <div className="mission-item">
                      <div className="mission-item-img"><div className="values-img" /></div>
                      <h4 className="sub-title" data-splitting>Core Values</h4>
                      <p>{this.props.block.CoreValue_text}</p>
                    </div>
                  </div>
                </div>
                <div className="mission-line1">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 358.41 224.97" data-llp-composed="true" id="mission-line1" className="lazy-line-painter" style={{ display: 'block' }}><title>services-line3</title><line x1="0.27" y1="224.54" x2="358.15" y2="0.42" style={{ fill: 'none', strokeMiterlimit: 10, stroke: 'rgb(214, 22, 22)', strokeOpacity: 1, strokeWidth: '10.7', strokeDasharray: '422.265, 422.265', strokeDashoffset: 0 }} data-llp-id="mission-line1-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} data-llp-stroke-cap data-llp-stroke-join /></svg>
                </div>
                <div className="mission-line2">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 358.41 224.97" data-llp-composed="true" id="mission-line2" className="lazy-line-painter" style={{ display: 'block' }}><title>services-line3</title><line x1="0.27" y1="224.54" x2="358.15" y2="0.42" style={{ fill: 'none', strokeMiterlimit: 10, stroke: 'rgb(214, 22, 22)', strokeOpacity: 1, strokeWidth: '10.7', strokeDasharray: '422.265, 422.265', strokeDashoffset: 0 }} data-llp-id="services-line3-0" data-llp-duration={500} data-llp-delay={0} fillOpacity={0} data-llp-stroke-cap data-llp-stroke-join /></svg>
                </div>
              </div>
            </section>
            {/* mission */}

            {/* site-main-content close */}

          </div>

        </Layout_Light>
      </>
    )
  }
}
