import React, { Component } from 'react';
import Recaptcha from "react-recaptcha";
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import Link from './ActiveLink'; // our version of link
import axios from "axios"

export default class Footer extends Component {

  constructor(props) {
    super(props)
    this.state = {
      file: null,
      output: [],
      captchares: "",
      fields: {
        fullname: '',
        companyname: '',
        email: '',
        contactno: '',
        communicationmod: '',
        comment: '',

      },
      errors: {}
    }
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    //get recaptcha response

 setTimeout(() => {
  $(document).ready(function () {
    var qtl;
    qtl = new TimelineLite();
    var cov = $(this).find(".reveal-block__black1");
    var cov2 = $(this).find(".reveal-block__orange1");
    var img = $(this).find(".formquote");


    qtl
      .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
      .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
      .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left" }, "-=1.2")
      .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
      .from(img, 1, { opacity: 0 }, "reveal")
    qtl.pause();

    $('.quote-btn').click(function () {
      $('body').addClass('ohidden');
      var tmax = new TimelineMax();

      tmax.fromTo('.quotewrap', 0.5, { opacity: 0, 'z-index': 0 }, { opacity: 1, autoAlpha: 1, 'z-index': 9999999, ease: Elastic.easeOut.config(0.1, 0.5) });

      qtl.play();
    });

    $('.quote-fixed-button').click(function () {
      $('body').addClass('ohidden');
      var tmax = new TimelineMax();

      tmax.fromTo('.quotewrap', 0.5, { opacity: 0, 'z-index': 0 }, { opacity: 1, autoAlpha: 1, 'z-index': 9999999, ease: Elastic.easeOut.config(0.1, 0.5) });

      qtl.play();
    });

    $('.q-close').click(function () {
      $('body').removeClass('ohidden');
      qtl.reverse();

      setTimeout(function () {
        var tmax = new TimelineMax();
        tmax.fromTo('.quotewrap', 0.5, { opacity: 1, 'z-index': 500 }, { opacity: 0, autoAlpha: 1, 'z-index': -1, ease: Elastic.easeOut.config(0.1, 0.5) });
      }, 1000);
    });

    /*
     this code helps to split the
     words in the banner which help us to add 
     typing effect to all content. 
*/
    Splitting({
      /* target: String selector, Element, Array of Elements, or NodeList */
      target: "[data-splitting]",
      /* by: String of the plugin name */
      by: "chars",
      /* key: Optional String to prefix the CSS variables */
      key: null
    });
    /*********** footer section animation ****************/
    var controller = new ScrollMagic.Controller();
    var t4 = new TimelineMax();
    t4.staggerFromTo('testimonials-trigger-2 .partnership-quote .sub-title', 0.2, {
      xPercent: -100,
      opacity: 0,
      ease: Elastic.easeOut.config(0.1, 0.5)
    },
      {
        xPercent: 0,
        opacity: 1,
        ease: Elastic.easeOut.config(0.1, 0.5)
      }, 0.3)

      .fromTo(
        'testimonials-trigger-2 .partnership-quote .button',
        0.1,
        { scale: 0 },
        { scale: 1 }
      )

    var scene = new ScrollMagic.Scene({
      triggerElement: ".testimonials-trigger-2"
    })
      .setTween(t4)
      .addTo(controller);


    var t8 = new TimelineMax();

    t8.staggerFromTo('.partnership-quote .sub-title', 0.2, {
      xPercent: -100,
      opacity: 0,
      ease: Elastic.easeOut.config(0.1, 0.5)
    },
      {
        xPercent: 0,
        opacity: 1,
        ease: Elastic.easeOut.config(0.1, 0.5)
      }, 0.3)

      .fromTo(
        '.partnership-quote .button',
        0.2,
        { scale: 0 },
        { scale: 1 }
      )

    var scene = new ScrollMagic.Scene({
      triggerElement: ".partnership-quote",
      triggerHook: 0.8
    })
      .setTween(t8)
      // .addIndicators()
      .addTo(controller);


    //
    var controller = new ScrollMagic.Controller();
    var t5 = new TimelineMax();

    t5.fromTo('.footer-left-bottom-line-2 svg', 0.5, { display: "none" }, {
      display: "block", onComplete: function () {
        footer_left_bottom_line2();
      }
    })

      .fromTo('.footer-left-bottom-line-1 svg', 0.5, { display: "none" }, {
        display: "block", onComplete: function () {
          footer_left_bottom_line1();
        }
      })

      .staggerFromTo('.register-partners > .row > .col', 0.01, {
        xPercent: 100,
        opacity: 0,
        ease: Elastic.easeOut.config(0.01, 0.02)
      },
        {
          xPercent: 0,
          opacity: 1,
          ease: Elastic.easeOut.config(0.01, 0.02)
        }, 0.1)

      .fromTo(
        '.footer-logo',
        0.5,
        { scale: 0 },
        { scale: 1 }
      )

      .staggerFromTo('.footer-menu li, .social-icons a', 0.01, {
        xPercent: 100,
        opacity: 0,
        ease: Elastic.easeOut.config(0.01, 0.02)
      },
        {
          xPercent: 0,
          opacity: 1,
          ease: Elastic.easeOut.config(0.01, 0.02)
        }, 0.1)

      .fromTo(
        '.footer-block',
        0.2,
        { opacity: 0 },
        { opacity: 1 }
      )

      .staggerFrom('.copyright .char', 0.1, { opacity: 0, ease: Power1.easeIn }, 0.01, "+=0.05")

    var scene = new ScrollMagic.Scene({
      triggerElement: ".site-footer",
      triggerHook: 0.8
    })
      .setTween(t5)
      // .addIndicators()
      .addTo(controller);


    /*********** footer section animation close ****************/
    function footer_left_bottom_line1() {

      if (document.readyState === 'complete') {

        /**
         * Setup your Lazy Line element.
         * see README file for more settings
         */

        let el = document.querySelector('#footerleft-bottom-line-1');
        let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616" });
        myAnimation.paint();
      }
    }

    function footer_left_bottom_line2() {

      if (document.readyState === 'complete') {

        /**
         * Setup your Lazy Line element.
         * see README file for more settings
         */

        let el = document.querySelector('#footerleft-bottom-line-2');
        let myAnimation = new LazyLinePainter(el, { "ease": "easeLinear", "strokeWidth": 1, "strokeOpacity": 1, "strokeColor": "#d61616" });
        myAnimation.paint();
      }
    }


  });
 }, 1000);
 

  }

  handleValidation() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    //Name
    if (!fields["fullname"]) {
      formIsValid = false;
      errors["fullname"] = "Enter full name";
    }


    //Email
    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Enter valid email";
    }

    if (typeof fields["email"] !== "undefined") {
      let lastAtPos = fields["email"].lastIndexOf("@");
      let lastDotPos = fields["email"].lastIndexOf(".");

      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          fields["email"].indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          fields["email"].length - lastDotPos > 2
        )
      ) {
        formIsValid = false;
        errors["email"] = "Email is not valid";
      }
    }

    if ($('#quote-recaptcha').find('textarea.g-recaptcha-response').val().length > 0
      //  && 
      //   (typeof fields["recaptcha"]  == "undefined")
    ) {
      $('#quote-recaptcha').next("span.captcha-error").text("")
    } else {
      formIsValid = false;
      $('#quote-recaptcha').next("span.captcha-error").text("Please verify that you are human")
    }
    this.setState({ errors: errors });
    return formIsValid;
  }

  contactSubmit(e) {
    e.preventDefault();
    if (this.handleValidation()) {
      axios({
        method: 'post',
        url: `${API_URL}/estimates`,
        headers: {

          'Authorization': `Bearer ${API_JWT_Token}`
        },
        data: this.state.fields
      })
      $('.contact-banner').find('.get-quote-success').show()
      setTimeout(function () {
        $('.contact-banner').find('.get-quote-success').hide()
      }, 20000)
      this.state.fields["fullname"] = ''
      this.state.fields["companyname"] = ''
      this.state.fields["email"] = ''
      this.state.fields["contactno"] = ''
      this.state.fields["communicationmod"] = ''
      this.state.fields["comment"] = ''
      grecaptcha.reset();
    }

  }


  handleChange(field, e) {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields });
  }
  loadcallback = function () {
    // console.log('quote captcha loaded!!!');
  }
  render() {

    return (
      <>

        <div className="partnership-quote" id="getq">

          <h4 className="sub-title">
            {this.props.accquinated.title}
          </h4>
          <p className="get-quote-p">
            {this.props.accquinated.description}
          </p>
          <a
            className="button quote-btn"
            id="modal-trigger"
            data-type="qu-modal-trigger"
          >
            Click now for free estimate
          </a>
        </div>
        <footer className="site-footer">
          <div className="footer1">
            <div className="container footer-container">
              <div className="register-partners container mt-5 mb-5">
                <div className="row">
                  {this.props.associations.length !== 0 && this.props.associations.map((association) => {
                    return (
                      <div className="col" key={association.id}>
                        <img src={association.Logo.url} alt="NASSCOM" />
                        <small className="d-block pt-3">
                          <strong>{association.Association_type}</strong>
                          {association.Title}
                        </small>
                      </div>
                    )
                  })}
                </div>
              </div>

              {/* <Link activeClassName="active" href="/">
                    <a title="August Infotech">
                      <img
                        className="footer-logo"
                        src="https://res.cloudinary.com/augustinfotech/image/upload/v1639732502/august_infotech_b35dcd0663_22c9f52298.png"
                        alt="August Infotech"
                        width={216}
                        height={50}
                      />
                    </a>
                  </Link> */}
              <ul className="footer-menu">
                {this.props.menus.length !== 0 && this.props.menus.map((menu) => {
                  {
                    if (menu.Menu_type == "Footer") {
                      return (
                        <div key={menu.id}>
                          {menu.Links.map((link) => {
                            return (
                              <Link
                                activeClassName="active"
                                href={link.Url} key={link.id}>

                                <li >
                                  <a href={link.Url}>{link.Label}
                                  </a>
                                </li>
                              </Link>
                            )
                          }
                          )}
                        </div>
                      )
                    }
                  }
                })
                }
              </ul>



              <div className="social-icons">
                {this.props.social_icons.map((social) => {
                  return (
                    <a key={social.id}
                      className={social.class_name}
                      href={social.social_url}
                      target="_blank"
                    >
                      <i
                        className={social.font_class_name}
                        aria-hidden="true" />
                    </a>
                  )
                })}
              </div>
            </div>
          </div>
          <div className="footer2">
            <div className="container">
              {this.props.output.length !== 0 && this.props.output.map((contact) => {
                {
                  return (
                    <div className="footer-block" key={contact.id}>
                      <h5>{contact.Title}</h5>
                      {contact.contact_Info ?
                        <a href="tel:$`{contact.contact_Info}`">{contact.contact_Info}</a>
                        : <a href="mailto:$`{contact.contact_Link}`">{contact.contact_Link}</a>
                      }
                    </div>
                  )
                }
              }

              )}
            </div>
          </div>
          <div className="footer3">
            <div className="copyright">
              <p data-splitting>
                Copyright © 2021{" "}
                <Link href="/">
                  <a> AugustCode Solutions USA and India</a>
                </Link>
                . All Rights Reserved
              </p>
            </div>
          </div>

          <div className="footer-left-bottom-line">
            <div className="footer-left-bottom-line-1">
              <svg
                id="footerleft-bottom-line-1"
                data-name="Layer 1"
                xmlns="http://www.w3.org/2000/svg"
                width="160.4"
                height="160.39"
                viewBox="0 0 160.4 160.39"
                data-llp-composed="true"
                className="lazy-line-painter"
              >
                <defs>
                  <style
                    dangerouslySetInnerHTML={{
                      __html:
                        "\n                        .cls-1 {\n                          fill: #fff;\n                          stroke: #d61616;\n                          stroke-miterlimit: 10;\n                        }\n                      "
                    }}
                  />
                </defs>
                <title>footer-left-bottom-line-1</title>
                <line
                  className="cls-1"
                  x1="0.35"
                  y1="160.03"
                  x2="160.04"
                  y2="0.35"
                  data-llp-id="footerleft-bottom-line-1-0"
                  data-llp-duration={500}
                  data-llp-delay={0}
                  fillOpacity={0}
                  style={{}}
                />
              </svg>
            </div>
            <div className="footer-left-bottom-line-2">
              <svg
                id="footerleft-bottom-line-2"
                data-name="Layer 1"
                xmlns="http://www.w3.org/2000/svg"
                width="250.4"
                height="251.39"
                viewBox="0 0 250.4 251.39"
                data-llp-composed="true"
                className="lazy-line-painter"
              >
                <defs>
                  <style
                    dangerouslySetInnerHTML={{
                      __html:
                        "\n                        .cls-1 {\n                          fill: #fff;\n                          stroke: #d61616;\n                          stroke-miterlimit: 10;\n                        }\n                      "
                    }}
                  />
                </defs>
                <title>footer-left-bottom-line-2</title>
                <line
                  className="cls-1"
                  x1="0.35"
                  y1="251.03"
                  x2="250.04"
                  y2="0.35"
                  data-llp-id="footerleft-bottom-line-2-0"
                  data-llp-duration={500}
                  data-llp-delay={0}
                  fillOpacity={0}
                  style={{}}
                />
              </svg>
            </div>
          </div>
        </footer>
        {/* Footer close */}
        {/*careers modal start  */}
        {/* MODAL */}
        <div className="cd-modal" data-modal="modal-trigger" id="carrer-modal">
          <div
            className="cd-svg-bg"
            data-step1="M-59.9,540.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L864.8-41c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3L-59.5,540.6 C-59.6,540.7-59.8,540.7-59.9,540.5z"
            data-step2="M33.8,690l-188.2-300.3c-0.1-0.1,0-0.3,0.1-0.3l925.4-579.8c0.1-0.1,0.3,0,0.3,0.1L959.6,110c0.1,0.1,0,0.3-0.1,0.3 L34.1,690.1C34,690.2,33.9,690.1,33.8,690z"
            data-step3="M-465.1,287.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L459.5-294c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3 l-925.4,579.8C-464.9,287.7-465,287.7-465.1,287.5z"
            data-step4="M-329.3,504.3l-272.5-435c-0.1-0.1,0-0.3,0.1-0.3l925.4-579.8c0.1-0.1,0.3,0,0.3,0.1l272.5,435c0.1,0.1,0,0.3-0.1,0.3 l-925.4,579.8C-329,504.5-329.2,504.5-329.3,504.3z"
            data-step5="M341.1,797.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L1265.8,216c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3L341.5,797.6 C341.4,797.7,341.2,797.7,341.1,797.5z"
            data-step6="M476.4,1013.4L205,580.3c-0.1-0.1,0-0.3,0.1-0.3L1130.5,0.2c0.1-0.1,0.3,0,0.3,0.1l271.4,433.1c0.1,0.1,0,0.3-0.1,0.3 l-925.4,579.8C476.6,1013.6,476.5,1013.5,476.4,1013.4z"
          >
            <svg
              height="100%"
              width="100%"
              preserveAspectRatio="none"
              viewBox="0 0 800 500"
            >
              <title>SVG Modal background</title>
              <path
                id="cd-changing-path-1"
                d="M-59.9,540.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L864.8-41c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3L-59.5,540.6 C-59.6,540.7-59.8,540.7-59.9,540.5z"
              />
              <path
                id="cd-changing-path-2"
                d="M-465.1,287.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L459.5-294c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3 l-925.4,579.8C-464.9,287.7-465,287.7-465.1,287.5z"
              />
              <path
                id="cd-changing-path-3"
                d="M341.1,797.5l-0.9-1.4c-0.1-0.1,0-0.3,0.1-0.3L1265.8,216c0.1-0.1,0.3,0,0.3,0.1l0.9,1.4c0.1,0.1,0,0.3-0.1,0.3L341.5,797.6 C341.4,797.7,341.2,797.7,341.1,797.5z"
              />
            </svg>
          </div>
          <div className="cd-modal-content">
            <div className="careerswrap h-100 row">
              <div className="col col-12 col-lg-6 career-bg d-none d-lg-block" />
              <div className="col col-12 col-lg-6 d-flex align-items-center flex-lg-column">
                <div className="w-50 m-auto career-form">
                  <h2>
                    Don't just make a living{" "}
                    <small>
                      <br />
                      Make a difference, Take it higher
                    </small>
                  </h2>
                  <form className="w-100">
                    <div className="form-group">
                      <input className="form-control career-name" placeholder="Full Name" />
                    </div>
                    <div className="form-group">
                      <input className="form-control career-email" placeholder="Email" />
                    </div>
                    <div className="form-group">
                      <input className="form-control career-contact" placeholder="Contact Number" />
                    </div>
                    <div className="form-group">
                      <input
                        className="form-control career-position"
                        placeholder="Applied for Position"
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="file"
                        className="form-control-file career-file"
                        id="exampleFormControlFile1"
                        onChange={this.uploadResume}
                      />
                    </div>
                    <div className="form-group">
                      <textarea
                        className="form-control career-comment"
                        rows={3}
                        placeholder="Comment"
                      />
                    </div>
                    <div className="form-group">

                    </div>
                    <button type="submit" className="btn btn-primary btn-lg submit-career" onClick={this.submitCareer}>
                      SUBMIT
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>{" "}
          {/* cd-modal-content */}
          <a href="#0" className="modal-close">
            Close
          </a>
        </div>
        {/*careers modal end  */}
        {/* quotation modal start */}

        <section className="quotewrap">
          <div className="formquote">
            <div className="careerswrap h-100 row">
              <div className="col col-12 col-lg-6 career-bg d-none d-lg-block" />
              <div className="col col-12 col-lg-6 d-flex align-items-center flex-lg-column">
                <a href="#" className="q-close">
                  Close
                </a>
                <div className="w-50 m-auto career-form">
                  <div className="alert alert-success get-quote-success" role="alert">
                    Thank you for requesting Quote! Our executive will contact soon.
                  </div>
                  <h2>
                    Get your
                    <small>
                      <br />
                      free quotation today
                    </small>
                  </h2>
                  <form className="w-100"
                    onSubmit={this.contactSubmit.bind(this)} >
                    <div className="form-group">
                      <input className="form-control full-name1" placeholder="Name*"
                        onChange={this.handleChange.bind(this, "fullname")}
                        value={this.state.fields["fullname"]} />
                      <span className="error error-name">{this.state.errors["fullname"]}</span>
                    </div>
                    <div className="form-group">
                      <input className="form-control company-name1" placeholder="Company Name"
                        onChange={this.handleChange.bind(this, "companyname")}
                        value={this.state.fields["companyname"]} />

                    </div>
                    <div className="form-group">
                      <input className="form-control email-id1" placeholder="Email Id*"
                        onChange={this.handleChange.bind(this, "email")}
                        value={this.state.fields["email"]}
                      />
                      <span className="error error-email">{this.state.errors["email"]}</span>
                    </div>
                    <div className="form-group">
                      <input className="form-control phone-num" placeholder="Phone Number"
                        onChange={this.handleChange.bind(this, "contactno")}
                        value={this.state.fields["contactno"]}
                      />
                    </div>
                    <div className="form-group">
                      <select className="form-control" id="aioConceptName"
                        onChange={this.handleChange.bind(this, "communicationmod")}
                        value={this.state.fields["communicationmod"]} >*
                        <option>Mode of communication</option>
                        <option>Email</option>
                        <option>Phone</option>
                      </select>
                      <span className="error error-comm-mode"></span>
                    </div>
                    <div className="form-group">
                      <textarea
                        className="form-control comment1"
                        rows={3}
                        placeholder="Comment"
                        defaultValue={""}
                      />
                    </div>
                    <div className="form-group">

                      <Recaptcha
                        elementID='quote-recaptcha'
                        className='recaptcha'

                        sitekey="6LeUmKsdAAAAAFvZYra-Usln_yYZqGGMll7Znx9t"
                        onChange={this.loadcallback}
                      />
                      <span className="captcha-error error alert-danger"></span>
                    </div>
                    <div className="form-group">
                      <button type="submit" className="mt-2 btn btn-primary btn-lg">
                        SUBMIT
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div className="reveal-block-quote reveal-block__black1" />
          <div className="reveal-block-quote reveal-block__orange1" />
        </section>
        {/* quotation modal end */}
      </>


    )
  }
}
