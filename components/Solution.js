import React, { Component } from 'react';
import Head from "next/head";
import axios from "axios"
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import Layout_Dark from "../Layout/layout_dark"

export default class Solutions extends Component {
  constructor(state) {
    super(state);
    this.state = {
      hasError: false,
      pages: [],
      isLoaderLoaded: false,
    }
  }
  componentDidMount() {
    $(document).ready(function () {

      $('[data-toggle="collapse"]').click(function () {
        $('.collapse').collapse('hide')
      });

      var serl;
      serl = new TimelineLite();
      var scov = $(this).find(".reveal-block__black2");
      var scov2 = $(this).find(".reveal-block__orange2");
      var simg = $(this).find(".formquote2");


      serl
        .from(scov, 0.5, { scaleY: 0, transformOrigin: "left top" })
        .to(scov, 0.5, { scaleY: 0, transformOrigin: "right bottom" }, "reveal")
        .from(scov2, 0.5, { scaleY: 0, transformOrigin: "left top" }, "-=1.2")
        .to(scov2, 0.5, { scaleY: 0, transformOrigin: "right bottom" }, "reveal", "-=1.2")
        .from(simg, 1, { opacity: 0 }, "reveal");
      serl.pause();



      var controller = new ScrollMagic.Controller();
      $(".setanime").each(function () {

        var tl = new TimelineMax();
        var cov = $(this).find(".reveal-block__black");
        var cov2 = $(this).find(".reveal-block__orange");
        var img = $(this).find("img");

        tl
          .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
          .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
          .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left" }, "-=1.2")
          .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
          .from(img, 1, { opacity: 0 }, "reveal");

        var scene = new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.7
        })
          .setTween(tl)
          .addTo(controller);
      });


      $(window).scroll(function () {
        if ($(window).scrollTop() >= 600) {
          $('.sidebar').addClass('fixed-header');
        }
        else {
          $('.sidebar').removeClass('fixed-header');
        }
      });

      // Service page text animation
      var serviceTxtAnim = new TimelineMax();

      serviceTxtAnim
        .staggerFromTo('.section-title h3, .about-section p, .section-title .custom-h3-heading, .about-section .custom-h1-heading', 0.5, {
          xPercent: 100,
          autoAlpha: 0,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, {
          xPercent: 0,
          autoAlpha: 1,
          ease: Elastic.easeOut.config(0.1, 0.75)
        }, 0.2);

      $(".servicesection").each(function () {
        var serviceSecTxtAnim = new TimelineMax();
        var serviceSecH2 = $(this).find(".dds");
        var serviceSecH3 = $(this).find(".dds-h3");
        var serviceSecP = $(this).find(".ser-data");

        serviceSecTxtAnim
          .staggerFromTo(serviceSecH2, 0.5, {
            xPercent: 100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2)
          .staggerFromTo([serviceSecH3, serviceSecP], 0.5, {
            xPercent: 100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2)

        var scene = new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.7
        })
          .setTween(serviceSecTxtAnim)
          .addTo(controller);
      });

      $(".services-workflow-content").each(function () {
        var serviceSecTxtAnim = new TimelineMax();
        var serviceSecFirstCol = $(this).find(".col").first();
        var serviceSecSecondCol = $(this).find(".col").last();

        serviceSecTxtAnim
          .staggerFromTo(serviceSecFirstCol, 0.5, {
            xPercent: -100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2)
          .staggerFromTo(serviceSecSecondCol, 0.5, {
            xPercent: 100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2)

        var scene = new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.7
        })
          .setTween(serviceSecTxtAnim)
          .addTo(controller);
      });
      var find = $(".how-to-engage");
      var findtext = new TimelineMax();
      findtext.staggerFromTo(find, 0.5, {
        xPercent: -100,
        autoAlpha: 0,
        ease: Elastic.easeOut.config(0.1, 0.75)
      }, {
        xPercent: 0,
        autoAlpha: 1,
        ease: Elastic.easeOut.config(0.1, 0.75)
      }, 0.3)
      var scene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 0.7
      })
        .setTween(findtext)
        .addTo(controller);


      $("#accordion").each(function () {
        var serviceSecTxtAnim = new TimelineMax();
        var serviceSecH2 = $(this).find("h1");
        var card = $(this).find(".card");
        serviceSecTxtAnim
          .staggerFromTo(serviceSecH2, 0.5, {
            xPercent: 100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2)
          .staggerFromTo(card, 0.5, {
            xPercent: -100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.3)

        var scene = new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.7
        })
          .setTween(serviceSecTxtAnim)
          .addTo(controller);
      });


      var testimonialServicesAnim = new TimelineMax();

      testimonialServicesAnim.staggerFromTo('.testimonialservices blockquote', 0.2, {
        xPercent: -100,
        opacity: 0,
        ease: Elastic.easeOut.config(0.1, 0.5)
      },
        {
          xPercent: 0,
          opacity: 1,
          ease: Elastic.easeOut.config(0.1, 0.5)
        }, 0.3)

        .fromTo(
          '.testimonialservices .button',
          0.2,
          { scale: 0 },
          { scale: 1 }
        )

      var scene = new ScrollMagic.Scene({
        triggerElement: ".testimonialservices"
      })
        .setTween(testimonialServicesAnim)
        .addTo(controller);

    });


  }

  data() {

    let arr = [];
    let temp = this.props?.path?.seo_info?.meta
    temp.map((el) => {
      arr.push(
        <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
    })
    return arr
  }
  render() {
    return (
      <>
        <Layout_Dark menus={this.props.menus} page={this.props.page} social_icons={this.props.social_icons} accquinated={this.props.accquinated} associations={this.props.associations} output={this.props.output} cookie={this.props.cookie}
          seo_info={this.props.path?.seo_info}
          meta={this.data()}>
          {/* site-main-content */}
          <div className="site-main-content ">
            <div className="service-banner">

              <section id="about-id" className="about-section about-page-section section-gapping offset-lg-1 offset-0">

                <div className="container">
                  <div className="section-title">
                    <p className="mb-5 custom-h3-heading">{this.props.path.page_title}</p>
                    <h1 className="custom-h1-heading section-heading-h1">{this.props.path.sub_title}</h1>
                  </div>
                  <div className="row">
                    <div className="col col-lg-12">
                      <p className="service-banner-content">
                        {this.props.path.page_body}
                      </p>

                    </div>
                  </div>
                </div>

              </section>

            </div>
            <a className="contact-us-link" href="/contact">Contact Us</a>
            {/* about page top section */}
            {/*SERVICES*/}

            <div className="servicesList">
              <div className="container">
                <div className="rightCont clearb" id="serlist">

                  <div className="row">

                    <div className="col col-lg-12 servicesection">

                      <div id="accordion" role="tablist" aria-multiselectable="true">
                        <h1> Our Solutions </h1>
                        <div className="card">
                          <h5 className="card-header" role="tab" id="headingOne">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" className="d-block">
                              <i className="fa fa-chevron-up pull-right" /> {this.props.services.length && this.props.services[0].Title}
                            </a>
                          </h5>
                          <div id="collapseOne" className="collapse show" role="tabpanel" aria-labelledby="headingOne">
                            <div className="card-body card-data1">
                              <h2>{this.props.services.length && this.props.services[0].Short_Description} </h2>
                              <p>{this.props.services.length && this.props.services[0].body}</p>
                              <ul className="reveal-ul">

                                {this.props.services.length && this.props.services[0].Technology.map((item) =>
                                  <li className="reveal-li" key={item.id}>{item.text}</li>
                                )}

                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="card">
                          <h5 className="card-header" role="tab" id="headingTwo">
                            <a className="collapsed d-block" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              <i className="fa fa-chevron-up pull-right" /> {this.props.services.length && this.props.services[1].Title}
                            </a>
                          </h5>
                          <div id="collapseTwo" className="collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div className="card-body card-data1">
                              <h2>{this.props.services.length && this.props.services[1].Short_Description} </h2>
                              <p>{this.props.services.length && this.props.services[1].body}</p>
                              <ul className="reveal-ul">
                                {this.props.services.length && this.props.services[1].Technology.map((item) =>
                                  <li className="reveal-li" key={item.id}>{item.text}</li>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="card">
                          <h5 className="card-header" role="tab" id="headingThree">
                            <a className="collapsed d-block" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              <i className="fa fa-chevron-up pull-right" /> {this.props.services.length && this.props.services[2].Title}
                            </a>
                          </h5>
                          <div id="collapseThree" className="collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div className="card-body card-data1">
                              <h2>{this.props.services.length && this.props.services[2].Short_Description} </h2>
                              <p>{this.props.services.length && this.props.services[2].body}</p>
                              <ul className="reveal-ul">
                                {this.props.services.length && this.props.services[2].Technology.map((item) =>
                                  <li className="reveal-li" key={item.id}>{item.text}</li>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="card">
                          <h5 className="card-header" role="tab" id="headingThree">
                            <a className="collapsed d-block" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                              <i className="fa fa-chevron-up pull-right" /> {this.props.services.length && this.props.services[3].Title}
                            </a>
                          </h5>
                          <div id="collapseFour" className="collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div className="card-body card-data1">
                              <h2>{this.props.services.length && this.props.services[3].Short_Description} </h2>
                              <p>{this.props.services.length && this.props.services[3].body}</p>
                              <ul className="reveal-ul">
                                {this.props.services.length && this.props.services[3].Technology.map((item) =>
                                  <li className="reveal-li" key={item.id}>{item.text}</li>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="card">
                          <h5 className="card-header" role="tab" id="headingThree">
                            <a className="collapsed d-block" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                              <i className="fa fa-chevron-up pull-right" /> {this.props.services.length && this.props.services[4].Title}
                            </a>
                          </h5>
                          <div id="collapseFive" className="collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div className="card-body card-data1">
                              <h2>{this.props.services.length && this.props.services[4].Short_Description} </h2>
                              <p>{this.props.services.length && this.props.services[4].body}</p>
                              <ul className="reveal-ul">
                                {this.props.services.length && this.props.services[4].Technology.map((item) =>
                                  <li className="reveal-li" key={item.id}>{item.text}</li>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div className="card">
                          <h5 className="card-header" role="tab" id="headingThree">
                            <a className="collapsed d-block" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                              <i className="fa fa-chevron-up pull-right" /> {this.props.services.length && this.props.services[5].Title}
                            </a>
                          </h5>
                          <div id="collapseSix" className="collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div className="card-body card-data1">
                              <h2>{this.props.services.length && this.props.services[5].Short_Description} </h2>
                              <p>{this.props.services.length && this.props.services[5].body}</p>
                              <ul className="reveal-ul">
                                {this.props.services.length && this.props.services[5].Technology.map((item) =>
                                  <li className="reveal-li" key={item.id}>{item.text}</li>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col col-lg-12 servicesection">
                      <div className="servicenameHalf" dangerouslySetInnerHTML={{ __html: this.props.solution.body }}>

                      </div>
                    </div>
                    <div className="col col-12">
                      <h2 className="text-center how-to-engage">{this.props.engage.Title}</h2>
                      <div className="row services-workflow-content">
                        <div className="col col-12 col-md-6 pr-md-5">
                          <h2 className="text-sm-left text-md-right">When to choose</h2>

                          <h1 className="text-sm-left text-md-right">Constrained Budget Engagement</h1>
                          <div dangerouslySetInnerHTML={{ __html: this.props.engage.Constrained_Budget_Engagement }}></div>
                        </div>
                        <div className="col col-12 col-md-6 pl-md-5">
                          <h2 className="text-sm-left text-md-left evolve-budget-h2">When to choose</h2>
                          <h1 className="text-sm-left text-md-left evolve-budget-h1"> Evolving Scope Engagement</h1>
                          <div dangerouslySetInnerHTML={{ __html: this.props.engage.Evolving_Scope_Engagement }}>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/*SERVICES*/}
            {/*TESTIMONIAL comment as API does not have testimonials data*/}
            {/*<div className="testimonialservices mt-55 mb-50">
          <div className="container">
            <div className="row">
              <div className="col col-md-12 text-center">
                <blockquote>
                  <span className="qstart">"</span>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{this.props.services.TestimonialContent} 
                  <div className="author mt-4 mb-3">- {this.props.services.TestimonialName} <span className="authorcountry">{this.props.services.TestimonialLocation}</span></div>
                  <Link href="/testimonials"><a className="button">Read More</a></Link>
                  <span className="qends">"</span>
                </blockquote>
              </div>
            </div>
          </div>    
        </div>*/}
            {/*TESTIMONIAL*/}
          </div>
        </Layout_Dark>
      </>
    )
  }
}
