import React, { Component } from 'react';

import Link from "next/link";
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"

export default class Cookies extends Component {

  constructor(props) {
    super(props)
    this.state = {
      cookieSet: false
    }
  }

  componentDidMount() {
    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }
    var aiCookie = getCookie("ai");
    if (aiCookie != "") {
      this.setState({ cookieSet: true })
    } else {
    }

  }

  setCookieAI = () => {
    $(".alert").alert('close');
    //set cookie
    var cname = "ai"
    var cvalue = "AI";
    var exdays = 7;
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires;
    this.setState({ cookieSet: true })

  }
  render() {
    if (this.state.cookieSet == true) {
      return (
        <> <div className="no-cookie-div"></div> </>
      )
    }
    else {
      return (
        <>

          <div className="alert alert-warning alert-dismissible fade show alert-cookie" role="alert">
            <div className="container alert-cookie-container">
              <p>{this.props.cookie.Cookie_Policy} <Link href="/privacy_policy"><a>Privacy Policy.</a></Link>
              </p>
              <button type="button" className="btn btn-primary close-cookie" data-dismiss="alert" aria-label="Close" onClick={() => this.setCookieAI()}>
                <span aria-hidden="true">Accept & Proceed</span>
              </button>
            </div>
          </div>

        </>
      )
    }
  }
}
