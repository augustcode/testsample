import React, { Component } from 'react';
import Head from "next/head";
import Link from "next/link";
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import Router, { useRouter } from 'next/router'
import fetch from 'isomorphic-unfetch'
import Error from 'next/error'
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import Layout_Dark from "../Layout/layout_dark";
import axios from "axios"
let prev = 0;
let next = 0;
let last = 0;

export default class blog extends Component {

  constructor(props) {
    super(props);
    this.state = {
      blogs: '',
      blogData: [],
      pages: [],
      numbers: 1,
      // response:'',
      currentPage: parseInt(props.query.page) || 1,
      blogsPerPage: 6,
    };
  }
  async componentDidMount() {

    // const Res = await axios.get(`${API_URL}/pages`
    // ,{
    //   headers:{
    //     'Authorization' : `Bearer ${API_JWT_Token}`
    // }

    // }
    // )
    // const Data = await Res.data
    // this.setState({pages : Data }) 



    //  const temp = await axios(`${API_URL}/blogs/count`
    //  ,{
    //   headers:{
    //     'Authorization' : `Bearer ${API_JWT_Token}`
    // }

    // }
    //  );
    //  const response = await temp.data ;

    var start = this.props.blogData;

    start = this.state.currentPage === 1 ? 0 : (this.state.currentPage - 1) * this.state.blogsPerPage

    // const start= this.state.currentPage === 1 ? 0 : (this.state.currentPage - 1) * this.state.blogsPerPage

    // fetch(`${API_URL}/blogs?_limit=${response}&_start=${start}`
    // ,{
    //   headers:{
    //     'Authorization' : `Bearer ${API_JWT_Token}`
    // }
    // }
    // )
    // .then(res => res.json())
    // .then(data =>
    //   {
    //     this.setState({ blogData: data })
    //   }

    // )


    // .then(() => {

    this.shuffle(this.props.blogData)
    // })

    //suffle the testimonial array as display at random basis end
    this.setState({ blogs: this.props.blogData });

    setTimeout(() => {
      $(document).ready(function () {
        $(".page-item").click(function (e) {
          e.preventDefault();
          $('html,body').animate({
            scrollTop: 0
          }, 700);
        });

        var controller = new ScrollMagic.Controller();

        $(".setanime").each(function () {
          var tl = new TimelineMax();
          var cov = $(this).find(".reveal-block__black");
          var cov2 = $(this).find(".reveal-block__orange");
          var img = $(this).find("img");

          tl
            .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
            .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
            .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left" }, "-=1.2")
            .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
            .from(img, 1, { opacity: 0 }, "reveal");

          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.7
          })
            .setTween(tl)
            .addTo(controller);
        });

        var serviceTxtAnim = new TimelineMax();

        serviceTxtAnim
          .staggerFromTo('.section-title h3, .about-section p, .section-title .custom-h3-heading, .about-section .custom-h1-heading', 0.5, {
            xPercent: 100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2);

        $(".blogcaption").each(function () {
          var blogcaptionTxtAnim = new TimelineMax();
          var blogcaptionH2 = $(this).find("h2");
          var blogcaptionDate = $(this).find(".blogdate");
          var blogcaptionP = $(this).find("p");
          var blogcaptionReadmore = $(this).find(".read-more");
          blogcaptionTxtAnim
            .fromTo(blogcaptionH2, 0.5, {
              xPercent: 100,
              autoAlpha: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
              xPercent: 0,
              autoAlpha: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.2)
            .fromTo(blogcaptionDate, 0.5, {
              xPercent: 100,
              autoAlpha: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
              xPercent: 0,
              autoAlpha: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.2)
            .fromTo(blogcaptionP, 0.5, {
              xPercent: 100,
              autoAlpha: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
              xPercent: 0,
              autoAlpha: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.5)
            .fromTo(blogcaptionReadmore, 0.5, {
              xPercent: 100,
              autoAlpha: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
              xPercent: 0,
              autoAlpha: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.5);

          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.7
          })
            .setTween(blogcaptionTxtAnim)
            .addTo(controller);
        });
      });
    }, 1000);





  }
  //  Here, “unsafe” refers not to security but instead conveys that code 
  // using these lifecycles will be more likely to have bugs in future versions of React,
  //  especially once async rendering is enabled
  UNSAFE_componentWillReceiveProps() {
    $(document).ready(function () {
      $('html,body').animate({
        scrollTop: 0
      }, 700);

      global.setTimeout(() => {
        var controller = new ScrollMagic.Controller();
        $(".setanime").each(function () {
          var tl = new TimelineMax();
          var cov = $(this).find(".reveal-block__black");
          var cov2 = $(this).find(".reveal-block__orange");
          var img = $(this).find("img");

          tl
            .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
            .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
            .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left" }, "-=1.2")
            .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
            .from(img, 1, { opacity: 0 }, "reveal");

          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.7
          })
            .setTween(tl)
            .addTo(controller);
        });

        var serviceTxtAnim = new TimelineMax();

        serviceTxtAnim
          .staggerFromTo('.section-title h3, .about-section p', 0.5, {
            xPercent: 100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2);

        $(".blogcaption").each(function () {
          var blogcaptionTxtAnim = new TimelineMax();
          var blogcaptionH2 = $(this).find("h2");
          var blogcaptionDate = $(this).find(".blogdate");
          var blogcaptionP = $(this).find("p");
          var blogcaptionReadmore = $(this).find(".read-more");
          blogcaptionTxtAnim
            .fromTo(blogcaptionH2, 0.5, {
              xPercent: 100,
              autoAlpha: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
              xPercent: 0,
              autoAlpha: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.2)
            .fromTo(blogcaptionDate, 0.5, {
              xPercent: 100,
              autoAlpha: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
              xPercent: 0,
              autoAlpha: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.2)
            .fromTo(blogcaptionP, 0.5, {
              xPercent: 100,
              autoAlpha: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
              xPercent: 0,
              autoAlpha: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.5)
            .fromTo(blogcaptionReadmore, 0.5, {
              xPercent: 100,
              autoAlpha: 0,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
              xPercent: 0,
              autoAlpha: 1,
              ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.5);

          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.7
          })
            .setTween(blogcaptionTxtAnim)
            .addTo(controller);
        });

      },
        1000);
    });
  }
  //  Here, “unsafe” refers not to security but instead conveys that code 
  // using these lifecycles will be more likely to have bugs in future versions of React,
  //  especially once async rendering is enabled
  UNSAFE_componentWillMount() {
    this.setState({ blogs: this.props.blogData });
  }

  dispBlogDate(blogDate) {
    if (blogDate) {
      let day = (new Date(blogDate)).toLocaleDateString('en-US', { day: 'numeric' });
      let year = (new Date(blogDate)).toLocaleDateString('en-US', { year: 'numeric' });
      let month = (new Date(blogDate)).toLocaleDateString('en-US', { month: 'long' });
      return (<>{month}{' '}<span>{day}{','} {year}</span></>)
    }
  }
  sortByDate(array) {
    array.sort(() => Math.random() - 0.5);
    this.setState({ blogData: array })
  }

  shuffle(array) {

    array.sort((a, b) => new Date(b.blog_date) - new Date(a.blog_date));


    this.setState({ blogs: array })
  }


  data() {

    let arr = [];
    let temp = this.props?.path?.seo_info?.meta
    temp.map((el) => {
      arr.push(
        <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
    })
    return arr
  }
  render() {

    let { currentPage, blogsPerPage } = this.state;
    // Logic for displaying current todos
    let indexOfLastBlog = currentPage * blogsPerPage;
    let indexOfFirstBlog = indexOfLastBlog - blogsPerPage;
    let currentBlogs = this.state.blogs.slice(indexOfFirstBlog, indexOfLastBlog);
    prev = currentPage > 0 ? (currentPage - 1) : 0;
    last = Math.ceil(this.state.blogs.length / blogsPerPage);
    next = (last === currentPage) ? currentPage : currentPage + 1;
    // Logic for displaying page numbers

    let initialNumber = this.state.currentPage - 2
    let linkLimit = this.state.currentPage + 2
    let pageNumbers = [];
    for (let i = initialNumber; i <= linkLimit; i++) {
      if (i <= last && i >= 1) {
        pageNumbers.push(i);
      }
    }
    return (
      <>
        <Layout_Dark menus={this.props.menus} page={this.props.page} social_icons={this.props.social_icons} accquinated={this.props.accquinated} associations={this.props.associations} output={this.props.output} cookie={this.props.cookie}
          seo_info={this.props.path?.seo_info}
          meta={this.data()} >

          <div className="site-main-content">
            {/* blog list page start */}
            <section id="about-id" className="about-section about-page-section section-gapping featureListBlog">

              <div className="container">
                <div className="section-title">
                  <p className="mb-5 custom-h3-heading">{this.props.path.page_title}</p>
                </div>
                <div className="row">

                  <div className="col col-lg-6">
                    <h1 className="custom-h1-heading">{this.props.path.page_body}</h1>
                  </div>
                </div>
                {/*BLOG LISTING*/}
                <section className="blogwrap">

                  {currentBlogs.length !== 0 && currentBlogs.map((blog, i) => {

                    return (
                      <section className={'mt-5 allbloglist ' + (i % 2 ? 'rgtblog' : 'lftblog')} key={blog.id}>
                        <figure className="fig1 setanime">
                          <div className="blogimg">

                            <img src={!blog.blog_banner[0] || blog.blog_banner[0].url == 'undefined' ? '/static/images/default-blog.jpg' : blog.blog_banner[0].url} alt={blog.Title} />
                            {/* <div className="reveal-block reveal-block__black"  />
                              <div className="reveal-block reveal-block__orange" />  */}
                          </div>

                          <figcaption className="blogcaption">
                            <Link href="/blog/[slug]" as={`/blog/${blog.slug}`}  >
                              <a><h2><span className="blog-list-title" dangerouslySetInnerHTML={{ __html: blog.Title }} /></h2></a>
                            </Link>
                            <span className="mt-3 blogdate">
                              {this.dispBlogDate(blog.blog_date)} | {blog.blog_author}</span>
                            <p>
                              <span className="blog-list-content" dangerouslySetInnerHTML={{ __html: blog.blog_excerpt }} />
                            </p>
                            <Link
                              href="/blog/[slug]"
                              as={`/blog/${blog.slug}`} >
                              <a className="arrowmore"><img src="/static/images/left-arrow.png" alt="blogdetail" /></a>
                            </Link>
                          </figcaption>
                        </figure>
                      </section>
                    );

                  })}
                </section>
                {/*PAGINATION START*/}

                {pageNumbers.length > 1 ?
                  <div className="text-center">
                    <nav aria-label="navigation" className="d-inline-block paginationwrap">
                      <Pagination>
                        <PaginationItem>
                          {prev === 0 ? <PaginationLink disabled>&laquo;</PaginationLink> :
                            <PaginationLink onClick={() => {
                              this.setState({ currentPage: this.state.currentPage - 1 })
                              Router.push(`/blog?page=${this.state.currentPage - 1}`)
                            }
                            }>&laquo;</PaginationLink>
                          }
                        </PaginationItem>
                        {pageNumbers.map((number, i) =>

                          <Pagination key={i}>
                            <PaginationItem active={pageNumbers[i] === (this.state.currentPage) ? true : false} >
                              <PaginationLink onClick={() => {

                                this.setState({ currentPage: number })
                                Router.push(`/blog?page=${number}`)
                              }
                              }>
                                {number}
                              </PaginationLink>
                            </PaginationItem>
                          </Pagination>
                        )}
                        <PaginationItem>
                          {this.state.page === last ? <PaginationLink disabled>&raquo;</PaginationLink> :
                            <PaginationLink onClick={() => {
                              this.setState({ currentPage: this.state.currentPage + 1 })
                              Router.push(`/blog?page=${this.state.currentPage + 1}`)
                            }}>&raquo;</PaginationLink>
                          }
                        </PaginationItem>
                      </Pagination>
                      {/*PAGINATION END*/}
                    </nav>
                  </div>
                  : ''}
              </div>
            </section>
            {/* blog list page end */}
          </div>

          <a className="contact-us-link contact-us-link-white-bg" href="/contact">Contact Us</a>
        </Layout_Dark>

      </>
    )
  }
}

