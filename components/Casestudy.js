import React, { Component } from 'react';
import Head from "next/head";
import axios from "axios"
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import Link from "next/link";
import Layout_Dark from "../Layout/layout_dark";



export default class CaseStudyList extends Component {


  constructor(props) {
    super(props);
  }
  async componentDidMount() {
    $(document).ready(function () {
      var controller = new ScrollMagic.Controller();
      $(".setanime").each(function () {
        var tl = new TimelineMax();
        var cov = $(this).find(".reveal-block__black");
        var cov2 = $(this).find(".reveal-block__orange");
        var img = $(this).find("img");

        tl
          .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
          .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
          .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left" }, "-=1.2")
          .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
          .from(img, 1, { opacity: 0 }, "reveal");

        var scene = new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.7
        })
          .setTween(tl)
          .addTo(controller);
      });

    });
  }
  data() {

    let arr = [];
    let temp = this.props?.path?.seo_info?.meta
    temp.map((el) => {
      arr.push(
        <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
    })
    return arr
  }
  render() {

    return (

      <>
        <Layout_Dark menus={this.props.menus} page={this.props.page} social_icons={this.props.social_icons} accquinated={this.props.accquinated} associations={this.props.associations} output={this.props.output} cookie={this.props.cookie}
          seo_info={this.props.path?.seo_info}
          meta={this.data()}
        >


          {/* <title>{ this.props.path.seo_info.seo_title}</title> */}
          {/*Meta tags Start*/}
          {/* <meta name="description" content={ this.props.path.seo_info.seo_description} />
            <meta property="og:title" content={ this.props.path.seo_info.meta[0].meta_name} />
            <meta property="og:description" content={this.props.path.seo_info.meta[0].meta_content} /> */}
          {/*Meta tags End*/}
          {/*Twitter Tag Start*/}
          {/* <meta name="twitter:title" content={this.props.path.seo_info.meta[0].meta_name} />
            <meta name="twitter:description" content={this.props.path.seo_info.meta[0].meta_content} /> */}
          {/*Twitter Tag End*/}
          {/*Canonical Tag Start*/}
          {/* <link rel="canonical" href={'//www.augustinfotech.com/casestudy_list'} /> */}
          {/*Canonical Tag End*/}

          <section className="casestudyWrap">
            <section id="about-id" className="about-section about-page-section section-gapping featureListBlog">
              <div className="container">
                <div className="section-title">
                  <p className="custom-h3-heading mb-5">{this.props.path.page_title}</p>
                </div>
                <div className="row">
                  <div className="col col-lg-6">
                    <h1 className="custom-h1-heading">{this.props.path.page_body}</h1>
                  </div>
                </div>
                {/* <div className="row  mt-5"> */}
                {this.props.detail !== null && this.props.detail.map((case_study, i) => {
                  return (
                    <div key={case_study.id}>
                      {i % 2 == 0 ?
                        <div className="row  mt-5" >
                          <div className="col col-12 col-lg-5 mt-5" >
                            <figure className="setanime">
                              <img className="mb-5"
                                src={!case_study.banner_image.url || case_study.banner_image.url == 'undefined'
                                  ? "null" : case_study.banner_image.url} width="100%" alt="3D Magento" />
                              <div className="reveal-block reveal-block__black" />
                              <div className="reveal-block reveal-block__orange" />
                            </figure>
                          </div>
                          <div className="col col-12  col-lg-7 mt-5" >
                            <figcaption>
                              <div className="section-title">
                                <h2 className="mb-3">{case_study.Title}</h2>
                              </div>
                              <p>{case_study.Short_description}</p>
                              <Link href="/case_study/[slug]"
                                as={`/case_study/${case_study.slug}`}><a className="read-more"><strong>Read More</strong></a></Link>
                            </figcaption>
                          </div>
                        </div>

                        :
                        <div className="row  mt-5" >
                          <div className="col col-12 col-lg-7 mt-5" >
                            <figcaption>
                              <div className="section-title">
                                <h2 className="mb-3">{case_study.Title}</h2>
                              </div>
                              <p>{case_study.Short_description}</p>
                              <Link href="/case_study/[slug]"
                                as={`/case_study/${case_study.slug}`} ><a className="read-more"><strong>Read More</strong></a></Link>
                            </figcaption>
                          </div>
                          <div className="col col-12  col-lg-5 mt-5" >
                            <figure className="setanime">
                              <img className="mb-5" src={case_study.banner_image.url} width="100%" alt="3D Magento" />
                              <div className="reveal-block reveal-block__black" />
                              <div className="reveal-block reveal-block__orange" />
                            </figure>
                          </div>
                        </div>

                      }
                    </div>
                  )
                })}
              </div>
            </section>
          </section>
        </Layout_Dark>
      </>
    )
  }
}
