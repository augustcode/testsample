import React, { Component } from 'react';
import Head from "next/head";
import Router from 'next/router'
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import axios from "axios"
import {
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";
import Layout_Dark from "../Layout/layout_dark"

let prev = 0;
let next = 0;
let last = 0;
let first = 0;
export default class Testinomial extends Component {
  constructor(state) {
    super(state);
    this.state = {
      testimonials: [],
      numbers: 1,
      pages: [],
      todos: [],
      todos1: '',
      currentPage: 1,
      todosPerPage: 10,
    };
  }

  componentDidMount() {
    this.shuffle(this.props.testimonial)
    setTimeout(() => {
      window.onload = (() => {
        $(".page-item").click(function (e) {
          e.preventDefault();
          $('html,body').animate({
            scrollTop: 0
          }, 700);
        });

        var controller = new ScrollMagic.Controller();
        // Testimonial page text animation
        var testimonialTxtAnim = new TimelineMax();

        testimonialTxtAnim
          .staggerFromTo('.section-title h4, .section-title h3, .caption-text p, .section-title .custom-h4-heading, .caption-text .custom-h1-heading', 0.5, {
            xPercent: 100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2);

        $(".testimonialwrap > .row > .col").each(function () {
          var testimonialSecAnim = new TimelineMax();
          var testimonialBlock = $(this).find('.testimonialblock');
          var testimonialImg = $(this).find('.testimonialblock .testi-men img');
          var testimonialBlockquote = $(this).find('.testimonialblock blockquote');

          testimonialSecAnim
            .fromTo(testimonialBlock, 0.5, { scale: 0 }, { scale: 1 })
            .fromTo(
              testimonialImg,
              0.1,
              { scale: 0 },
              { scale: 1 },
              '-=0.2'
            )
            .fromTo(
              testimonialBlockquote,
              0.5,
              { autoAlpha: 0 },
              { autoAlpha: 1 }
            )
          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.5
          })
            .setTween(testimonialSecAnim)
            // .addIndicators()
            .addTo(controller);
        });
      });
    }, 100)
  }

  componentDidUpdate() {
    setTimeout(() => {
      window.onload = (() => {

        var controller = new ScrollMagic.Controller();
        // Testimonial page text animation
        var testimonialTxtAnim = new TimelineMax();

        testimonialTxtAnim
          .staggerFromTo('.section-title h4, .section-title h3, .caption-text p, .section-title .custom-h4-heading, .caption-text .custom-h1-heading', 0.5, {
            xPercent: 100,
            autoAlpha: 0,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, {
            xPercent: 0,
            autoAlpha: 1,
            ease: Elastic.easeOut.config(0.1, 0.75)
          }, 0.2);

        $(".testimonialwrap > .row > .col").each(function () {
          var testimonialSecAnim = new TimelineMax();
          var testimonialBlock = $(this).find('.testimonialblock');
          var testimonialImg = $(this).find('.testimonialblock .testi-men img');
          var testimonialBlockquote = $(this).find('.testimonialblock blockquote');

          testimonialSecAnim
            .fromTo(testimonialBlock, 0.5, { scale: 0 }, { scale: 1 })
            .fromTo(
              testimonialImg,
              0.1,
              { scale: 0 },
              { scale: 1 },
              '-=0.2'
            )
            .fromTo(
              testimonialBlockquote,
              0.5,
              { autoAlpha: 0 },
              { autoAlpha: 1 }
            )
          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.5
          })
            .setTween(testimonialSecAnim)
            // .addIndicators()
            .addTo(controller);
        });
      });
    }, 100)

  }

  //  Here, “unsafe” refers not to security but instead conveys that code 
  // using these lifecycles will be more likely to have bugs in future versions of React,
  //  especially once async rendering is enabled
  UNSAFE_componentWillMount() {
    this.setState({ todos: this.props.testimonial });
  }

  //shuflle array on random basis
  shuffle(array) {

    array.sort(() => Math.random() - 0.5);
    this.setState({ testimonials: array })
  }
  data() {

    let arr = [];
    let temp = this.props?.path?.seo_info?.meta
    temp.map((el) => {
      arr.push(
        <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
    })
    return arr
  }
  render() {
    let { todos, currentPage, todosPerPage } = this.state;

    let indexOfLastTodo = currentPage * todosPerPage;
    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    let currentTestimonials = this.state.testimonials.slice(indexOfFirstTodo, indexOfLastTodo);
    prev = currentPage > 0 ? (currentPage - 1) : 0;
    last = Math.ceil(this.state.testimonials.length / todosPerPage);
    next = (last === currentPage) ? currentPage : currentPage + 1;
    // Logic for displaying page numbers
    let pageNumbers = [];
    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }

    // Logic for displaying current todos


    return (

      <>
        <Layout_Dark menus={this.props.menus} page={this.props.page} social_icons={this.props.social_icons} accquinated={this.props.accquinated} associations={this.props.associations} output={this.props.output}
          seo_info={this.props.path?.seo_info} cookie={this.props.cookie}
          meta={this.data()}>
          {/* site-main-content */}

          <div className="site-main-content contact-us-wrap">
            {/* about page top section */}
            <section id="about-id" className="about-section about-page-section section-gapping featureListBlog testimonial-page-section">

              <div className="container">
                <div className="section-title col-lg-10 offset-lg-1 offset-0 mt-5">
                  <div className="row">
                    <div className="w-100">
                      <p className="custom-h4-heading" style={{ opacity: 0 }}>{this.props.path.sub_title}</p>
                    </div>
                  </div>
                </div>
                <div className="row caption-text">
                  <div className="col col-lg-7 offset-lg-1 offset-0">
                    <h1 className="custom-h1-heading" style={{ opacity: 0 }}>{this.props.path.page_body}</h1>
                  </div>
                </div>
                <section className="testimonialwrap offset-lg-1 offset-0">
                  <div className="row">

                    {currentTestimonials && currentTestimonials.map((test, i) => {

                      return (
                        <div className="col col-12 col-lg-6" key={test.Title + i}>
                          <div className="testimonialblock">
                            <div className="testi-men text-center m-auto">
                              <img src={this.props.path.banner_image[0].url} className="rounded-circle" alt="rounded-circle" />
                            </div>
                            <blockquote className="text-center">
                              <img src="/static/images/quotes.svg" alt="quotes" width={35} />
                              {test.testimonial_text}
                              <div className="author mt-4 mb-3">
                                {test.Title}
                                <br />
                                {/* <span className="authorcountry">{testimonial.testimonialLocation}</span> */}
                                {test.is_verified
                                  ?
                                  <div className="verified-text">
                                    Verified
                                  </div>
                                  :
                                  null
                                }
                              </div>
                            </blockquote>
                          </div>
                        </div>
                      );

                    })}
                  </div>

                  {/*PAGINATION*/}



                  {pageNumbers.length > 1 ?
                    <div className="text-center">
                      <nav aria-label="navigation" className="d-inline-block paginationwrap">

                        <Pagination>
                          <PaginationItem>
                            {prev === 0 ? <PaginationLink disabled>&laquo;</PaginationLink> :
                              <PaginationLink onClick={() => {
                                this.setState({ currentPage: this.state.currentPage - 1 })
                                Router.push(`/testimonials/?page=${this.state.currentPage - 1}`)

                              }
                              }>&laquo;</PaginationLink>
                            }
                          </PaginationItem>
                          {pageNumbers.map((number, i) =>
                            <Pagination key={i}>
                              <PaginationItem active={pageNumbers[i] === (this.state.numbers) ? true : false} >
                                <PaginationLink onClick={() => {
                                  // this.setState(this.state.currentPage)
                                  this.setState({ currentPage: number })
                                  Router.push(`/testimonials/?page=${number}`)
                                }

                                }>
                                  {number}
                                </PaginationLink>
                              </PaginationItem>
                            </Pagination>
                          )}
                          <PaginationItem>
                            {this.state.numbers === last ? <PaginationLink disabled>&raquo;</PaginationLink> :
                              <PaginationLink onClick={() => {
                                this.setState({ currentPage: this.state.currentPage + 1 })
                                Router.push(`/testimonials/?page=${this.state.currentPage + 1}`)
                              }
                              }>&raquo;</PaginationLink>

                            }

                          </PaginationItem>
                        </Pagination>
                      </nav>
                    </div>
                    : ''
                  }

                </section>
              </div>

            </section>
            {/* about page top section */}
            {/* site-main-content close */}
          </div>
          <a className="contact-us-link contact-us-link-white-bg" href="/contact">Contact Us</a>

        </Layout_Dark>
      </>


    )
  }
}
