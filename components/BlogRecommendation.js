import React, { Component } from 'react';
import Link from "next/link";
import axios from "axios"

export default class BlogRecommendation extends Component {

    constructor(props) {
        super(props)
        this.state = {
            cookieSet: false,
            cookieBlogData: null,
            recommendedBlogData: null
        }
    }

    componentDidMount() {



        $(document).ready(function () {
            var controller = new ScrollMagic.Controller();
            $(".setanime").each(function () {
                var tl = new TimelineMax();
                var cov = $(this).find(".reveal-block__black");
                var cov2 = $(this).find(".reveal-block__orange");
                var img = $(this).find(".blog-recommend-img");

                tl
                    .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
                    .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
                    .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left" }, "-=1.2")
                    .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
                    .from(img, 1, { opacity: 0 }, "reveal");

                var scene = new ScrollMagic.Scene({
                    triggerElement: this,
                    triggerHook: 0.7
                })
                    .setTween(tl)
                    .addTo(controller);
            });

            setTimeout(() => {

                var t4 = new TimelineMax();
                t4.staggerFromTo('.partnership-quote .sub-title', 0.2, {
                    xPercent: -100,
                    opacity: 0,
                    ease: Elastic.easeOut.config(0.1, 0.5)
                },
                    {
                        xPercent: 0,
                        opacity: 1,
                        ease: Elastic.easeOut.config(0.1, 0.5)
                    }, 0.3)

                    .fromTo(
                        '.partnership-quote .button',
                        0.1,
                        { scale: 0 },
                        { scale: 1 }
                    )

                var scene = new ScrollMagic.Scene({
                    triggerElement: ".testimonials-trigger-2"
                })
                    .setTween(t4)
                    .addTo(controller);


                var t8 = new TimelineMax();

                t8.staggerFromTo('.partnership-quote .sub-title', 0.2, {
                    xPercent: -100,
                    opacity: 0,
                    ease: Elastic.easeOut.config(0.1, 0.5)
                },
                    {
                        xPercent: 0,
                        opacity: 1,
                        ease: Elastic.easeOut.config(0.1, 0.5)
                    }, 0.3)

                    .fromTo(
                        '.partnership-quote .button',
                        0.2,
                        { scale: 0 },
                        { scale: 1 }
                    )

                var scene = new ScrollMagic.Scene({
                    triggerElement: ".partnership-quote",
                    triggerHook: 0.8
                })
                    .setTween(t8)
                    .addTo(controller);
            }, 7000);

            var serviceTxtAnim = new TimelineMax();

            serviceTxtAnim
                .staggerFromTo('.section-title h3, .about-section p, .section-title .custom-h3-heading, .about-section .custom-h1-heading', 0.5, {
                    xPercent: 100,
                    autoAlpha: 0,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, {
                    xPercent: 0,
                    autoAlpha: 1,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, 0.2);

            $(".blogcaption").each(function () {
                var blogcaptionTxtAnim = new TimelineMax();
                var blogcaptionH2 = $(this).find("h2");
                var blogcaptionDate = $(this).find(".blogdate");
                var blogcaptionP = $(this).find("p");
                var blogcaptionReadmore = $(this).find(".read-more");
                blogcaptionTxtAnim
                    .fromTo(blogcaptionH2, 0.5, {
                        xPercent: 100,
                        autoAlpha: 0,
                        ease: Elastic.easeOut.config(0.1, 0.75)
                    }, {
                        xPercent: 0,
                        autoAlpha: 1,
                        ease: Elastic.easeOut.config(0.1, 0.75)
                    }, 0.2)
                    .fromTo(blogcaptionDate, 0.5, {
                        xPercent: 100,
                        autoAlpha: 0,
                        ease: Elastic.easeOut.config(0.1, 0.75)
                    }, {
                        xPercent: 0,
                        autoAlpha: 1,
                        ease: Elastic.easeOut.config(0.1, 0.75)
                    }, 0.2)
                    .fromTo(blogcaptionP, 0.5, {
                        xPercent: 100,
                        autoAlpha: 0,
                        ease: Elastic.easeOut.config(0.1, 0.75)
                    }, {
                        xPercent: 0,
                        autoAlpha: 1,
                        ease: Elastic.easeOut.config(0.1, 0.75)
                    }, 0.5)
                    .fromTo(blogcaptionReadmore, 0.5, {
                        xPercent: 100,
                        autoAlpha: 0,
                        ease: Elastic.easeOut.config(0.1, 0.75)
                    }, {
                        xPercent: 0,
                        autoAlpha: 1,
                        ease: Elastic.easeOut.config(0.1, 0.75)
                    }, 0.5);

                var scene = new ScrollMagic.Scene({
                    triggerElement: this,
                    triggerHook: 0.7
                })
                    .setTween(blogcaptionTxtAnim)
                    .addTo(controller);
            });

        });
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        var blogCookie = getCookie("blog_recommendation");
        if (blogCookie != "") {
            this.setState({ cookieSet: true })
            this.setCookieAI(this.props, blogCookie);
        } else {
            this.setCookieAI(this.props);
        }
    }

    componentWillReceiveProps(newProps) {
        var controller = new ScrollMagic.Controller();
        $(".setanime").each(function () {
            var tl = new TimelineMax();
            var cov = $(this).find(".reveal-block__black");
            var cov2 = $(this).find(".reveal-block__orange");
            var img = $(this).find(".blog-recommend-img");

            tl
                .from(cov, 0.5, { scaleX: 0, transformOrigin: "left" })
                .to(cov, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal")
                .from(cov2, 0.5, { scaleX: 0, transformOrigin: "left" }, "-=1.2")
                .to(cov2, 0.5, { scaleX: 0, transformOrigin: "right" }, "reveal", "-=1.2")
                .from(img, 1, { opacity: 0 }, "reveal");

            var scene = new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 0.7
            })
                .setTween(tl)
                .addTo(controller);
        });


        var t4 = new TimelineMax();
        t4.staggerFromTo('.partnership-quote .sub-title', 0.2, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
        },
            {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.3)

            .fromTo(
                '.partnership-quote .button',
                0.1,
                { scale: 0 },
                { scale: 1 }
            )

        var scene = new ScrollMagic.Scene({
            triggerElement: ".testimonials-trigger-2"
        })
            .setTween(t4)
            .addTo(controller);


        var t8 = new TimelineMax();

        t8.staggerFromTo('.partnership-quote .sub-title', 0.2, {
            xPercent: -100,
            opacity: 0,
            ease: Elastic.easeOut.config(0.1, 0.5)
        },
            {
                xPercent: 0,
                opacity: 1,
                ease: Elastic.easeOut.config(0.1, 0.5)
            }, 0.3)

            .fromTo(
                '.partnership-quote .button',
                0.2,
                { scale: 0 },
                { scale: 1 }
            )

        var scene = new ScrollMagic.Scene({
            triggerElement: ".partnership-quote",
            triggerHook: 0.8
        })
            .setTween(t8)
            .addTo(controller);

        var serviceTxtAnim = new TimelineMax();

        serviceTxtAnim
            .staggerFromTo('.section-title h3, .about-section p, .section-title .custom-h3-heading, .about-section .custom-h1-heading', 0.5, {
                xPercent: 100,
                autoAlpha: 0,
                ease: Elastic.easeOut.config(0.1, 0.75)
            }, {
                xPercent: 0,
                autoAlpha: 1,
                ease: Elastic.easeOut.config(0.1, 0.75)
            }, 0.2);

        $(".blogcaption").each(function () {
            var blogcaptionTxtAnim = new TimelineMax();
            var blogcaptionH2 = $(this).find("h2");
            var blogcaptionDate = $(this).find(".blogdate");
            var blogcaptionP = $(this).find("p");
            var blogcaptionReadmore = $(this).find(".read-more");
            blogcaptionTxtAnim
                .fromTo(blogcaptionH2, 0.5, {
                    xPercent: 100,
                    autoAlpha: 0,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, {
                    xPercent: 0,
                    autoAlpha: 1,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, 0.2)
                .fromTo(blogcaptionDate, 0.5, {
                    xPercent: 100,
                    autoAlpha: 0,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, {
                    xPercent: 0,
                    autoAlpha: 1,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, 0.2)
                .fromTo(blogcaptionP, 0.5, {
                    xPercent: 100,
                    autoAlpha: 0,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, {
                    xPercent: 0,
                    autoAlpha: 1,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, 0.5)
                .fromTo(blogcaptionReadmore, 0.5, {
                    xPercent: 100,
                    autoAlpha: 0,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, {
                    xPercent: 0,
                    autoAlpha: 1,
                    ease: Elastic.easeOut.config(0.1, 0.75)
                }, 0.5);

            var scene = new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 0.7
            })
                .setTween(blogcaptionTxtAnim)
                .addTo(controller);
        });

        //   });
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        var blogCookie = getCookie("blog_recommendation");
        if (blogCookie != "") {
            this.setState({ cookieSet: true })
            this.setCookieAI(newProps, blogCookie);
        } else {
            this.setCookieAI(newProps);
        }
    }

    setCookieAI = (blogData, blogCookie) => {
        var cname = "blog_recommendation"
        let blogCategories = [];
        let blogTags = [];
        if (blogCookie != '' && blogCookie != undefined) {
            blogCookie = JSON.parse(blogCookie);
            if (blogCookie['recent_blog_ids'].indexOf(blogData.currentBlogId) === -1) {
                blogCookie['recent_blog_ids'].push(blogData.currentBlogId)
            }

            let BlogCatsProp = blogData.currentBlogCategories.split(',');
            if (BlogCatsProp) {
                BlogCatsProp.forEach((catVal, i) => {
                    if (blogCookie['categories'].indexOf(catVal.trim()) === -1) {
                        blogCategories[i] = catVal.trim();
                    }
                })

                if (blogCategories.length >= 1) {
                    blogCookie['categories'] = blogCookie['categories'].concat(blogCategories)
                }
            }

            let BlogTagsProp = blogData.currentBlogTags.split(',');
            if (BlogTagsProp) {
                BlogTagsProp.forEach((tagVal, i) => {
                    if (blogCookie['tags'].indexOf(tagVal.trim()) === -1) {
                        blogTags[i] = tagVal.trim();
                    }
                })

                if (blogTags.length >= 1) {
                    blogCookie['tags'] = blogCookie['tags'].concat(blogTags)
                }
            }

            blogCookie['current_blog_ids'] = [blogData.currentBlogId];
            var cvalue = { 'current_blog_ids': blogCookie['current_blog_ids'], 'recent_blog_ids': blogCookie['recent_blog_ids'], 'categories': blogCookie['categories'], 'tags': blogCookie['tags'] };
        } else {
            let BlogCatsProp = blogData.currentBlogCategories.split(',');
            let blogCategories = [];
            let blogTags = [];
            if (BlogCatsProp) {
                blogCategories = BlogCatsProp.map(function (catVal, i) {
                    return catVal.trim();
                })
            }

            let BlogTagsProp = blogData.currentBlogTags.split(',');
            if (BlogTagsProp) {
                blogTags = BlogTagsProp.map(function (tagVal, i) {
                    return tagVal.trim();
                })
            }
            var cvalue = { 'current_blog_ids': [blogData.currentBlogId], 'recent_blog_ids': [blogData.currentBlogId], 'categories': blogCategories, 'tags': blogTags };
        }
        //set cookie
        var expires = "";
        document.cookie = cname + "=" + JSON.stringify(cvalue) + ";" + expires + ";path=/";
        this.setState({ cookieSet: true })
        this.setState({ cookieBlogData: cvalue })
        this.getRecommendedBlogsData(cvalue)
    }

    getRecommendedBlogsData(cookieBlogData) {
        const payload = {
            "arr": cookieBlogData['recent_blog_ids'],
            "current": cookieBlogData['current_blog_ids'],
            "categories": cookieBlogData['categories'],
            "tags": cookieBlogData['tags'],
            "n": 3
        };
        var self = this;

        axios({
            method: 'post',
            url: "https://related-blogs.herokuapp.com/api",
            config: {
                headers: {
                    'Content-Type': 'application/json'
                }
            },
            data: payload
        }).then(function (res) {
            if (res.data.id != '' && res.data.id != undefined) {
                if (res.data.id != '' && res.data.id != undefined) {
                    axios({
                        method: 'get',
                        url: "https://augsutcodebackoffice.augustinfotech.com/wp-json/wp/v2/bloglistdata/" + res.data.id,
                        config: {
                            headers: {
                                'Access-Control-Allow-Origin': '*',
                            }
                        },
                        data: payload
                    }).then(function (response) {
                        if (response.data != '' && response.data != undefined) {
                            self.setState({ recommendedBlogData: response.data })
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    dispBlogDate(blogDate) {
        if (blogDate) {
            let day = (new Date(blogDate)).toLocaleDateString('en-US', { day: 'numeric' });
            let year = (new Date(blogDate)).toLocaleDateString('en-US', { year: 'numeric' });
            let month = (new Date(blogDate)).toLocaleDateString('en-US', { month: 'short' });
            return (<>{day}<span>{month} {' '} {year}</span></>)
        }
    }

    render() {
        if (this.state.cookieSet == true && this.state.recommendedBlogData != '' && this.state.recommendedBlogData != null && this.state.recommendedBlogData != undefined) {
            return (
                <>
                    <div className="mb-5 partnership-quote">
                        <h4 className="sub-title">Recommended Blogs</h4>
                    </div>
                    <div className="mb-5 row justify-content-center">
                        {this.state.recommendedBlogData.BlogDetails.map((blog, i) => {
                            return (
                                <div className="col col-12 col-md-4" key={blog.BlogId}>
                                    <div className="blog-home-wrap">
                                        <Link href="/blog/[slug]" as={`/blog/${blog.slug}`}>
                                            <a className="home-blog-img">
                                                <img className="blog-recommend-img" src={!blog.Image || blog.Image == 'undefined' ? '/static/images/default-blog.jpg' : blog.Image} alt={blog.Title} />
                                            </a>
                                        </Link>
                                        <div className="title d-flex align-items-center">
                                            <div className="date">
                                                {this.dispBlogDate(blog.Date)}
                                            </div>
                                            <h5>
                                                <Link href="/blog/[slug]" as={`/blog/${blog.slug}`} >
                                                    <a title={blog.Title}>
                                                        <b dangerouslySetInnerHTML={{ __html: blog.Title }} />
                                                    </a>
                                                </Link>
                                                <span>By August Infotech</span>
                                            </h5>
                                        </div>
                                        <p><span className="blog-list-content" dangerouslySetInnerHTML={{ __html: blog.Excerpt }} /></p>
                                        <Link href="/blog/[slug]" as={`/blog/${blog.slug}`} >
                                            <a className="rm">
                                                Read More
                                            </a>
                                        </Link>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </>
            )
        } else {
            return (
                <>
                    <div>
                    </div>
                </>
            )
        }
    }
}
