import React, { Component } from 'react';
import Head from "next/head";
import Recaptcha from "react-recaptcha";
import { API_URL } from "../next.config"
import { API_JWT_Token } from "../next.config"
import axios from "axios";
import Layout_Light from '../Layout/layout_light';
export default class Contact extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fields: {
        fullname: '',
        companyname: '',
        email: '',
        subject: '',
        comment: '',

      },
      errors: {},
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    $(document).ready(function () {
      window.onload = () => { }
      if ($('.contact-banner').length) {
        const tilt3 = $('.tilt3').tilt({
          glare: false,
          maxTilt: 3
        });
        var renderer, scene, camera, ww, wh, particles, imagedata;
        ww = window.innerWidth,
          wh = window.innerHeight;

        var centerVector = new THREE.Vector3(0, 0, 0);
        var previousTime = 0;


        var init = function () {
          var getImageData = function (image) {

            var canvas = document.createElement("canvas");
            canvas.width = image.width;
            canvas.height = image.height;

            var ctx = canvas.getContext("2d");
            ctx.drawImage(image, 0, 0);

            return ctx.getImageData(0, 0, image.width, image.height);
          }
          THREE.ImageUtils.crossOrigin = '';
          renderer = new THREE.WebGLRenderer({
            canvas: document.getElementById("map"),
            antialias: true
          });
          renderer.setSize(ww, wh);
          renderer.setClearColor(0x1d1f23);

          scene = new THREE.Scene();

          camera = new THREE.PerspectiveCamera(50, ww / wh, 0.1, 10000);
          camera.position.set(-100, 0, 220);
          camera.lookAt(centerVector);
          scene.add(camera);

          var texture = THREE.ImageUtils.loadTexture("/static/images/transparentMap.png", undefined, function () {

            var imagedata = getImageData(texture.image);
            var drawTheMap = function () {

              var geometry = new THREE.Geometry();
              var material = new THREE.PointsMaterial({
                size: 3,
                color: 0x313742,
                sizeAttenuation: false
              });
              for (let y = 0, y2 = imagedata.height; y < y2; y += 2) {
                for (let x = 0, x2 = imagedata.width; x < x2; x += 2) {
                  if (imagedata.data[(x * 4 + y * 4 * imagedata.width) + 3] > 128) {

                    var vertex = new THREE.Vector3();
                    vertex.x = Math.random() * 1000 - 500;
                    vertex.y = Math.random() * 1000 - 500;
                    vertex.z = -Math.random() * 500;

                    vertex.destination = {
                      x: x - imagedata.width / 2,
                      y: -y + imagedata.height / 2,
                      z: 0
                    };
                    vertex.speed = Math.random() / 200 + 0.015;
                    geometry.vertices.push(vertex);
                  }
                }
              }
              particles = new THREE.Points(geometry, material);

              scene.add(particles);

              requestAnimationFrame(render);
            };

            drawTheMap();
          });

          window.addEventListener('resize', onResize, false);

        };
        var onResize = function () {
          ww = window.innerWidth;
          wh = window.innerHeight;
          renderer.setSize(ww, wh);
          camera.aspect = ww / wh;
          camera.updateProjectionMatrix();
        };

        var render = function (a) {

          requestAnimationFrame(render);

          for (var i = 0, j = particles.geometry.vertices.length; i < j; i++) {
            var particle = particles.geometry.vertices[i];
            particle.x += (particle.destination.x - particle.x) * particle.speed;
            particle.y += (particle.destination.y - particle.y) * particle.speed;
            particle.z += (particle.destination.z - particle.z) * particle.speed;
          }

          if (a - previousTime > 100) {
            var index = Math.floor(Math.random() * particles.geometry.vertices.length);
            var particle1 = particles.geometry.vertices[index];
            var particle2 = particles.geometry.vertices[particles.geometry.vertices.length - index];
            TweenMax.to(particle, Math.random() * 2 + 1, { x: particle2.x, y: particle2.y, ease: Power2.easeInOut });
            TweenMax.to(particle2, Math.random() * 2 + 1, { x: particle1.x, y: particle1.y, ease: Power2.easeInOut });
            previousTime = a;
          }

          particles.geometry.verticesNeedUpdate = true;
          camera.position.x = Math.sin(a / 5000) * 100;
          camera.lookAt(centerVector);

          renderer.render(scene, camera);
        };

        init();
      }

    });


  }

  handleValidation() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;


    //Name
    if (!fields["fullname"]) {
      formIsValid = false;
      errors["fullname"] = "Enter full name";
    }


    //Email
    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Enter valid email";
    }

    if (typeof fields["email"] !== "undefined") {
      let lastAtPos = fields["email"].lastIndexOf("@");
      let lastDotPos = fields["email"].lastIndexOf(".");

      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          fields["email"].indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          fields["email"].length - lastDotPos > 2
        )
      ) {
        formIsValid = false;
        errors["email"] = "Email is not valid";
      }
    }

    if (!fields["comment"]) {
      formIsValid = false;
      errors["comment"] = "Enter Comment";
    }

    if ($('#contact-recaptcha').find('textarea.g-recaptcha-response').val().length > 0
      //  && 
      //   (typeof fields["recaptcha"]  == "undefined")
    ) {
      $('#contact-recaptcha').next("span.captcha-error").text("")
    } else {
      formIsValid = false;
      $('#contact-recaptcha').next("span.captcha-error").text("Please verify that you are human")
    }
    this.setState({ errors: errors });
    return formIsValid;
  }

  contactSubmit(e) {
    e.preventDefault();
    if (this.handleValidation()) {
      axios({
        method: 'post',
        url: `${API_URL}/contacts`,
        headers: {
          'Authorization': `Bearer ${API_JWT_Token}`
        },
        data: this.state.fields
      })
      $('.contact-banner').find('.get-quote-success').show()
      setTimeout(function () {
        $('.contact-banner').find('.get-quote-success').hide()
      }, 20000)
      this.state.fields["fullname"] = ''
      this.state.fields["companyname"] = ''
      this.state.fields["email"] = ''
      this.state.fields["subject"] = ''
      this.state.fields["comment"] = ''
      grecaptcha.reset();
    }

  }
  handleChange(field, e) {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields });
  }
  data() {

    let arr = [];
    let temp = this.props?.path?.seo_info?.meta
    temp.map((el) => {
      arr.push(
        <meta name={el.meta_name} content={el.meta_content} key={el.id} />)
    })
    return arr
  }
  render() {
    return (
      <>
        <Layout_Light menus={this.props.menus} page={this.props.page} social_icons={this.props.social_icons} accquinated={this.props.accquinated} associations={this.props.associations} output={this.props.output} cookie={this.props.cookie}

          seo_info={this.props.path?.seo_info}
          meta={this.data()}>
          <Head>
            <title>{this.props.path.seo_info.seo_title}</title>
            {/*Meta tags Start*/}
            <meta name="description" content={this.props.path.seo_info.seo_description} />
            <meta property="og:title" content={this.props.path.seo_info.meta[0].meta_name} />
            <meta property="og:description" content={this.props.path.seo_info.meta[0].meta_content} />
            {/*Meta tags End*/}
            {/*Twitter Tag Start*/}
            <meta name="twitter:title" content={this.props.path.seo_info.meta[0].meta_name} />
            <meta name="twitter:description" content={this.props.path.seo_info.meta[0].meta_content} />
            <link rel="canonical" href={'//www.augustinfotech.com/contact'} />
            {/*Canonical Tag End*/}
            {/* tawk.to script for live chat start */}
            {/* <script id='tawkToCode' type="text/javascript" dangerouslySetInnerHTML={this.setTawkToCode()}></script> */}
            {/* tawk.to script for live chat end */}
          </Head>

          {/*CONTACT US*/}

          <section className="contact-banner d-flex align-items-center">
            <canvas id="map" />
            <div className="container blackbg">
              <div className="tilt3">
                <div className="section-title">
                  <div className="w-100"><p className="text-white custom-h4-heading">{this.props.path.sub_title}</p></div>
                  <p className="mb-5 custom-h3-heading text-white">{this.props.path.page_title}</p>
                </div>
                <h1 className="custom-h1-heading text-white">
                  {this.props.path.page_body}
                </h1>
              </div>
              <div className="mt-5 md-2 alert alert-success get-quote-success" role="alert">
                Thank you for getting in touch!
              </div>
              <form className="bannerform mt-5"
                onSubmit={this.contactSubmit.bind(this)}>
                <div className="row">
                  <div className="col col-12 col-lg-6">
                    <div className="form-group">
                      <input

                        className="form-control contact-name" placeholder="Full Name*"
                        onChange={this.handleChange.bind(this, "fullname")}
                        value={this.state.fields["fullname"]}
                      />
                      <span className="name-error error alert-danger">{this.state.errors["fullname"]}</span>
                    </div>
                  </div>
                  <div className="col col-12 col-lg-6">
                    <div className="form-group">
                      <input className="form-control companyname" placeholder="Company Name"
                        onChange={this.handleChange.bind(this, "companyname")}
                        value={this.state.fields["companyname"]} />

                    </div>
                  </div>
                  <div className="col col-12 col-lg-6">
                    <div className="form-group">
                      <input
                        className="form-control contact-email email" placeholder="Email ID*"
                        onChange={this.handleChange.bind(this, "email")}
                        value={this.state.fields["email"]}
                      />
                      <span className="email-error error alert-danger">{this.state.errors["email"]}</span>
                    </div>
                  </div>
                  <div className="col col-12 col-lg-6">
                    <div className="form-group">
                      <input className="form-control subject contact-subject" placeholder="Subject"
                        onChange={this.handleChange.bind(this, "subject")}
                        value={this.state.fields["subject"]} />

                    </div>
                  </div>
                  <div className="col col-12 col-lg-6">
                    <div className="form-group">
                      <textarea
                        refs="comment"
                        className="form-control comment contact-comment" rows={3} placeholder="Comment*"
                        onChange={this.handleChange.bind(this, "comment")}
                        value={this.state.fields["comment"]} />
                      <span className="comment-error error alert-danger">{this.state.errors["comment"]}</span>
                    </div>
                  </div>
                  <div className="col col-12 col-lg-12 mt-4">
                    <div className="form-group">

                      <Recaptcha
                        elementID='contact-recaptcha'
                        // verifyCallback= {this.verifyCallback()}
                        className="recaptcha"
                        // sitekey="6LexHsEUAAAAAPu90mStRHK8syUzVxSriDvHWj3y"                  
                        sitekey="6LeUmKsdAAAAAFvZYra-Usln_yYZqGGMll7Znx9t"
                      />

                      <span className="captcha-error error alert-danger"></span>
                    </div>
                  </div>
                </div>
                <button type="submit" className="button mt-4 btn-lg submit-btn">SUBMIT</button>
              </form>
            </div>
          </section>

          {/* site-main-content */}
          <div className="site-main-content contact-us-wrap">
            {/* about page top section */}
            <section id="about-id" className="about-section about-page-section">
              <section className="contact-detail">
                <div className="offset-lg-1 offset-0">
                  <div className="container">
                    <div className="row">
                      <div className="col col-12 col-lg-4 mb-5 mb-lg-0"
                        dangerouslySetInnerHTML={{ __html: this.props.contact_us.LetsTalk_text }} >
                      </div>
                      <div className="col col-12 col-lg-4 mb-5 mb-lg-0"
                        dangerouslySetInnerHTML={{ __html: this.props.contact_us.SayHello_text }}>
                      </div>
                      <div className="col col-12 col-lg-4"
                        dangerouslySetInnerHTML={{ __html: this.props.contact_us.OurOffice_text }}>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </section>

          </div>
        </Layout_Light>
      </>
    )
  }
}
