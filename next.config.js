// next.config.js
const withAssetsImport = require('next-assets-import')
const NODE_ENV = process.env.NODE_ENV || "development"
module.exports = withAssetsImport()
// const API_URL = NODE_ENV === "production" ?  "https://ai-backend-postgres.herokuapp.com": "http://localhost:1337"
// const API_URL = "http://192.168.123.121:1337"
const API_URL = "https://admin3.augustinfotech.com"
const API_JWT_Token = ""
const PORT = 80
const WEBSITE_URL = "https://www.augustinfotech.com"

module.exports = {
  env: {
    PORT: PORT,
    API_URL: API_URL,
    API_JWT_Token: API_JWT_Token,
    WEBSITE_URL: WEBSITE_URL
  },
  images: {
    domains: [
      'res.cloudinary.com'
    ],
  },
  headers:
  {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Origin': 'True',
    'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
    'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
  },
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  API_URL,
  API_JWT_Token,
  distDir: 'build',
  async redirects() {
    return [
      {
        source: '/blog/how-to-integrate-identity-management-system-wordpress',
        destination: '/blog/how-to-integrate-identity-management-system-with-word-press',
        permanent: true,
      },
      
      {
        source: '/materialize-design-css-framework',
        destination: '/blog/materialize-design-css-framework',
        permanent: true,
      },

      {
        source: '/seminar-on-how-to-build-effective-website',
        destination: '/blog/seminar-on-how-to-build-effective-website',
        permanent: true,
      },

      {
        source: '/5-benefits-of-using-issue-based-magazine-plugin',
        destination: '/blog/5-benefits-of-using-issue-based-magazine-plugin',
        permanent: true,
      },

      {
        source: '/5-reasons-for-using-premium-wordpress-magazine-theme',
        destination: '/blog/5-reasons-for-using-premium-wordpress-magazine-theme',
        permanent: true,
      },

      {
        source: '/adding-six-social-media-posts-to-your-wordpress-blog',
        destination: '/blog/adding-six-social-media-posts-to-your-wordpress-blog',
        permanent: true,
      },

      {
        source: '/5-quick-tips-to-promote-your-premium-wordpress-theme',
        destination: '/blog/5-quick-tips-to-promote-your-premium-wordpress-theme',
        permanent: true,
      },

      {
        source: '/pillars-of-inbound-marketing-that-no-one-ever-taught-you-infographic',
        destination: '/blog/pillars-of-inbound-marketing-that-no-one-ever-taught-you-infographic',
        permanent: true,
      },

      {
        source: '/shopify-vs-bigcommerce-best-hosted-ecommerce-platform',
        destination: '/blog/shopify-vs-bigcommerce-best-hosted-ecommerce-platform',
        permanent: true,
      },

      {
        source: '/paypal-express-checkout-with-recurring-payment',
        destination: '/blog/paypal-express-checkout-with-recurring-payment ',
        permanent: true,
      },

      {
        source: '/what-are-the-5-key-concepts-of-react-native',
        destination: '/blog/what-are-the-5-key-concepts-of-react-native',
        permanent: true,
      },


    ]
  }
}
