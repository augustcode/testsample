import React, { Component } from 'react';
import Head from "next/head";
import Link from "next/link";
import Recaptcha from "react-recaptcha";
import Header_Dark from "../components/header_dark"
import Footer from "../components/footer"
import Cookies from "../components/Cookies"
export default class Layout_Dark extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {

    
      $(document).ready(function () {
        $('html').removeClass('no-scroll');
        // back to top
        if ($('#back-to-top').length) {
          var scrollTrigger = 100, // px
            backToTop = function () {
              var scrollTop = $(window).scrollTop();
              if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
              } else {
                $('#back-to-top').removeClass('show');
              }
            };
          backToTop();
          $(window).on('scroll', function () {
            backToTop();
          });
          $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
              scrollTop: 0
            }, 700);
          });
        }
        /*
          this line adds no-scroll to default html page.
          This fixes the bug caused by the react js architecture.
        */


        //main menu text animation
        var $menu = $('.Menu-list'),
          $item = $('.Menu-list-item'),
          w = $(window).width(), //window width
          h = $(window).height(); //window height

        $(window).on('mousemove', function (e) {
          var offsetX = 0.5 - e.pageX / w, //cursor position X
            offsetY = 0.5 - e.pageY / h, //cursor position Y
            dy = e.pageY - h / 2, //@h/2 = center of poster
            dx = e.pageX - w / 2, //@w/2 = center of poster
            theta = Math.atan2(dy, dx), //angle between cursor and center of poster in RAD
            angle = theta * 180 / Math.PI - 90, //convert rad in degrees
            offsetPoster = $menu.data('offset'),
            transformPoster = 'translate3d(0, ' + -offsetX * offsetPoster + 'px, 0) rotateX(' + (-offsetY * offsetPoster) + 'deg) rotateY(' + (offsetX * (offsetPoster * 2)) + 'deg)'; //poster transform

          //get angle between 0-360
          if (angle < 0) {
            angle = angle + 360;
          }

          //poster transform
          $menu.css('transform', transformPoster);

          //parallax for each layer
          $item.each(function () {
            var $this = $(this),
              offsetLayer = $this.data('offset') || 0,
              transformLayer = 'translate3d(' + offsetX * offsetLayer + 'px, ' + offsetY * offsetLayer + 'px, 20px)';

            $this.css('transform', transformLayer);
          });
        });

        // mouse cursor animation
        var container = document.getElementById("main");
        var circle = document.querySelector(".circle");

        $("a").hover(
          function () {
            $(".circle").addClass("bgremove");
          }, function () {
            $(".circle").removeClass("bgremove");
          }
        );

        TweenMax.set(circle, { scale: 0, xPercent: -50, yPercent: -50 });

        container.addEventListener("pointerenter", function (e) {
          TweenMax.to(circle, 0.1, { scale: 1, opacity: 0.8 });
          positionCircle(e);
        });

        container.addEventListener("pointerleave", function (e) {
          TweenMax.to(circle, 0.1, { scale: 0, opacity: 0 });
          positionCircle(e);
        });

        container.addEventListener("pointermove", function (e) {
          positionCircle(e);

        });

        function positionCircle(e) {
          var rect = container.getBoundingClientRect();
          var relX = e.pageX - container.offsetLeft;
          var relY = e.pageY - container.offsetTop;

          TweenMax.to(circle, 0.3, { x: relX, y: relY });
        }

        $(document).mousemove(function (event) {
          $(".circle").addClass("bgremove");
        });

        var lastTimeMouseMoved = new Date().getTime();
        var t = setTimeout(function () {
          var currentTime = new Date().getTime();
          if (currentTime - lastTimeMouseMoved > 1000) {
            $(".circle").removeClass("bgremove");
            // $('.fall').remove();
          }
        }, 1000)

      });
   
  }

  //set the google analytic code start
  setGoogleAnalyticCode() {
    return {
      __html: `
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
  
    gtag('config', 'UA-16730861-1');  
    `
    };
  }
  //set the google analytic code end
  //set the rich snippet start
  setRichSnippet1() {
    return {
      __html: `{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.augustinfotech.com\/","name":"August Infotech","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.augustinfotech.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}`
    };
  }
  setRichSnippet2() {
    return {
      __html: `{"@context":"http:\/\/schema.org","@type":"Organization","url":"https:\/\/www.augustinfotech.com\/","sameAs":["https:\/\/www.facebook.com\/August.Infotech","https:\/\/www.instagram.com\/augustinfotech\/","https:\/\/www.linkedin.com\/in\/august-infotech-hr\/","https:\/\/twitter.com\/AugustInfotech"],"@id":"#organization","name":"August Infotech","logo":"http:\/\/www.augustinfotech.com\/app\/asset\/uploads\/2016\/10\/august-infotech.png"}`
    };
  }
  setRichSnippet3() {
    return {
      __html: `
    [{
      "@context" : "http://schema.org",
      "@type" : "Organization",
      "name" : "August Infotech",
      "logo" : "http://www.augustinfotech.com/app/asset/uploads/2016/10/august-infotech.png",
      "description": "August Infotech is India based WordPress, Magento, Shopify Outsource Web Development Company.",
      "knowsAbout": [ "Content Management System", "WordPress", "Website development", "E-commerce", "Shopify", "Magento", "Woocommerce", "PHP Application Development", "Drupal", "Big Data", "Python", "ERPNext", "DotNet","Mobile Development", "iOS","Android", "Native", "Hybrid","React JS"],
      "email" : "info@augustinfotech.com",
      "url" : [ "https://www.augustinfotech.com/", 
      "https://www.augustinfotech.com/blog/",
      "https://www.linkedin.com/company/august-infotech/" ] ,
      "telephone" : [ "[India Flag] +91.898.000.8230", "[USA Flag] +1.646.583.0048" ],
      "memberOf": [ "South Gujarat Chamber of Commerce and Industry", "Ministry of Industry and Trade, Government of India", "Enom", "ERPNext", "Drupal"],
      "address" : {
        "@type" : "PostalAddress",
        "streetAddress" : "805 SNS Platina, Vesu Main Road",
        "addressLocality" : "Vesu, Surat",
        "addressRegion" : "Gujarat",
        "addressCountry" : "India",
        "postalCode" : "395007"
      }
    }]
    `
    };
  }
  //set the rich snippet end
  //set the google tag manager start
  setGoogleTagManager() {
    return {
      __html: `
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5VWBS5V');
    `
    };
  }
  //set the google tag manager end

  //set the facebook pixel code start
  setFacebookPixelCode() {
    return {
      __html: `!function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '511539982763357');
    fbq('track', 'PageView');`
    };
  }

  structuredData() {

    const temp_1 = this.props?.seo_info?.structuredData
    const data = JSON.stringify(temp_1)
    return {
      __html: data || `[
      {"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.augustinfotech.com\/","name":"August Infotech","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.augustinfotech.com\/?s={search_term_string}","query-input":"required name=search_term_string"}},
      {"@context":"http:\/\/schema.org","@type":"Organization","url":"https:\/\/www.augustinfotech.com\/","sameAs":["https:\/\/www.facebook.com\/August.Infotech","https:\/\/www.instagram.com\/augustinfotech\/","https:\/\/www.linkedin.com\/in\/august-infotech-hr\/","https:\/\/twitter.com\/AugustInfotech"],"@id":"#organization","name":"August Infotech","logo":"http:\/\/www.augustinfotech.com\/app\/asset\/uploads\/2016\/10\/august-infotech.png"},
      {
        "@context" : "http://schema.org",
        "@type" : "Organization",
        "name" : "August Infotech",
        "logo" : "http://www.augustinfotech.com/app/asset/uploads/2016/10/august-infotech.png",
        "description": "August Infotech is India based WordPress, Magento, Shopify Outsource Web Development Company.",
        "knowsAbout": [ "Content Management System", "WordPress", "Website development", "E-commerce", "Shopify", "Magento", "Woocommerce", "PHP Application Development", "Drupal", "Big Data", "Python", "ERPNext", "DotNet","Mobile Development", "iOS","Android", "Native", "Hybrid","React JS"],
        "email" : "info@augustinfotech.com",
        "url" : [ "https://www.augustinfotech.com/", 
        "https://www.augustinfotech.com/blog/",
        "https://www.linkedin.com/company/august-infotech/" ] ,
        "telephone" : [ "[India Flag] +91.898.000.8230", "[USA Flag] +1.646.583.0048" ],
        "memberOf": [ "South Gujarat Chamber of Commerce and Industry", "Ministry of Industry and Trade, Government of India", "Enom", "ERPNext", "Drupal"],
        "address" : {
          "@type" : "PostalAddress",
          "streetAddress" : "805 SNS Platina, Vesu Main Road",
          "addressLocality" : "Vesu, Surat",
          "addressRegion" : "Gujarat",
          "addressCountry" : "India",
          "postalCode" : "395007"
        }
      }
    ]`
    }
  }
  //set the facebook pixel code end
  render() {
    return (
      <>
        <Head>
          <meta charSet="utf-8" />
          <title>
            {
              this.props?.seo_info?.seo_title ? this.props.seo_info.seo_title : "White label Digital agency in india | Software development, mobile app, web development outsourcing company in india - AugustCode."
            }
          </title>
          {/*Meta tags*/}
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1 shrink-to-fit=no" />
          {/*REMOVE NOINDEX NOFOLLOW ONCE LIVE*/}
          <meta name="robots" content="index,follow" />
          {/*REMOVE NOINDEX NOFOLLOW ONCE LIVE*/}
          <meta name="description"
            content={
              this.props?.seo_info?.seo_description ? this.props.seo_info.seo_description : "AugustCode is best White label digital agency and one of the leading outsourcing company in India offering Software, Mobile application, Web development."
            } />
          {/* dynamic meta content */}
          {this.props?.seo_info?.preventIndexing !== 'true' ? this.props.meta : ''}
          <meta property="og:locale" content="en_US" />
          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://www.augustinfotech.com/" />
          <meta property="og:site_name" content="AugustCode Solutions Pvt. Ltd" />
          <meta name="og:image" content="/static/images/social_media_black.png" />
          <meta name="author" content="AugustCode Solutions Pvt. Ltd" />
          <meta name="organization" content="AugustCode Solutions Pvt. Ltd" />
          <meta name="city" content="Surat" />
          <meta name="country" content="India" />
          <meta name="msvalidate.01" content="7D5A53B5E634B9E407A09F50C7C10B54" />
          {/*Twitter Tag Start*/}
          <meta name="twitter:card" content="summary_large_image" />

          {/* <meta name="twitter:title"
           content=
           "White label Digital agency in india | Software development, mobile app, web development outsourcing company in india - AugustCode." />
          <meta name="twitter:description" content="AugustCode is best White label digital agency and one of the leading outsourcing company in India offering Software, Mobile application, Web development." /> */}
          <meta name="twitter:image"
            content=
            {
              this.props?.seo_info?.sharedImage?.url ? this.props.seo_info.sharedImage.url : "/static/images/social_media_black.png"
            } />



          <meta property="twitter:url" content="https://www.augustinfotech.com/" />
          <meta property="twitter:dc.title" content="AugustCode Solutions Pvt. Ltd." />
          {/*Twitter Tag End*/}
          {/*Geo Tags Start*/}
          <meta name="DC.title" content="AugustCode Solutions Pvt. Ltd." />
          <meta name="geo.region" content="US" />
          <meta name="geo.placename" content="India" />
          <meta name="geo.position" content="21.158779" />
          {/*Geo Tags End*/}
          {/*CSS FILES*/}
          <link rel="stylesheet" type="text/css" href="/static/css/bootstrap.min.css" />
          {/* <link rel="stylesheet" type="text/css" href="/static/css/font-awesome.min.css" /> */}
          <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
          <link rel="stylesheet" type="text/css" href="/static/css/menu.css" />
          <link rel="stylesheet" href="/static/css/hcp0zud.css" />
          <link href="/static/css/aos.css" rel="stylesheet" />
          <link rel="stylesheet" type="text/css" href="/static/css/slick.css" />
          <link rel="stylesheet" type="text/css" href="/static/css/animate.css" />
          <link rel="stylesheet" type="text/css" href="/static/css/splitting.css" />
          <link rel="stylesheet" type="text/css" href="/static/css/style.css" />
          <link rel="stylesheet" type="text/css" href="/static/css/responsive.css" />
          {/* above all files moved into in the main.css with the same order for less css call and improve GT matrix rank start */}
          <link rel="stylesheet" type="text/css" href="/static/css/main.css" />
          {/* above all files moved into in the main.css with the same order for less css call and improve GT matrix rank end */}
          {/* Favicon */}
          <link rel="apple-touch-icon-precomposed" media="(resolution: 326dpi)" href="/static/images/ico/apple-touch-icon-144-precomposed.png" />
          <link rel="apple-touch-icon-precomposed" media="(resolution: 163dpi)" href="/static/images/ico/apple-touch-icon-144-precomposed.png" />
          <link rel="apple-touch-icon-precomposed" media="(resolution: 132dpi)" href="/static/images/ico/apple-touch-icon-144-precomposed.png" />
          <link rel="shortcut icon" type="image/x-icon" href="/static/images/ico/favicon.ico" />
          {/* <script src="/static/js/jquery-2.2.4.min.js"></script>  */}
          {/* <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> */}
          {/* <script defer src="/static/js/popper.min.js"></script>
          <script defer src="/static/js/bootstrap.min.js"></script>
          <script defer src="/static/js/slick.min.js"></script> */}
          {/* <script src="/static/js/TweenMax.min.js"></script> */} {/* cause the issue if we pass the defer or async */}
          {/* <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script> */}
          {/* <script src="/static/js/ScrollMagic.min.js"></script> /*} {/* cause the issue if we pass the defer or async */}
          {/* <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script> */}
          {/* <script defer src="/static/js/animation.gsap.js"></script> */}
          {/*<script src="/static/js/debug.addIndicators.js"></script> */} {/* cause the issue if we pass the defer or async */}
          {/* <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js"></script> */}
          {/* <script defer src="/static/js/MorphSVGPlugin.min.js"></script>
          <script defer src="/static/js/splitting.js"></script>
          <script defer src="/static/js/anime.min.js"></script>
          <script defer src="https://cdn.jsdelivr.net/npm/lazy-line-painter@1.9.4/lib/lazy-line-painter-1.9.4.min.js"></script>
          <script defer src="/static/js/imagesloaded.pkgd.min.js"></script>
          <script defer src="/static/js/three.min.js"></script>  
          <script defer src="/static/js/player.js"></script>
          <script defer src="/static/js/tilt.jquery.js"></script>   */}
          {/* <script defer src="/static/js/snap.svg-min.js"></script> */}
          {/* Global site tag (gtag.js) - Google Analytics Start */}
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-16730861-1"></script>
          <script defer dangerouslySetInnerHTML={this.setGoogleAnalyticCode()} />
          {/* Global site tag (gtag.js) - Google Analytics End */}
          {/* Rich Snippet Start */}
          {/* <script defer type='application/ld+json' dangerouslySetInnerHTML={this.setRichSnippet1()} />
          <script defer type='application/ld+json' dangerouslySetInnerHTML={this.setRichSnippet2()} />
          <script defer type='application/ld+json' dangerouslySetInnerHTML={this.setRichSnippet3()} /> */}
          <script defer type="application/ld+json" dangerouslySetInnerHTML={this.structuredData()} />
          {/* Rich Snippet End */}
          {/* Google Tag Manager */}
          <script dangerouslySetInnerHTML={this.setGoogleTagManager()} />
          {/* Google Tag Manager end */}
          {/* Facebook Pixel Code */}
          <script dangerouslySetInnerHTML={this.setFacebookPixelCode()}></script>
          <noscript><img height="1" width="1" style={{ display: "none" }}
            src={"https://www.facebook.com/tr?id=511539982763357&ev=PageView&noscript=1"}
          /></noscript>
          {/* End Facebook Pixel Code */}
          <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        </Head>


        <noscript className='noscript-warning'>
          <div className="noscript-message">
            <h1 className="text-white">JavaScript is required</h1>
            <p>You need to enable JavaScript to see website.</p>
            <p>Do you need to know how to enable it? <a href="http://enable-javascript.com/" target="_blank" rel="noopener">Go here.</a></p>
          </div>
        </noscript>
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VWBS5V" height="0" width="0" style={{ display: "none", visibility: "hidden" }}></iframe></noscript>
        <div id="main">
          <div className="circle"></div>
          <div className="quote-fixed">
            <a id="modal-trigger" data-type="qu-modal-trigger" className="quote-fixed-button"><img src="/static/images/click_here_estimate.png" alt="Click now for free estimate" /></a>
          </div>
          <div className="main-wrap site-wrapper">
            <Header_Dark menus={this.props.menus} page={this.props.page} />
            <div className="row"><div className="col col-12"><Cookies cookie={this.props.cookie} /></div></div>
            {this.props.children}
            {/* =========================== load JS files before the footer start =========================== */}
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script defer src="/static/js/popper.min.js"></script>
            {/* <script defer src="/static/js/bootstrap.min.js"></script> */}
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
            {/* <script defer src="/static/js/slick.min.js"></script> */}
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
            {/* <script defer src="/static/js/animation.gsap.js"></script> */}
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.2/plugins/animation.gsap.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js"></script>
            <script defer src="/static/js/MorphSVGPlugin.min.js"></script>
            <script defer src="/static/js/splitting.js"></script>
            {/* <script defer src="/static/js/anime.min.js"></script> */}
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
            <script defer src="https://cdn.jsdelivr.net/npm/lazy-line-painter@1.9.4/lib/lazy-line-painter-1.9.4.min.js"></script>
            {/* <script defer src="/static/js/imagesloaded.pkgd.min.js"></script> */}
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.min.js"></script>
            {/* <script defer src="/static/js/three.min.js"></script> */}
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/three.js/84/three.min.js"></script>
            {/* <script defer src="/static/js/player.js"></script> */}
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/vimeo-player/2.8.2/player.min.js"></script>
            <script defer src="/static/js/tilt.jquery.js"></script>
            {/* <script defer src="/static/js/snap.svg-min.js"></script> */}
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.4.1/snap.svg-min.js"></script>
            {/* =========================== load JS files before the footer end =========================== */}
            <Footer menus={this.props.menus} page={this.props.page} social_icons={this.props.social_icons} accquinated={this.props.accquinated} associations={this.props.associations} output={this.props.output} />
          </div>
          {/* main-wrapper close */}
          {/* <!--GO TO TOP--> */}
          <a href="#" id="back-to-top" title="Back to top">&uarr;</a>
        </div>
      </>
    )
  }
}
